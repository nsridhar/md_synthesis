#ifndef _CLUST_HEAD_H_

#define _CLUST_HEAD_H_


#include <stdio.h>
#include <math.h> 
#include <stdlib.h>
#include <string.h>

#define _DEBUG_

#ifdef _DEBUG_
#define assert(expr)  if (!(expr)) {		\
    printf("\n%s%s\n%s%s\n%s%d\n\n",		\
	   "Assertion failed: ",#expr,		\
	   "in file ",__FILE__,			\
	   "in line ",__LINE__);		\
    exit(1);					\
  }
#endif


/* important warning! 
   after creating a cluster by using 
   either the "create_cluster" or the
   "create_copy_cluster" function,
   ALWAYS reference the function 
   "delete_cluster" to clear the area
   of memory which was dynamically allocated */

typedef struct the_cluster{
  
  int species_num;
  char **species_symbol;
  
  int unit_num;
  int *unit_per_species;
  int *unit_type;
  double **unit_coord;
  //double *cm_coord;
  
  double prob;
  
} the_cluster;


typedef struct the_cluster *cluster;


int compare_cluster(cluster clust1,
		    cluster clust2);

int copy_cluster(cluster clust1, 
		 cluster clust2);

cluster create_cluster(int species_num,
		       int unit_num,
		       double probability,
		       char **unit_species,
		       double **unit_coord);


cluster create_copy_cluster(cluster input_cluster);


int delete_cluster(cluster *p_cluster);




int find_cluster_cm(cluster clust,
		    double *species_mass,
		    double *cm_coord);


int reset_cluster(cluster clust);


int rotate_cluster(cluster clust,
		   double phi,
		   double theta,
		   double psi);
//		   double *species_mass);


int translate_cluster(cluster clust,
		      double *trans_vec);

#endif
