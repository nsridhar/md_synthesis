#include "header.h"

int output_configuration(int n_atom, int block, int step,
			 double time, char *xyz_filename, 
			 int *atom_species, //int *atom_state,
			 char element_symbol[][3],
			 double *x, double *y, double *z)
{

  FILE *p_output_file;
  
  if ( (p_output_file=fopen(xyz_filename,"w")) == NULL ) {
    printf("%s%s\n%s\n",
	   "A fatal error occured while opening file ",
	   xyz_filename,
	   "Cannot continue, sorry!");
    exit(1);
  }
  
  fprintf(p_output_file, "%d\n", n_atom);
  fprintf(p_output_file, "Block %d, step %d, time/s %12.4e\n",
	  block, step, time);

  /*print position of all atoms*/
  for (int i=0; i<n_atom; i++)
    fprintf(p_output_file, "%s  %12.3f %12.3f %12.3f\n",
	    element_symbol[atom_species[i]], 
	    x[i], y[i], z[i]);
	    //, atom_state[i]);

  

  fclose(p_output_file); 
  
  return(0);
}
