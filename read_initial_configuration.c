#include "header.h"


int read_initial_configuration(char *pos_file_name, 
			       int *tot_atom_number, 
			       int num_species, 
			       //int num_gas_species,
			       int *atoms_per_species, 
			       int *atom_species,
			       char element_symbol[][3],
			       //int num_fix_plane,
			       //int *num_fix_atom,
			       //int *n_per_plane,
			       double *lowest_quota,
			       //double *initial_slab_height,
			       //double *z_thermostat,
			       //int *ncellx, int *ncelly, int *ncellz,
			       double *x_box, double *y_box, double *z_box,
			       //double *slab_height,
			       double *x_pos, double *y_pos, double *z_pos)
{
 
  FILE *p_pos_file;

  char line[n_max_atom+30][200];
  
  int line_cnt;
  char* token[100]; 
  
  if ( (p_pos_file=fopen(pos_file_name, "r")) == NULL )  {
    printf("Problems encountered in function read_initial_configuration.\n");
    printf("could not open file %s\n", pos_file_name);
    printf("Cannot continue, sorry!\n");
    exit(1);
  }
  
  
  /* read input file line by line and store 
     each line in buffer strings */
  line_cnt = 0;
  while ( fgets(line[line_cnt],200,p_pos_file) != NULL 
	  &&
	  line_cnt < n_max_atom+30 ) 
    line_cnt++;
  
  fclose(p_pos_file);



  line_cnt = 0;
  
 
  /* read total atom number in the system */
  break_line(line[line_cnt++], token);
  *tot_atom_number = atoi(token[0]);
  //fscanf(p_pos_file,"%d", tot_atom_number);
  /* make sure number of atoms 
     does not exceed maximum allowed 
     number of atoms */
  if ( *tot_atom_number > n_max_atom ||
       *tot_atom_number <= 0 ) {
    printf("\n%s\n%s%d%s\n%s%d%s\n%s\n\n",
	   "Error in function read_initial_configuration!",
	   "The number of atoms in the system is ",
	   *tot_atom_number, ",",
	   "whereas the maximum allowed number of atoms is ",
	   n_max_atom, ".",
	   "It is not possible to continue, sorry!");
    exit(1);
  }
    


  /* read dimensions of simulation unit  
     cell in Angstroms */
  break_line(line[line_cnt++], token);
  //*ncellx = atoi(token[0]);
  //*ncelly = atoi(token[1]);
  //*ncellz = atoi(token[2]);
  *x_box = atof(token[0]);
  *y_box = atof(token[1]);
  *z_box = atof(token[2]);
  //fscanf(p_pos_file, "%d %d %d",
  //	 ncellx, ncelly, ncellz);
  //fscanf(p_pos_file, "%lf %lf %lf",
  //	 x_box, y_box, z_box);
  
  /*initialise all elements of 
   array counting the number of 
   atoms per each chemical species 
   to 0 */ 
    
    for (int index=0; index<num_species; index++)
      atoms_per_species[index] = 0;
    
    
    char element[2];

    /* start cycle over all atoms */
  for (int i=0; i<*tot_atom_number; i++) {
    
    //fscanf(p_pos_file,"%s %lf %lf %lf", element, 
    //   x_pos+i, y_pos+i, z_pos+i);
   
    break_line(line[line_cnt++], token);
    strncpy(element, token[0], 2);
    x_pos[i] = atof(token[1]);
    y_pos[i] = atof(token[2]);
    z_pos[i] = atof(token[3]);

    /* assign atom with a type */ 
    int this_type = 0;
    atom_species[i] = -1;
    while ( this_type<num_species 
	    &&
	    atom_species[i] < 0 ) {
      
      if ( strncmp(element,
		   element_symbol[this_type],2) == 0 ) {
	atom_species[i] = this_type;
	atoms_per_species[this_type]++;
      }
      
      this_type++;
    }

/*     int atom_element[num_species]; */
/*     for (int index=0; index<num_species; index++) */
/*       atom_element[index] = 0; */
    
/*     int j = 0; */
       
/*     for ( int k = 0; k<num_species; k++ )  */
/*       if ( strncmp(element,element_symbol[k],2) == 0 ) { */
/* 	atom_element[j] = k; */
/* 	j++; */
/*       } */
    
    if ( atom_species[i] < 0 ) {
      printf("\n%s\n%s%s\n%s\n%s\n\n",
	     "Error in function read_initial_configuration.",
	     "The element type read ", element,
	     "has not been recognised in this context.",
	     "Cannot continue, sorry!");
      exit(1);
    }
    
    
  } /* end of for (int i=0; i<*tot_atom_number; i++) */
  /* end of cycle over all atoms */

  
  /* consistency check */
  int atom_sum = 0;
  for(int i = 0; i<num_species; i++)
    atom_sum += atoms_per_species[i];
  if( *tot_atom_number != atom_sum ) {
    printf("%s\n%s\n%s\n",
	   "Error in function read_initial_configuration.",
	   "The total number of atoms do not match",
	   "Cannot continue, sorry!");
    exit(1);
  }
  

  
  /* find maximum distances among atoms
     in all three directions */
  double max_x = -1.0e20, 
    max_y = -1.0e20,
    max_z = -1.0e20;
  double min_x = 1.0e20,
    min_y = 1.0e20,
    min_z = 1.0e20;

  for (int i=0; i<*tot_atom_number; i++) {
    if ( x_pos[i] > max_x )
      max_x = x_pos[i];
    if ( x_pos[i] < min_x )
      min_x = x_pos[i];
    if ( y_pos[i] > max_y )
      max_y = y_pos[i];
    if ( y_pos[i] < min_y )
      min_y = y_pos[i];
    if ( z_pos[i] > max_z )
      max_z = z_pos[i];
    if ( z_pos[i] < min_z )
      min_z = z_pos[i];
  }
    
  *lowest_quota = min_z;

  double x_len, y_len, z_len;
  x_len = max_x - min_x;
  y_len = max_y - min_y;
  z_len = max_z - min_z;

  /* make sure simulation box dimensions are 
     compatible with distances among atoms */
  if ( x_len > *x_box ) {
    printf("\n%s\n%s\n%s%.3lf%s\n%s%.3lf%s\n%s\n\n",
	   "Fatal error in function read_initial_configuration.",
	   "The length of the simulation unit cell along x",
	   "has been set equal to ", *x_box, "Angstr",
	   "whereas the maximum distance among atoms is ",
	   x_len, "Angstr.",
	   "It is not possible to continue, sorry!");
    exit(1);
  }
  if ( y_len > *y_box ) {
    printf("\n%s\n%s\n%s%.3lf%s\n%s%.3lf%s\n%s\n\n",
	   "Fatal error in function read_initial_configuration.",
	   "The length of the simulation unit cell along y",
	   "has been set equal to ", *y_box, "Angstr",
	   "whereas the maximum distance among atoms is ",
	   y_len, "Angstr.",
	   "It is not possible to continue, sorry!");
    exit(1);
  }
  if ( max_z > *z_box ) {
    printf("\n%s\n%s\n%s%.3lf%s\n%s%.3lf%s\n%s\n\n",
	   "Fatal error in function read_initial_configuration.",
	   "The length of the simulation unit cell along z",
	   "has been set equal to ", *z_box, "Angstr",
	   "whereas the maximum z coordinate of atoms is ",
	   max_z, "Angstr.",
	   "It is not possible to continue, sorry!");
    exit(1);
  }


   
  return 0;
  
}
