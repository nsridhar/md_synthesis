#include "header.h"

#define one_over_4pieps0 14.38008 /* 1/(4*pi*eps0) in units of eV*Angstr/e^2, where e is the electron charge, 1 e = 1.6*10^-19 C*/

/*  
    V_coul(r) is calculated according to the 
    prescription of Fennell and Gezelter 
    (J. Chem. Phys. Vol. 124, page 234104 (2006)) 
    V_coul(r) = q1*q2*[ erfc(alpha*r)/r 
    - erfc(alpha*Rc)/Rc
    + ( erfc(alpha*Rc)/Rc^2 
    + 2*alpha/pi^0.5*exp(-alpha^2*Rc^2)/Rc )
    * (r-Rc) ];
    where Rc is the radius of the cutoff sphere
    and alpha is a damping constant with 
    dimensions of an inverse length. 
    The function also calculates the derivative
    of the Coulomb potential multiplied by the
    distance r. 
*/

int calculate_coulomb_potential(double r,
				double r_cut,
				double q1, double q2,
				double alpha,
				double *p_coulomb_pot,
				double *p_dpot_times_r)
{
  
  double multipl_const = one_over_4pieps0*q1*q2;
  double alpha_r = alpha*r;
  double alpha_rcut = alpha*r_cut;
  double erfc_alpha_r = 1.0 - erf(alpha_r);
  double erfc_alpha_rcut = 1.0 - erf(alpha_rcut);
  //double erfc_alpha_r = erfc(alpha_r);
  //double erfc_alpha_rcut = erfc(alpha_rcut);
  double exp_minus_alpha_rcut_sq = exp(-alpha_rcut*alpha_rcut);
  
  double one_over_r = 1.0/r;
  double one_over_rcut = 1.0/r_cut;
  double two_alpha_over_sqrt_pi = 2.0*alpha/sqrt_pi;

  double v_coul = 
    multipl_const * 
    ( erfc_alpha_r*one_over_r 
      - erfc_alpha_rcut*one_over_rcut 
      + (r-r_cut) * 
      (erfc_alpha_rcut*one_over_rcut*one_over_rcut 
       + two_alpha_over_sqrt_pi*exp_minus_alpha_rcut_sq
       *one_over_rcut) );
  
  double pot_deriv_times_dist = 
    multipl_const *
    ( - erfc_alpha_r*one_over_r 
      - two_alpha_over_sqrt_pi*exp(-alpha_r*alpha_r)
      + erfc_alpha_rcut*one_over_rcut*one_over_rcut*r 
      + two_alpha_over_sqrt_pi*exp_minus_alpha_rcut_sq
      *one_over_rcut*r );
  
  
  /* "export" calculated 
     quantities 
     to calling function */
  *p_coulomb_pot = v_coul;
  *p_dpot_times_r = pot_deriv_times_dist;
    
  return 0;
}




/**************************************************************/


/* 
   calculates second derivative of Coulom potential 
   as per prescription of Fennell and Gezelter 
   (J. Chem. Phys. Vol. 124, page 234104 (2006))  
   D^2(V_coul(r)) =  q1*q2*
   [4*alpha/pi^0.5*exp(-alpha^2*r^2)/r^2 +
   4*alpha^3/pi^0.5*exp(-alpha^2*r^2) +
   2*erfc(alpha*r)/r^3]
*/
int calculate_coulomb_second_deriv(double r,
				   //double r_cut,
				   double q1, double q2,
				   double alpha,
				   double *p_coulomb_second_deriv)
{

  double multipl_const = one_over_4pieps0*q1*q2;
  double one_over_r = 1.0/r;
  double alpha_r = alpha*r;
  double exp_minus_alpha_r_sq = exp(-alpha_r*alpha_r);
  double two_alpha_over_sqrt_pi = 2.0*alpha/sqrt_pi;

  double second_deriv = multipl_const *
    (2.0*two_alpha_over_sqrt_pi*exp_minus_alpha_r_sq
     *one_over_r*one_over_r 
     + 2.0*two_alpha_over_sqrt_pi*alpha
     *alpha*exp_minus_alpha_r_sq
     + 2.0*erfc(alpha_r)*one_over_r
     *one_over_r*one_over_r);
  
  *p_coulomb_second_deriv = second_deriv;
  
  return 0;
}
