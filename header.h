#ifndef _HEAD_H_
#define _HEAD_H_

#include <stdio.h>
#include <math.h> 
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "cluster_linked_list.h"

#define kb 8.617e-5      /* Boltzmann constant expressed in eV/K */
#define avogadro_number 6.0221415e23
#define kg_per_amu  1.66e-27  /* factor converting weights from amu to Kg ( =1.0/(6.02e26)=1.0/(Na*10^3) ) */
#define joule_per_ev  1.6e-19  /* factor converting energies from eV to Joules */
#define metre_per_angstrom  1.0e-10 /* factor converting lenghts from Angstr to metres */
#define pi 3.14159265
#define sqrt_pi 1.77245385 /* square root of pi */
//#define one_over_4pieps0 14.38008 /* 1/(4*pi*eps0) in units of eV*Angstr/e^2, where e is the electron charge, 1 e = 1.6*10^-19 C*/


//#define n_max_atom 8000   /* maximum number of atoms in the system */
#define n_max_type 5     /* maximum number of atom species in the system */

#define _DEBUG_

#ifdef _DEBUG_

#define assert(expr)  if (!(expr)) {		\
    printf("\n%s%s\n%s%s\n%s%d\n\n",		\
	   "Assertion failed: ",#expr,		\
	   "in file ",__FILE__,			\
	   "in line ",__LINE__);		\
    exit(1);					\
  }
#endif


//#define anint(expr) rint(expr)

#define anint(expr) (int)(expr < 0 ? expr-0.5 : expr+0.5)

/*atom and substrate related parameters */
char element_name[n_max_type][3]; /*element = "Au", "Ag", "C", etc. */
double van_der_waals_radius[n_max_type], /*Van der Waals radius of the element in Angstroms. Here only used for display  in output file */
//ionic_radius[n_max_type], /* in case of ionic compounds, ionic radii are provided */
  dmas[n_max_type]; /* mass number of the element making up the system expressed in a.m.u. */
// rnn; /*nearest neighbour distance between slab atoms*/



int  n_fix_atom,   /* total number of fixed atoms  */
  n_species,    /* number ofatomic species (<=n_max_species) */
  n_gas_species, /* number of atomic species which are initially in gas phase */
  n_substrate_species, /* number of atomic species initially making up the substrate */
  n_initial_atom, /* number of atoms at the beginnig of the simulation */
  n_tot_atom,   /* total number of atoms (<=n_max_atom) */
  n_max_atom, /* maximum number of particles at the end of the simulation */
  n_atom_type[n_max_type], /* number of atoms per species */
  set_box_dimensions, /* = 0, 1; when set to 1, box dimensions can be input by the user */
  initial_struct_z_translation; /* =1, 0; translate structure along z in such a way that highest point is at z=0 */
double boxx, boxy, boxz,  /* simulation unit cell dimensions in Angstroms */
  boxx_input, boxy_input, boxz_input, /* values of unit cell dimensions in Abngstroms input by user. Only necessary if option set_box_dimensions is set to 1 */
  z_fix_atom, /* all atoms with initial z coordinates less than z_fix_atom are fixed */
  min_z, /* point of initial configuration with lowest z-coordinate */
  max_z, /* z-coordinate of upper limit of simulation cell, the condition z[i]<max_z hlods for each i=0,1,2...n_tot_atom */
  init_substrate_surface_z_coord, /* z-coordinate of substrate surface when deposition is initiated. assumed to be 0 unless different value is input by user */
  min_x_box_corner, min_y_box_corner, min_z_box_corner, /* position of simulation cell corner with lowest x-, y-, and z-coordinates. this corner belongs by convention to first cell in cell list  */
  slab_thickness, /* vertical dimension of substrate */
  init_slab_thickness, /* vertical dimension of slab at the beginning of the deposition */
  substrate_current_height; /* z coordinate of highest point of growing solid */


/* array with clusters to be deposited */
cluster *deposit_clust;
int n_deposit_clust; 


/* parameters of cell list */
int n_cellx, n_celly, n_cellz, /* number of cells in the 3 dimensions of space into which the simulation box is divided */
  n_cell, /* n_cell = n_cellx*n_celly*n_cellx */
  n_stripe; /* size of array mask_point is 2*n_stripe */



/*potential parameters*/

/* Type of interaction between
   each couple of species. 
   The convention is as follows: 
   Id numb  Interaction type
   0        Born-Mayer (substrate) 
   1        Lennard-Jones ( depositing species )
   2        10-5 potential ( 0-depositing species )
   3        10-5 and r^-5 hybrid potential ( Al-depositing species ) */ 
int interaction_type[n_max_type][n_max_type];

/* Lennard-Jones 12-6 potential parameters */
double sigma_lj[n_max_type][n_max_type], /* typical length of Lennard Jones 
					    potential expressed in Angstroms */ 
  epsilon_lj[n_max_type][n_max_type]; /* typical energy of Lennard Jones 
					 potential expressed in eV */

/* Born-Mayer potential parameters */
double q[n_max_type], 
  b[n_max_type][n_max_type],
  c[n_max_type][n_max_type], 
  d[n_max_type][n_max_type],  
  alpha[n_max_type][n_max_type], 
  rho[n_max_type][n_max_type]; 

/* Mixed interaction hybrid potential parameters */
double sigma_inter[n_max_type][n_max_type],
  epsilon_inter[n_max_type][n_max_type],
  join_coeff_inter[n_max_type][n_max_type][6],
  join_range_limits[n_max_type][n_max_type][2]; /* hybrid potential Vij is a polynomial between join_range_limits[i][j][0] and join_range_limits[i][j][1] */



/* truncation and Verlet list parameters */
double cutoff_radius[n_max_type][n_max_type],  /*cutoff distance expressed in units of sigma*/
  potential_range[n_max_type][n_max_type], /*range of action of potential. Potential vanishes for any r>potential_range. Also expressed in units of sigma*/
  species_bond_distance[n_max_type][n_max_type], /* bond distance between atoms of all species */
  max_pot_range,  /* maximum range among all potentials used. it is 
		     the maximum value in matrix potential_range */
  a[n_max_type][n_max_type][6]; /*parameters for potential truncation. Between cutoff radius and potential range distance
				  the potential is expressed as: a[0] + a[1]*r + a[2]*r^2 +...+ a[5]*r^5 */


/* miscellaneous MD parameters */ 
int n_block, /*MD simulation is comprised of n_blocks of n_step iterations each*/
  n_relax_step,/*iterations for slab energy minimisation*/
  current_block, /*current block*/
  verlet_cnt, /*count number of times when Verlest list was updated*/
  n_adatom_x_cross, n_adatom_y_cross, /*in case of single adatom run, 
					these variables keep track of number 
					of times when the adatom crosses the 
					border of the simulation unit cell*/
  n_step_therma, /* number of prerun iteration */
  energy_output_freq, /*output energy each energy_output_freq iterations*/
  velocity_output_freq, /*write atom velocities in file every velocity_output_freq iterations*/
  relax_output_freq, /*temperature and kinetic energy output frequency during slab relaxation*/
  adatom_position_freq, /* number of steps between two consecutive outputs of adatom position in case of run with single adatom */
  thermostat_freq,
  random_number_generator_seed; /* seed of random number generator run2 */

unsigned long int n_step, /*number of MD iterations per block*/
  current_step, /*current iteration*/
  config_output_freq, /*system configuration is written into a file once each config_output_freq MD iterations*/
  step_per_atom_release; /*number of MD iterations between consecutive cluster depositions */


/* Variables for restarting simulation from given 
   initial configuration or initial velocity set.
   In case single_adsorbate is set to 1, one atom
   is deposited at beginning of simulation 
   Option for locally minimising structure */
int initial_positions, initial_velocities, single_adsorbate, /* = 0,1 */
  energy_minimisation;                                      /* = 0,1 */
char initial_positions_file[100], initial_velocities_file[100],
  adsorbate_position_file[100], adsorbate_velocity_file[100];


double initial_system_temperature,
  final_system_temperature,
  substrate_thermostat_temperature, /* temperature of thermostat applied to lower part of substrate */
  system_temperature, /* current system temperature  */
  minimisation_bottom_temperature, /* minimum temperature when simulation stops, only used in the case of local optimisation*/
  initial_atom_surface_distance, /* initial distance between a newly created atom and substrate surface*/
  min_initial_atom_surface_distance, /* minimum intiial distance allowed between impinging atom and substrate */
  initial_clust_temp, /* kinetic energy of atom after being introduced*/
  impingement_rate,  /* rate of creation of impinging particles in 1/ps*/
  sec_per_atom_release, /* time in sec between introduction of two consecutive new atoms */
  z_slab_thermostat, /* thermostat is applied to all substrate atoms with z <= z_slab_thermostat */
  initial_pot_energy,
  final_pot_energy,
  potential_energy, /* current system potential energy */
  kinetic_energy, /* current kinetic energy */
  time_step, /* time step duration in s */
  current_time; /* current time in the simulated system expressed in s */


/*structure which keeps track of CPU and actual time*/
/*taken by program execution*/
//typedef struct {
//  clock_t begin_clock, end_clock;
//  time_t begin_time, end_time;
//} time_keeper;



int Al2O3_generator(int ncellx, int ncelly, int ncellz, 
		    int n_offset, 
		    double rnn,
		    int *atom_species, 
		    int *atoms_per_species, 
		    int *n_per_plane,  
		    int num_fix_plane,  
		    int *num_fixed_atom,
		    double *xlen, double *ylen, double *zlen,
		    double *x, double *y, double *z);


//double anint(double x);

int assign_cm_coordinates(double z_coord,
			  double box_x_dim, 
			  double box_y_dim,
			  double box_z_dim,
			  double *atom_x_pos, 
			  double *atom_y_pos, 
			  double *atom_z_pos);

int assign_atom_velocities(double init_temp,
			   int init_index,
			   int dep_atom_num,
			   int *atom_type,
			   //cluster dep_clust,
			   //char species_symbol[][3],
			   double *species_mass,
			   double *x_vel, double *y_vel,
			   double *z_vel);

int assign_variable(const char* input_string, char* format, ...);

int break_line(char* string, char** token);

int calculate_10_5_potential(double dist12_squared,
			     double *p_pot12,
			     double *p_dpot12_times_dist,
			     double sigma12,
			     double epsilon12,
			     double cutoff_dist12,
			     double *truncation_params);

int calculate_born_mayer_potential(double dist12_squared,
				   double *p_pot12,
				   double *p_dpot12_times_dist,
				   double rho_param,
				   double q1, double q2,
				   double b12, double c12,
				   double d12, double alpha_coulomb,
				   double cutoff_dist12,
				   double pot_range_12,
				   double *truncation_params); 



int calculate_born_mayer_second_deriv
(double dist_12,
 double *p_pot12_second_deriv,
 double rho_param_12,
 double q1, double q2,
 double b12, double c12,
 double d12, 
 double alpha_coulomb_12);


//int calculate_coulomb_potential(double r,
//				double r_cut,
//				double q1, double q2,
//				double alpha,
//				double *p_coulomb_pot,
//				double *p_dpot_times_r);

//int calculate_coulomb_second_deriv
//(double r,
//double q1, double q2,
//double alpha,
//double *p_coulomb_second_deriv);



int calculate_hybrid_potential(double dist12_squared,
			       double *p_pot12,
			       double *p_dpot12_times_dist,
			       double sigma12,
			       double epsilon12,
			       double r1, double r2,
			       double cutoff_dist12,
			       double *join_coeff,
			       double *truncation_param);


int calculate_lj_potential(double dist12_squared,
                           //double *p_pot12,
                           //double *d_pot12_times_dist,
			   double *p_lj_pot12,
			   double *p_dlj12_times_dist,
                           double q1, double q2,
                           double alpha_coulomb_12, 
			   double sigma12,
			   double epsilon12,
			   double cutoff_dist12,
                           double pot_range_12,
			   double *truncation_params);


int calculate_temperature(int atom_number,
			  int fixed_atom_num,
			  int *atom_species,
			  double *atom_mass,
			  double *p_temperature,
			  double *vel_x,
			  double *vel_y,
			  double *vel_z);



int calculate_truncation_params
(int n_species,
 double *q_bm, 
 double alpha_bm[][n_max_type],
 double b_bm[][n_max_type], 
 double c_bm[][n_max_type],
 double d_bm[][n_max_type], 
 double rho_bm[][n_max_type],
 double sigma_lennard_jones[][n_max_type],
 double eps_lennard_jones[][n_max_type],
 double sigma_mixed_interact[][n_max_type],
 double eps_mixed_interact[][n_max_type],
 double hybrid_join_coeff[][n_max_type][6],
 double hybrid_join_interval_limits[][n_max_type][2],
 int interact_type[][n_max_type],
 double trunc_coeff[][n_max_type][6],
 double cutoff_distance[][n_max_type],
 double pot_range[][n_max_type]);



int check_new_clust_position(int init_index,
			     int dep_atom_num,
			    int atom_number,
			    int num_fixed_atom,
			    double *solid_highest_quota,
			    double minimum_atom_solid_distance,
			    int *atom_species,
			    double *vdw_radius,
			    double bond_distance[][n_max_type],
			    double box_x_dim, 
			    double box_y_dim,
			    double box_z_dim,
			    double *x_pos, 
			    double *y_pos, 
			    double *z_pos);


int construct_pair_list(int cellx_num,
			int celly_num,
			int cellz_num,
			int *p_cell_tot_num,
			double boxx_size,
			double boxy_size,
			double boxz_size,
			int *p_stripe_num,
			int **p_mask_pointer,
			//int **p_cell_pointer,
			double cutoff_radius);


int create_cell_arrays(int particle_num,
		       int cell_num,
		       int **p_cell_list,
		       int **p_cell_pointer);



int delete_cell_arrays(int *cell_list,
		       int *cell_pointer);
 
int delete_pair_list(int *mask_pointer);
		    

int delete_arrays(double *x_pos,
		  double *y_pos,
		  double *z_pos,
		  double *vx,
		  double *vy,
		  double *vz,
		  double *fx,
		  double *fy,
		  double *fz,
		  int *part_type,
		  int *fix_part_label);


//double erff(double x);
//Returns the error function erf(x) = (2/pi^(0.5))*Int[0,x; Exp[-t^2]; dt].

//double erffc(double x);
//Returns the complementary error function erfc(x) = 1 - erf(x).


int fcc111_gen(int ncellx, int ncelly, int ncellz, int n_offset, 
	       double rnn, double *layer_separation,
	       int *atom_species, int *atoms_per_species,  
	       int *n_per_plane,
	       int num_fix_plane,  int *num_fixed_atom,
	       double *xlen, double *ylen, double *zlen,
	       double *x, double *y, double *z);

double find_z_fix_atom(int atom_number,
		       double slab_fraction,
		       double slab_height,
		       double z_min,
		       double *z_coord);

double find_z_thermostat(int atom_number,
			 double slab_fraction,
			 double slab_height,
			 double z_min,
			 double *z_coord);

int find_z_extrema(int atom_num,
		   //int fixed_atom_num,
		   int *atom_species,
		   double bond_dist[][n_max_type],
		   double boxx_dim, 
		   double boxy_dim,
		   double *p_z_bottom,
		   double *p_z_top,
		   double *x_pos,
		   double *y_pos,
		   double *z_pos);



int force(int atom_num,
	  int cell_num,
	  int stripe_num,
	  int *fix_atom_label, 
	  int potential_type[][n_max_type],
	  double box_x, double box_y, 
	  //double box_z,
	  int *atom_species,
	  double *p_pot_en,
	  double rho_bm[][n_max_type], double *q_bm, 
	  double b_bm[][n_max_type], 
	  double c_bm[][n_max_type],
	  double d_bm[][n_max_type], 
	  double alpha_bm[][n_max_type],
	  double epsilon_lj[][n_max_type], 
	  double sigma_lj[][n_max_type],
	  double sigma_mixed[][n_max_type], 
	  double epsilon_mixed[][n_max_type],
	  double join_area_limits_mixed[][n_max_type][2],
	  double join_coeff_mixed[][n_max_type][6],
	  double pot_range[][n_max_type], 
	  double cutoff_distance[][n_max_type], 
	  double truncation_params[][n_max_type][6],
	  int *mask_pointer,
	  int *cell_pointer,
	  int *cell_list,
	  double *force_x, double *force_y, double *force_z,
	  double *x_coord, double *y_coord, double *z_coord);



int gauss(double *g1, double *g2, double *g3);


/* Function in library lib_gamma_jordan.a
   This function solves a system of linear
   equations by means of Gauss-Jordan elimination
   algorithm.
   var_num is the number of variables, 
   coeff is a (var_num x var_num) matrix
   of coefficients of the linear set of equations
   in var_num variables. rhs is a 
   (var_num X rhs_col_num) matrix of input coefficients
   such that the following equation 
   coeff * Sol = rhs
   is the system to solve, with Sol   
   (var_num X rhs_col_num) matrix of coefficients
   to determine to solve the system. 
   On output, coeff matrix is the identity matrix
   and the rhs matrix is replaced by the matrix Sol,
   having the set of solution vectors as columns*/
int gauss_jordan(double **coeff, 
			  double **rhs, 
			  int var_num, 
			  int rhs_col_num);


int init_substrate_vel(int num_substrate_species,
		       int fixed_atom_num,
		       int  *fixed_atom_label,
		       int *atoms_per_species, 
		       int *atom_species,
		       double *atom_mass, 
		       double temperature, 
		       double *vx, double *vy, double *vz);

int initialize_arrays(int init_part_num,
		      int deposit_clust_at_start,
		      int *p_max_final_part_num,
		      unsigned long int step_tot_num,
		      unsigned long int step_per_new_cluster,
		      int deposit_clust_max_size,
		      double **p_x_pos,
		      double **p_y_pos,
		      double **p_z_pos,
		      double **p_vx,
		      double **p_vy,
		      double **p_vz,
		      double **p_fx,
		      double **p_fy,
		      double **p_fz,
		      int **p_part_type,
		      int **p_fix_part_label);

char *int_to_string(int n);


int introduce_new_atom(int index,
		       int dep_clust_num,
		       cluster *dep_clust_array,
		       int particle_num,
		       int *p_deposit_part_num,
		       int num_fixed_atom,
		       double *species_masses,
		       char element_symbol[][3],
		       int *atom_species, 
		       int *atom_per_species,
		       double *vdw_radius,
		       double bond_distance[][n_max_type],
		       double distance_from_substrate,
		       double minimum_dist_from_substrate,
		       double *substrate_surface_z_position,
		       double clust_initial_temp,
		       double boxx, double boxy, 
		       double boxz,
		       double *x_pos, double *y_pos,
		       double *z_pos,
		       double *x_vel, double *y_vel, 
		       double *z_vel);


int label_fixed_atom(int atom_number,
		     double upper_z_coord,
		     int *fix_atom_label,
		     double *z_pos);

int label_substrate_atoms(int atom_number, 
			  int *atom_species,
			  int *atom_in_substrate,
			  double bond_dist[][n_max_type],
			  double box_x_dim, double box_y_dim,
			  double *x_pos, double *y_pos, 
			  double *z_pos);

int layer_analysis(int md_step, double system_time,
		   int atom_number, 
		   int initial_plane_number, int atom_per_plane,
		   int *atom_state,
		   double layer_separation, double tolerance, 
		   double *x_pos, double *y_pos, double *z_pos);



int output_configuration(int n_atom, int block, int step,
			 double time, char *xyz_filename, 
			 int *atom_type, 
			 char element_symbol[][3],
			 double *x, double *y, double *z);

int output_restart_file(int atom_num, char *file_name,
			int *atom_species, 
			char element_symbol[][3],
			int num_x_cell, int num_y_cell, 
			int num_z_cell,
			double *x_pos, double *y_pos, 
			double *z_pos);

int output_temperature(int block_num, int step_num, int atom_num, 
		       int num_substr_plane, int atom_per_plane, 
		       double slice_height, 
		       double max_height, double min_height, 
		       double *z_pos, 
		       int *atom_species, double *atom_mass,
		       double *x_vel, double *y_vel, double *z_vel);

int output_velocity(int n_atom, char *file_name, int *atom_species,
		    char element_symbol[][3],
		    double *vel_x, double *vel_y, double *vel_z);

int print_header(char *summary_file, char *energy_file);

int print_summary(char *file_name);

float ran2(void);

int random_choice(int size,
		  double *prob);


int read_dep_clust_input(cluster** p_clust_array,
			 char* input_file_name);


int read_initial_configuration(char *pos_file_name, 
			       int *tot_atom_number, 
			       int num_species, 
			       int *atoms_per_species, 
			       int *atom_species,
			       char element_symbol[][3],
			       //int num_fix_plane,
			       //int *num_fix_atom,
			       //int *n_per_plane,
			       double *lowest_quota,
			       //double *initial_slab_height,
			       //double *z_thermostat,
			       //int *ncellx, int *ncelly, int *ncellz,
			       double *x_box, 
			       double *y_box, 
			       double *z_box,
			       double *x_pos, 
			       double *y_pos, 
			       double *z_pos);

int read_initial_part_num(char *position_input_file);

int read_initial_velocities(char *vel_file_name, int n_atom,
			    double *x_vel, double *y_vel, double *z_vel);

int read_input(char *input_file_name, char *pot_params_input_file_name);



int resize_cell_list(int current_size,
		     int new_size,
		     int **p_cell_list);

int set_initial_structure(int particle_num,
			  int *part_species,
			  int translate_structure_vertically,
			  double bond_dist[][n_max_type],
			  double boxx_dim, 
			  double boxy_dim, 
			  double boxz_dim,
			  //double *p_min_x,
			  //double *p_min_y,
			  double *p_min_z,
			  double *p_solid_top_coord,
			  double *p_init_substrate_surf_z_pos,
			  double *x,
			  double *y,
			  double *z);

int set_potentials();

int set_seed(int seed_value);

int therma(int num_species, 
	   int *atoms_per_species,
	   int *atom_species, 
	   double *atom_mass,
	   int *fix_atom_label,
	   double x_box, double y_box, double z_box, 
	   double max_z_coord,
	   //double slab_height, 
	   //double min_z,
	   double time_interval,
	   int this_block, int this_step,
	   int single_adatom_run, 
	   int *adatom_x_cross, int *adatom_y_cross,
	   double *force_x, double *force_y, double *force_z,
	   double *vel_x, double *vel_y, double *vel_z,
	   double *x, double *y, double *z);

int translate_structure_xy(int particle_number,
			   double *p_xmin,
			   double *p_ymin,
			   double *x_coord,
			   double *y_coord);

int translate_structure_z(int particle_number, 
			  double transl_dist,
			  double *z_coord);


int update_cells(int particle_num,
		 int cellx_num,
		 int celly_num,
		 int cellz_num,
		 double boxx_size,
		 double boxy_size,
		 double boxz_size,
		 double x_min,
		 double y_min,
		 double z_min,
		 //int stripe_num,
		 //int *mask_pointer,
		 int *cell_pointer,
		 int *cell_list,
		 double *x_pos, 
		 double *y_pos,
		 double *z_pos);



int vel(int atom_num,
	int *fix_atom_label,
	int num_fixed_atoms,
	int *atom_species, 
	double *atom_mass, double time_interval, 
	double *prev_fx, double *prev_fy, double *prev_fz,
	double *force_x, double *force_y, double *force_z,
	double *p_kin_energy, 
	double *p_temperature,
	double *vel_x, double *vel_y, double *vel_z);


int vel_partial_update(int part_num,
		       int *fix_atom_label,
		       int num_fixed_atoms,
		       int *atom_species, 
		       double *atom_mass, 
		       double time_interval, 
		       double *force_x, 
		       double *force_y, 
		       double *force_z,
		       double *p_kin_energy, 
		       double *p_temperature,
		       double *vel_x, 
		       double *vel_y, 
		       double *vel_z);

int vel_quench(int atom_num,
	       int num_fixed_atoms,
	       int *atom_species, 
	       double *atom_mass,      
	       double *force_x, double *force_y, double *force_z,
	       double *p_kin_energy,
	       double *p_temperature, double exit_temperature,
	       double *vel_x, double *vel_y, double *vel_z);

int velocity_rescaling(int atom_number,
		       int *fix_atom_label,
		       int *atom_species, 
		       double *atom_mass,
		       double target_thermostat_temp,  
		       double thermostat_area_upper_boundary,
		       double *z_coord,
		       double *x_vel, double *y_vel, double *z_vel);



int write_adatom_coords(char *position_output_file,
			char *velocity_output_file,
			int block,
			int step, double time, 
			double x, double y, double z,
			double vel_x, double vel_y, 
			double vel_z);

int write_energy(char *output_file_name);

int write_energy_quench(char *output_file_name, 
			int block, 
			unsigned long int step, 
			double time_over_s,
			double pot_en, 
			double kin_en,
			double temp);

int write_layer(char *output_file_name, 
		int md_step, double system_time,
		int atom_num, int *atom_state,
		int *plane_index,
		int num_fixed_plane);

int write_temperature(char *output_file_name);


#endif
