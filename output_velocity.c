#include "header.h"


int output_velocity(int n_atom, char *file_name, int *atom_species,
		    char element_symbol[][3],
		    double *vel_x, double *vel_y, double *vel_z)
{
  FILE *p_output_file;
  
  
  if( (p_output_file=fopen(file_name,"w")) == NULL )    
    {
      printf("Error in function output_velocity.\n");
      printf("A fatal error occured while opening file %s\n",file_name);
      printf("Cannot continue, sorry!\n");
      exit(1);
    }
  
  
  fprintf(p_output_file, "%d\n", n_atom);
   fprintf(p_output_file, "\n");
  
  /*print velocity of all atoms*/
  for (int i=0; i<n_atom; i++)   
    fprintf(p_output_file, "%2s %20.2f %20.2f %20.2f\n",
	    element_symbol[atom_species[i]], 
	    vel_x[i], vel_y[i],vel_z[i]);
  
  
  fclose(p_output_file);
  
  return(0);
  
}
