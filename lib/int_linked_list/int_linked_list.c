/* 
   This file contains the implementation
   of the functions associated with a likned
   list of integers.
   
*/


//#include "header.h"
#include "int_linked_list.h"



/* returns number of elements
   in the list*/
int count_element_int_list
(int_linked_list the_list) {
  
  if ( the_list == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function count_element!",
	    "The list is not initialised.");
    return -1; 
  }
  
  //vertex_list current_element = the_list;

  if ( the_list->next == NULL )
    /* last element in the list */
    return 0;
  else
    return (1 + count_element_int_list(the_list->next));
  
}




/***********************************************/



/* Creates and initialises 
   an empty  list
   The first element in the list
   is just a sentinel */
int_linked_list create_int_list()
{
  int_linked_list the_list;

  /* allocate memory for the sentinel,
     first element of the list */
  the_list = (int_linked_list) malloc(sizeof(int_list_node));
  the_list->d = 0;
  the_list->next = NULL; 

  return the_list;
}


/***********************************************/


/* eliminates one element from the list
   returns 1 if the elimination was successful,
   and 0 otherwise */

/* iterative version */
int delete_element_int_list(data the_data,
		   int_linked_list the_list)
{
  int_linked_list current_elem, temp;

  if ( the_list == NULL ) {
    printf( "\n%s\n%s\n\n",
	    "Error in function delete_element!",
	    "The list is not initialised.");
    return -1;
  }
  
  
  current_elem = the_list;
  
  while ( current_elem->next != NULL )
    if ( current_elem->next->d ==  the_data ) {
      
      /* current_element->next points
	 to element to delete.
	 Reassign current_element->next
	 in such a way that it points to
	 element following element to delete
	 and free memory associated with
	 element to delete */
      temp = current_elem->next;
      current_elem->next = current_elem->next->next;
      free(temp);
      /* decrement counter of list elements */
      the_list->d--;
      return 1;
    }
    else
      /* consider following element
	 in the list  */
      current_elem = current_elem->next;

  /* the element to eliminate
     has not been found in
     the list */
  return 0;

} 




/* /\* recursive version of delete_element *\/ */
/* int delete_element_int_list(data the_data,  */
/* 			    int_linked_list the_list) */
/* { */
  
/*   int_linked_list temp; */

/*   if ( the_list == NULL ) { */
/*     printf( "\n%s\n%s\n\n",  */
/* 	    "Error in function delete_element!", */
/* 	    "The list is not initialised."); */
/*     return -1;  */
/*   } */
  

/*   if ( the_list->next == NULL ) */
/*     /\* at the end of the list, */
/*        the element to eliminate */
/*        has not been found in the list *\/ */
/*     return 0; */
/*   else  */
/*     if ( the_list->next->d ==  the_data ) {  */
/*       /\* the_list->next points  */
/* 	 to element to delete. */
/* 	 Reassign the_list->next */
/* 	 in such a way that it points to  */
/* 	 element following element to delete */
/* 	 and free memory associated with */
/* 	 element to delete *\/ */
/*       temp = the_list->next; */
/*       the_list->next = the_list->next->next; */
/*       free(temp); */
/*       return 1; */
/*     } */
/*     else */
/*       return ( delete_element_int_list(the_data, the_list->next) ); */
  
/* } */
  

/***********************************************/



/* deletes the whole list element by element,
   including sentinel node. the list pointer
   is set to NULL */
int delete_int_list(int_linked_list *p_list)
{

  int_linked_list the_list = *p_list;

  if ( the_list == NULL ) {
    /* all of the list has 
       already been deleted */
    *p_list = NULL;
    return 0;
  }  

  while( the_list->next != NULL ) {
    /* delete current node */
    int_linked_list temp = the_list->next;
    the_list->next = the_list->next->next;
    free(temp);
  }  
  
  /* the list is now composed 
     of the sentinel node only */
  /* delete sentinel node and set 
     list to NULL */
  free(the_list);
  *p_list = NULL;
 
  
  
  return 1;
  

}

/***********************************************/


/* returns 1 if element is in 
   the list and 0 otherwise */
int element_in_int_list(data the_data, 
			int_linked_list the_list)
{

  //vertex_list current_elem 

  if ( the_list == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function element_in_list!",
	    "The list is not initialised.");
    return -1;
  }

  
  /* use function recursively */
  if ( the_list->next == NULL )
    /* already at end of list,
       the element was not found 
       in the list */
    return 0;
  else 
    if ( the_data ==  the_list->next->d )
      return 1;
    else
      return ( element_in_int_list(the_data, 
				   the_list->next) );
  
}



/***********************************************/




/* Returns 1 if the list is empty 
   and 0 otherwise */
int empty_int_list(int_linked_list the_list) 
{
  
  if ( the_list == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function empty_list!",
	    "The list is not initialised.");
    return -1;
  }
  
  if ( the_list->next == NULL )
    return 1;
  else 
    return 0;
}



/********************************************/




/* inserts an element at the top
   of the list, immediately 
   after the sentinel,
   returns 1 in case of success 
   and 0 otherwise */
int insert_element_int_list(data the_data,
			    int_linked_list the_list)
{
  int_linked_list temp = the_list;

  if ( the_list == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function insert_element!",
	    "The list is not initialised.");
    return -1; 
  }
  
  /* return 0 if data to insert is 
     already  in the list */
  if ( element_in_int_list(the_data, the_list) )
    return 0;

  /* otherwise, insert element at list top */
  temp = the_list->next;
  the_list->next = malloc(sizeof(int_list_node));
  the_list->next->d = the_data;
  the_list->next->next = temp;
  
  /* increment counter of list elements */
  the_list->d++;

  return 1;
}


/***********************************************/


/* prints all elements of the list */
int print_int_list(int_linked_list the_list)
{

  if ( the_list == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function print_int_list!",
	    "The list is not initialised.");
    return -1; 
  }

  
  if ( the_list->next == NULL ) {
    /* already at the end of list */
    printf("%s\n", "NULL");
    return 0;
  }
  else {
    /* print data in element 
       pointed to by the_list->next 
       and invoke print_int_list 
       on list pointed to by
       the_list->next */
    printf("%d  ", the_list->next->d);
    return (print_int_list(the_list->next));
  }

  
}


/***********************************************/





/* /\* main program to test the functions */
/*    implemented above *\/ */
/* int main() */
/* { */

/*   /\* declare the list*\/ */
/*   linked_list integer_list; */

/*   /\*initialise the list *\/ */
/*   integer_list = create_list(); */

/*   data this_data; */

/*   char delete; */

/*   printf("\nWelcome to this program!\n"); */
/*   /\* prompt user in order to */
/*      add elements to list *\/ */
/*   do { */
/*     printf("Please enter an integer (0 to exit):\n"); */
/*     scanf("%d", &this_data); */
/*     if ( this_data ) */
/*       insert_element(this_data, integer_list); */
/*   } */
/*   while( this_data ); */

/*   /\* count number of elements in the list *\/ */
/*   int list_length = count_element(integer_list); */


/*   /\* print output *\/ */
/*     printf("The list you input is made of %d elements.\n", */
/* 	 list_length); */
/*   printf("The list you input is like the following:\n"); */
/*   print_list(integer_list); */

/*   //fflush(stdin); */
/*   getchar(); */
  
/*   /\* ask the user whether some elements */
/*      of the list should be cancelled *\/ */
/*     printf("Would you like to delete one list element? (y/n)\n"); */
/*   scanf("%c", &delete); */
/*   while ( delete == 'y' ) { */
/*     printf("Please, enter integer to delete from list.\n"); */
/*     scanf("%d", &this_data); */
/*     int element_deleted = */
/*       delete_element(this_data, integer_list); */
/*     if ( element_deleted ) */
/*       printf("The data %d has been deleted from list\n", */
/* 	     this_data); */
/*     else */
/*       printf("%s%d%s\n%s\n", */
/* 	     "The data ", this_data, " is not in the list.", */
/* 	     "No element was deleted from the list."); */
    
/*     printf("The list now is as follows:\n"); */
/*     print_list(integer_list); */
  
/*     if ( empty_list(integer_list) ) { */
/*       printf("Your list is now empty.\n"); */
/*       printf("The program terminates here.\n"); */
/*       break; */
/*     } */

/*     getchar(); */

/*     printf("Would you like to delete one list element? (y/n)\n"); */
/*     scanf("%c", &delete); */
/*   } */


/*   if ( !empty_list(integer_list) ) { */
/*     char find_data; */
/*     getchar(); */
/*     printf("Would you like to find one element in the list? (y/n)\n"); */
/*     scanf("%c", &find_data); */

/*     while ( find_data == 'y' ) { */
/*       printf("Please, enter integer to search in list.\n"); */
/*       scanf("%d", &this_data); */
/*       int data_in_list = element_in_list(this_data, integer_list); */
/*       printf("The result of the search of %d in list is %d\n", */
/* 	     this_data, data_in_list); */
/*       getchar(); */
/*       printf("Would you like to find one element in the list? (y/n)\n"); */
/*       scanf("%c", &find_data); */
/*     } */

/*   } */
  

/*   printf("Thanks for using this program!\n"); */
/*   printf("Hope you enjoyed it! :-)\n"); */

/*   return 0; */
/* } */




/***********************************************/

/* /\* returns data contained in  */
/*    a specific position in the list *\/ */
/* int show_data_int_list(int list_position, */
/* 		       int_linked_list the_list) */
/* { */
  
/*   if ( the_list == NULL ) { */
/*     printf( "\n%s\n%s\n\n",  */
/* 	    "Error in function show_data_int_list!", */
/* 	    "The list is not initialised."); */
/*     return -1;  */
/*   } */
  

  

/* } */



