#! /bin/sh
# Copyright (c) 2005 SUSE LINUX Products GmbH, Nuernberg, Germany
#
# Author: Ladislav Slezak <lslezak@suse.cz>
#
# System script for enabling/disabling IDE DMA mode
# 
# The script takes the first command line parameter and sets the configured DMA
# mode up. If the device is not configured in DEVICES_FORCE_IDE_DMA variable DMA
# status is not changed.
#

HDPARM=/sbin/hdparm
test -x $HDPARM || exit 5

# read values from sysconfig
. /etc/sysconfig/ide

if [ "$DEVICES_FORCE_IDE_DMA" = "" -o $1 = "" ] ; then
    exit 0;
fi


OLDIFS=$IFS
for dev in $DEVICES_FORCE_IDE_DMA ; do
    # Get device and DMA mode

    MODE=""
    DEVICE=""
    OPTIONS=""
    FIRST=0

    # The setting e.g. "/dev/hda:69:-c1:-m16:-u1:-W1:-A1" should be
    # expanded as "hdparm -d 1 -X 69 -c1 -m16 -u1 -W1 -A1 /dev/hda"

    IFS=":"
    for d in $dev ; do
	case $((FIRST++)) in
	0) DEVICE=$d ;;
	1) MODE=$d   ;;
	*) OPTIONS="$OPTIONS$d:"
	esac
    done
    unset d

    if [ "$DEVICE" == "$1" ] ; then
	echo "$DEVICE: setting up IDE DMA mode $MODE"

	# Set DMA mode by hdparm utility
	if [ -z "$MODE" ] ; then
	    echo "Missing DMA mode for device $DEVICE"
	elif [ $MODE = "off" ] ; then
	    $HDPARM -d 0 "$DEVICE"
	elif [ $MODE = "on" ] ; then
	    $HDPARM -d 1 "$DEVICE"
	else
	    $HDPARM -d 1 -X "$MODE" ${OPTIONS:+$OPTIONS} "$DEVICE"
	fi
    fi

    # Reset Internal Field Separator for the outer loop
    IFS=$OLDIFS
done
unset dev

