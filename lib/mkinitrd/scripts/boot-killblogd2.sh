#!/bin/bash
#
#%stage: setup
#%depends: killblogd
#%provides: killprogs
#
#%programs: usleep
#%dontshow
#
##### blogd end2
##
## Really kill blogd this time.
##
## Command line parameters
## -----------------------
##

if test -n "$blogd_pid" ; then
    kill -QUIT "$blogd_pid"
    typeset -i bquit=50
    while test -d "/proc/$blogd_pid" ; do
	usleep 30000
	if ((bquit-- <= 0)) ; then
	    kill -KILL "$blogd_pid"
	    break
	fi
    done
    unset bquit blogd_pid
    if test "$devpts" = "yes" ; then
	umount -t devpts /dev/pts
    fi
fi
