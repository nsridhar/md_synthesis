#!/bin/bash
#%stage: block
#%modules: nfs
#%programs: /sbin/mount.nfs
#%if: "$rootfstype" = "nfs" -o "$need_nfs"
#
##### Network FileSystem
##
## This is where NFS gets mounted.
## If no root= option was given, the root device will be taken from the DHCP-server.
##
## Command line parameters
## -----------------------
##
## root=<server>:/<folder>	the nfs root path
## 

# Prefer NFS root setting via DHCP the fallback provided in config/*.
# So at first, consider the command line (that's why we check for "$cmd_root"
# being empty here. Then consider the DHCP setting. And finally consider the
# fallback via config/*.

if [ -n "$ROOTPATH" -a -z "$cmd_root" ] ; then
  case "$ROOTPATH" in
    iscsi:*)
	;;
    *:*)
	rootfstype="nfs"
	rootdev="$ROOTPATH" ;;
    *)
	if [ -n "$DHCPSIADDR" ]; then
	    rootdev="$DHCPSIADDR:$ROOTPATH"
	    rootfstype="nfs"
	elif [ -n "$DHCPSNAME" ]; then
	    rootdev="$DHCPSNAME:$ROOTPATH"
	    rootfstype="nfs"
	fi ;;
  esac

  if [ -n "$rootdev" ] ; then
      echo >&2 "Using root device ($rootdev) provided via DHCP"
  fi
fi

if [ "$rootfstype" = "nfs" ]; then
	# load the nfs module before using it
	load_modules
	
	if [ -z "$rootdev" ]; then
	    echo "no local root= kernel option given and no root server set by the dhcp server."
	    echo "exiting to /bin/sh"
	    cd /
	    PATH=$PATH PS1='$ ' /bin/sh -i
	fi

	rootfsmod=
	if [ -n "$rootflags" ] ; then
	    rootflags="${rootflags},nolock"
	else
	    rootflags="nolock"
	fi
	# tell boot.rootfsck to skip warning
	ROOTFS_FSCK=0
	export ROOTFS_FSCK
else
	dont_load_modules
fi
