#!/bin/bash
#%stage: boot
#%depends: udev clock
#%programs: showconsole /sbin/blogd
#%if: -x /sbin/blogd
#%dontshow
#
##### blogd start
##
## This script starts blogd if this has not happened before.
##
## Command line parameters
## -----------------------
##

fboot=$(get_param fastboot)
quiet=$(get_param quiet)

if test -z "$fboot" -a -z "$quiet" -a -z "$REDIRECT" ; then
    REDIRECT=$(showconsole 2>/dev/null)
    if test -n "$REDIRECT" ; then
	if test "$devpts" != "yes" ; then
	    mount -t devpts devpts /dev/pts
	    devpts=yes
	fi
	> /dev/shm/initrd.msg
	ln -sf /dev/shm/initrd.msg /var/log/boot.msg
	mkdir -p /var/run
	/sbin/blogd $REDIRECT
    fi
fi
