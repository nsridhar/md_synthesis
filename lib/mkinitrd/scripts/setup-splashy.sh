#!/bin/bash
#
# Copyright (C) 2007 SUSE Linux Products GmbH
#
# Author: Holger Macht <hmacht@suse.de>
#
# This file is released under the GPLv2.
#
#%stage: setup
#

# never even 'return' from a mkinitrd script, which would result in an
# aborted initrd build process
if [ -d /etc/splashy ] && [ -f /usr/sbin/resume ]; then
    # find the current theme
    DUMMY=$(cat /etc/splashy/config.xml)
    THEME=$(echo $DUMMY | perl -p -e 's/<!--.*?-->//g;s/^.*<current_theme>//;s/<\/current_theme>.*$//;')
    if [ -z $THEME -o ! -d /usr/share/splashy/themes/$THEME ]; then
	echo "$0: warning, splashy theme not found in config file, using 'openSUSE'"
	THEME=openSUSE
    fi

    LIBDIR=lib
    if [ "`LANG=C LC_ALL=C file -b /usr/sbin/resume | awk '/^ELF ..-bit/ { print $2 }'`" = "64-bit" ]; then
	LIBDIR=lib64
    fi

    LIBS="\
/usr/$LIBDIR/libdirect-1.4.so.5 \
/usr/$LIBDIR/libdirectfb-1.4.so.5 \
/usr/$LIBDIR/libfusion-1.4.so.5 \
/usr/$LIBDIR/libpng14.so.14 \
/usr/$LIBDIR/libsplashy.so.1 \
/usr/$LIBDIR/libsplashycnf.so.1 \
/$LIBDIR/libglib-2.0.so.0 \
/$LIBDIR/libsysfs.so.2 \
/$LIBDIR/libm.so.6 \
/$LIBDIR/libz.so.1"

    SPLASHY_FILES="
/etc/splashy/config.xml \
/usr/share/splashy/themes/$THEME/*"

    for I in `directfb-config --libs --input=keyboard --imageprovider=jpeg,gif,png --font=ft2,default`; do
	F=`dirname -- $I`/`basename -- $I .o`.so
	if [ -f $F ]; then
	    if [ ! -d $tmp_mnt/`dirname $F` ]; then
		mkdir -p $tmp_mnt/`dirname $F`
            fi
	    cp -dp $F $tmp_mnt/$F
	fi
    done

    for I in $LIBS; do
	[ ! -d $tmp_mnt/`dirname $I` ] && mkdir -p $tmp_mnt/`dirname $I`
	cp -L $I $tmp_mnt/$I
    done

    for I in $SPLASHY_FILES; do
	[ ! -d $tmp_mnt/`dirname $I` ] && mkdir -p $tmp_mnt/`dirname $I`
	cp $I $tmp_mnt/$I
    done

    [ ! -d $tmp_mnt/etc/splashy/themes ] && \
	ln -s /usr/share/splashy/themes $tmp_mnt/etc/splashy/

fi
# never exit with nonzero, so we never cause mkinitrd to abort
# - not having splash during resume is ugly, not booting is evil.
# do not exit here - this script is sourced!
true
