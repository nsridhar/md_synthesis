/* 
   header file for implementation of linked
   list of integers.
   The elements of the list are not sorted.
   In this version, the data field in the 
   sentinel node contains the size (i.e. 
   number of nodes) of the list
*/


#ifndef _INT_LNKD_LIST_HEADER_H_
#define _INT_LNKD_LIST_HEADER_H_

#include <math.h> 
#include <stdlib.h>
#include <stdio.h>


#define assert(expr)  if (!(expr)) {		\
    printf("\n%s%s\n%s%s\n%s%d\n\n",		\
	   "Assertion failed: ",#expr,		\
	   "in file ",__FILE__,			\
	   "in line ",__LINE__);	        \
    exit(1);					\
  }


typedef int data;

  
typedef struct int_list_node{
  data d;
  struct int_list_node *next;
} int_list_node;


/* definition of linked list  */
typedef int_list_node *int_linked_list;






/* returns number of elements
   in the list*/
int count_element_int_list(int_linked_list the_list);


/* Creates and initialises 
   an empty  list
   The first element in the list
   is just a sentinel */
int_linked_list create_int_list();

/* eliminates one element from the list
   returns 1 if the elimination was successful,
   and 0 otherwise */
int delete_element_int_list(data the_data, 
			    int_linked_list the_list);


/* deletes the whole list element by element,
   including sentinel node. the list pointer
   is finally set to NULL 
   IMPORTANT!!!
   Always invoke this function before
   an integer  list created with function
   create_int_list() gets out of scope*/
int delete_int_list(int_linked_list *p_the_list);


/* returns 1 if element is in the list
   and 0 otherwise */
int element_in_int_list(data the_data, 
			int_linked_list the_list);

/* Returns 1 if the list is empty 
   and 0 otherwise */
int empty_int_list(int_linked_list the_list);


/* inserts an element at the top
   of the list, immediately 
   after the sentinel */
int insert_element_int_list(data the_data,
			    int_linked_list the_list);

/* prints all elements of the list */
int print_int_list(int_linked_list the_list);

/* returns data contained in 
   a specific position in the list */
int show_data_int_list(int list_position,
		      int_linked_list the_list);

  

#endif
