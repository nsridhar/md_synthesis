#include "cluster.h"

/* Functions implementing 
   functionalities of cluster 
   data structure */






/* 
   Assign an atom with a chemical type expressed
   by an integer. In case the symbol "this_ion_species"
   is not found in the array "species_name",
   it is added to the the latter array as
   the row species_num+1. The function returns 1. 
   If the symbol "this_ion_species" is found in the array 
   "species_name", the ion is associated with 
   the species corresponding to the index of the 
   array row where "this_ion_species" is found.
   The function returns 0.
   In case an error occurs because of insufficient
   memory allocated for the array species_num,
   the function returns -1.
*/


int assign_ion_with_species(int species_num,
			    int max_species_num,
			    char **species_name,
			    char *this_ion_species)
			    //int *p_ion_type)
{
    
  int iteration_cnt = 0;

  while ( iteration_cnt < species_num ) 
    if ( !strncmp(species_name[iteration_cnt],
		  this_ion_species, 2) ) 
      /* the element has been found in the
	 list of chemical symbols provided */
      return iteration_cnt;
  //return *p_ion_type = iteration_cnt;
  //return 0;
    else 
      iteration_cnt++;
  
  
  /* this ion element is not present in the 
     array "species name" passed on to the 
     function */
  if ( iteration_cnt >= max_species_num ) 
    /* an error occurred, the number of 
       chemical species found exceeds the 
       maximum number of species */
    return -1;
  //return *p_ion_type = -1;
  else
    /* a new chemical species has been 
       found in the cluster */
    return iteration_cnt;
  //return *p_ion_type = iteration_cnt;
    
 
}




/*****************************************************/



int compare_cluster(cluster clust1,
		    cluster clust2)
{

  /* compare ion and chemical
     species number of the 
     two clusters */
  if ( clust1->species_num - 
       clust2->species_num 
       ||
       clust1->unit_num - 
       clust2->unit_num )
    return 0;
  if ( fabs(clust1->prob - 
	    clust2->prob) < 1.0e-5 )
    return 0;
  
  /* compare all ions making up 
     the two clusters one by one */
  int *matched_ion = 
    calloc(clust1->unit_num, sizeof(int));
  for (int i=0; i<clust1->unit_num; i++) {
    int j=-1;
    do { 
      j++;
      if ( !matched_ion[j]
	   &&
	   !strncmp((clust1->species_symbol)
		    [(clust1->unit_type)[i]],
		   (clust2->species_symbol)
		    [(clust2->unit_type)[j]], 2)
	   &&
	   fabs( (clust1->unit_coord)[i][0] -
		 (clust2->unit_coord)[j][0] ) < 1.0e-5
	   
	   &&
	   fabs( (clust1->unit_coord)[i][1] -
		 (clust2->unit_coord)[j][1] ) < 1.0e-5
	   &&
	   fabs( (clust1->unit_coord)[i][2] -
		 (clust2->unit_coord)[j][2] ) < 1.0e-5  ) 
	matched_ion[j] = 1;
    } while ( j < clust2->unit_num 
	      &&
	      !matched_ion[j] ); 
    
    /* if in cluster 2 no ion j has been found 
       which matches ion i in cluster 1,
       then return 0 */
    if ( !matched_ion[j] ) {
      free(matched_ion);
      return 0;
    }

  } /* end of  for (int i=0; i<clust1->unit_num; i++) */

  /* all ions of cluster 1 have been found to 
     have a match with an ion in cluster 2. 
     The two clusters are the same one. Return 1 */
  free(matched_ion);
  return 1;

}






/*****************************************************/



/* make a copy of cluster pointed to by clust2 
   in cluster pointed to by clust1 */
int copy_cluster(cluster clust1, cluster clust2)
{
  
  if ( clust1 == NULL 
       ||
       clust2 == NULL )  {
    printf( "\n%s\n%s\n\n", 
	    "Error in function copy_cluster!",
	    "One of he clusters is not initialized.");
    return -1; 
  }
  

  /* reset clust1 */
  if ( reset_cluster(clust1) ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function copy_cluster!",
	    "Clust1 could not be reset!");
    exit(1); 
  }

  
  clust1->species_num = clust2->species_num;
  clust1->unit_num = clust2->unit_num;
  clust1->prob = clust2->prob;

  /* allocate memory for arrays */ 
  clust1->species_symbol = 
    calloc(clust1->species_num, sizeof(char*));
  for (int i=0; i<clust1->species_num; i++)
    (clust1->species_symbol)[i] = 
      calloc(3, sizeof(char));
  clust1->unit_per_species = 
    calloc(clust1->species_num, sizeof(int));
  clust1->unit_type = 
    calloc(clust1->unit_num, sizeof(int));
  clust1->unit_coord = 
    calloc(clust1->unit_num, sizeof(double*));
  for (int i=0; i<clust1->unit_num; i++)
    (clust1->unit_coord)[i] = 
      calloc(3, sizeof(double)); 
  
  
  /* copy names of chemical symbols */
  for (int i=0; i<clust1->species_num; i++) {
    strncpy(clust1->species_symbol[i],
	    clust2->species_symbol[i], 2);
    clust1->species_symbol[i][2] = '\0';
  }


  /* copy number of ions per 
     each chemical species */
  for (int i=0; i<clust1->species_num; i++)
    clust1->unit_per_species[i] =
      clust2->unit_per_species[i];

  /* copy chemical species of each ion */
  for (int i=0; i<clust1->unit_num; i++)
    clust1->unit_type[i] = 
      clust2->unit_type[i];

  /* assign each ion with a triplet of coordinates */
  for (int i=0; i<clust1->unit_num; i++)
    for(int j=0; j<3; j++)
      clust1->unit_coord[i][j] = 
	clust2->unit_coord[i][j];


  return 0;
}


/*****************************************************/



cluster create_cluster(int species_num,
		       int unit_num,
		       double probability,
		       char **unit_species,
		       double **unit_coord)
{
  
  int 
    assign_ion_with_species(int species_num,
			    int max_species_num,
			    char **species_name,
			    char *this_ion_species);
  
  /* allocate memory for the cluster */
  cluster this_cluster = 
    malloc(sizeof(the_cluster));
  assert( this_cluster != NULL );
  
  this_cluster->species_num = species_num;
  this_cluster->unit_num = unit_num;
  this_cluster->prob = probability;

  /* allocate memory for member arrays */ 
  this_cluster->species_symbol = 
    calloc(species_num, sizeof(char*));
  assert( this_cluster->species_symbol != NULL );
  for (int i=0; i<species_num; i++) {
    (this_cluster->species_symbol)[i] = 
      calloc(3, sizeof(char));
    assert( this_cluster->species_symbol[i] != NULL );
  }
  this_cluster->unit_per_species = 
    calloc(species_num, sizeof(int));
  assert( this_cluster->unit_per_species != NULL );
  this_cluster->unit_type = 
    calloc(this_cluster->unit_num, sizeof(int));
  assert( this_cluster->unit_type != NULL );
  this_cluster->unit_coord = 
    calloc(this_cluster->unit_num, sizeof(double*));
  assert( this_cluster->unit_coord != NULL );
  for (int i=0; i<this_cluster->unit_num; i++) {
    (this_cluster->unit_coord)[i] = 
      calloc(3, sizeof(double)); 
    assert( this_cluster->unit_coord[i] != NULL );
  }

  /* assign each ion with a triplet of coordinates */
  for (int i=0; i<this_cluster->unit_num; i++)
    for(int j=0; j<3; j++)
      (this_cluster->unit_coord)[i][j] = 
	unit_coord[i][j];

  /* cycle over ions constituting the cluster 
     and assign each of them with a type,
      also determine number
     of ions per each chemical type */
  int found_species_num = 0;
  //int found_ion_num = 0;
  for (int i=0; i<this_cluster->unit_num; i++) {
    int this_ion_type = 
      assign_ion_with_species(found_species_num,
			      species_num,
			      unit_species,
			      unit_species[i]);
      
    if ( this_ion_type < 0 ) {
      printf("\n%s\n%s%d%s%s\n%s%s\n%s\n\n",
	     "Error in function create_cluster!",
	     "atom ", i, " of type ", unit_species[i],
	     "has not been assigned with any known ",
	     "chemical species.",
	     "This program is exiting, sorry!");
      exit(1);
    }
    
    /* assign ion i with chemical species */
    this_cluster->unit_type[i] = this_ion_type;

    /* increment number of ions of current species */
    this_cluster->unit_per_species[this_ion_type]++;

    if ( this_ion_type >= found_species_num ) {
      strncpy(this_cluster->species_symbol[this_ion_type], 
	      unit_species[i], 2);
      this_cluster->species_symbol[this_ion_type][2] = 
	'\0';
      found_species_num++;
    }
    
 
  } /* end of cycle over ions */


  /* consistency checks */
  
  /* check that number of chemical species
     corresponds to parameter passed on to
     function */
  if ( found_species_num - species_num ) {
    printf("\n%s\n%d%s\n%s\n%s%d%s\n%s\n",
	   "Error in function create_cluster!",
	   found_species_num, " chemical species have been",
	   "found in this cluster.",
	   "The number of species passed on to function is ",
	   species_num, ".",
	   "This program is exiting, sorry!");
    exit(1);
  }


  /* check that number of ions 
     corresponds to parameter passed on to 
     function */
  int ion_num = 0;
  for (int i=0; i<this_cluster->species_num; i++)
    ion_num += this_cluster->unit_per_species[i];
  if ( ion_num - unit_num ) {
    printf("\n%s\n%d%s\n%s%d%s\n%s\n",
	   "Error in function create_cluster!",
	   ion_num, " ions have been found in this cluster.",
	   "The number of ions passed on to function is ",
	   unit_num, ".",
	   "This program is exiting, sorry!");
    exit(1);
  }

  return this_cluster;
}




/*****************************************************/





cluster create_copy_cluster(cluster input_cluster)
{
  

  cluster this_cluster = 
    malloc(sizeof(the_cluster));
  
  this_cluster->species_num = input_cluster->species_num;
  this_cluster->unit_num = input_cluster->unit_num;
  this_cluster->prob = input_cluster->prob;

  /* allocate memory for arrays */ 
  this_cluster->species_symbol = 
    calloc(this_cluster->species_num, sizeof(char*));
  assert( this_cluster->species_symbol != NULL );
  for (int i=0; i<this_cluster->species_num; i++) {
    (this_cluster->species_symbol)[i] = 
      calloc(3, sizeof(char));
    assert( this_cluster->species_symbol[i] != NULL );
  }
  this_cluster->unit_per_species = 
    calloc(this_cluster->species_num, sizeof(int));
  assert( this_cluster->unit_per_species != NULL );
  this_cluster->unit_type = 
    calloc(this_cluster->unit_num, sizeof(int));
  assert( this_cluster->unit_type != NULL );
  this_cluster->unit_coord = 
    calloc(this_cluster->unit_num, sizeof(double*));
  assert( this_cluster->unit_coord != NULL );
  for (int i=0; i<this_cluster->unit_num; i++) {
    (this_cluster->unit_coord)[i] = 
      calloc(3, sizeof(double)); 
    assert( this_cluster->unit_coord[i] != NULL ); 
  }
  
  /* copy names of chemical symbols */
  for (int i=0; i<this_cluster->species_num; i++) {
    strncpy(this_cluster->species_symbol[i],
	    input_cluster->species_symbol[i], 2);
    this_cluster->species_symbol[i][2] = '\0';
  }


  /* copy number of ions per 
     each chemical species */
  for (int i=0; i<this_cluster->species_num; i++)
    this_cluster->unit_per_species[i] =
      input_cluster->unit_per_species[i];

  /* copy chemical species of each ion */
  for (int i=0; i<this_cluster->unit_num; i++)
    this_cluster->unit_type[i] = 
      input_cluster->unit_type[i];

  /* assign each ion with a triplet of coordinates */
  for (int i=0; i<this_cluster->unit_num; i++)
    for(int j=0; j<3; j++)
      this_cluster->unit_coord[i][j] = 
	input_cluster->unit_coord[i][j];



  return (this_cluster);
}


/*****************************************************/







/* before a cluster gets out of scope,
   free all memory allocated for dynamic 
   arrays;
   also free memory allocated for structure
   the_array;
   finally, assign *p_cluster with NULL */
int delete_cluster(cluster *p_cluster)
{
  cluster this_cluster = *p_cluster;

  if ( this_cluster == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function delete_cluster!",
	    "The cluster is not initialized.");
    return -1; 
  }
  

  for (int i=0; i<this_cluster->species_num; i++)
    free((this_cluster->species_symbol)[i]);
  free(this_cluster->species_symbol);

  free(this_cluster->unit_per_species);
  free(this_cluster->unit_type);

  for (int i=0; i<this_cluster->unit_num; i++)
    free((this_cluster->unit_coord)[i]);
  free(this_cluster->unit_coord);
  
  free(this_cluster);
  *p_cluster = NULL;

  return 0;
}



/*****************************************************/






int find_cluster_cm(cluster clust,
		    double *species_mass,
		    double *cm_coord)
{
  int unit_num = clust->unit_num;
  
  double x_cm = 0.0, 
    y_cm = 0.0, 
    z_cm = 0.0,
    mass_cm = 0.0;
  
  for (int i=0; i<unit_num; i++) {
    double this_ion_mass = 
      species_mass[clust->unit_type[i]];
    x_cm += 
      this_ion_mass*
      clust->unit_coord[i][0];
    y_cm += 
      this_ion_mass*
      clust->unit_coord[i][1];
    z_cm += 
      this_ion_mass*
      clust->unit_coord[i][2];
    mass_cm += this_ion_mass;
  }
  
  x_cm /= mass_cm;
  y_cm /= mass_cm;
  z_cm /= mass_cm;

  cm_coord[0] = x_cm;
  cm_coord[1] = y_cm;
  cm_coord[2] = z_cm;

  return 0;
}





/*****************************************************/


int reset_cluster(cluster this_cluster)
{
  if ( this_cluster == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function reset_cluster!",
	    "Cannot reset uninitialized cluster.");
    return -1; 
  }

  
  for (int i=0; i<this_cluster->species_num; i++)
    free((this_cluster->species_symbol)[i]);
  free(this_cluster->species_symbol);
  
  free(this_cluster->unit_per_species);
  free(this_cluster->unit_type);
  
  for (int i=0; i<this_cluster->unit_num; i++)
    free((this_cluster->unit_coord)[i]);
  free(this_cluster->unit_coord);
  
  
  this_cluster->species_num = 0;
  this_cluster->unit_num = 0;
  this_cluster->prob = 0;

  return 0;
}




/*****************************************************/


/*
  a_(11)	=	cospsi*cosphi - costheta*sinphi*sinpsi	
  a_(12)	=	cospsi*sinphi + costheta*cosphi*sinpsi	
  a_(13)	=	sinpsi*sintheta	
  a_(21)	=	-sinpsi*cosphi - costheta*sinphi*cospsi	
  a_(22)	=	-sinpsi*sinphi + costheta*cosphi*cospsi	
  a_(23)	=	cospsi*sintheta	
  a_(31)	=	sintheta*sinphi	
  a_(32)	=	-sintheta*cosphi	
  a_(33)	=	costheta
  
  0 < phi, psi <= 360
  0 < theta < 180
  for angle convention cfr. 
  <http://mathworld.wolfram.com/EulerAngles.html>
  Rotation of angles phi, theta, psi are
  performed about z-, x'-, and z'-axis, respectively.
  All rotation axes include the origin of 
  coordinate axes.
  Input angles must be expressed in degrees.
*/

int rotate_cluster(cluster clust,
		   double phi,
		   double theta,
		   double psi)
//		   double *species_mass)
{

  if ( clust == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function rotate_cluster!",
	    "The cluster is not initialized.");
    return -1; 
  }

  /* conversion factor from 
     degrees to radians: 2*pi/360 */
  double deg_to_rad = 0.0174532925199;
  /* convert angles from degrees 
     to radians */
  phi *= deg_to_rad;
  theta *= deg_to_rad;
  psi *= deg_to_rad;

  /* calculate sine and cosine
     of all three input angles */
  double sin_phi = sin(phi);
  double cos_phi = cos(phi);
  double sin_theta = sin(theta);
  double cos_theta = cos(theta);
  double sin_psi = sin(psi);
  double cos_psi = cos(psi);
  
  /* calculate rotation matrix */
  double rot_matr[3][3] = 
    { {cos_psi*cos_phi - cos_theta*sin_phi*sin_psi,
       cos_psi*sin_phi + cos_theta*cos_phi*sin_psi,
       sin_psi*sin_theta}, 
      {-sin_psi*cos_phi - cos_theta*sin_phi*cos_psi,
       -sin_psi*sin_phi + cos_theta*cos_phi*cos_psi,
       cos_psi*sin_theta},
      {sin_theta*sin_phi,
       -sin_theta*cos_phi,
       cos_theta} };
  double  new_coord[3], old_coord[3];
  
  
  /* rotate cluster about origin
     of coordinate axes */
  for (int ion=0; ion<clust->unit_num; ion++) {
    for (int i=0; i<3; i++) {
      new_coord[i] = 0.0;
      old_coord[i] = clust->unit_coord[ion][i];
    }
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
	new_coord[i] += rot_matr[i][j]*old_coord[j];
    for (int i=0; i<3; i++)
      clust->unit_coord[ion][i] = new_coord[i]; 
  }
  

  return 0;
}




/*****************************************************/




int translate_cluster(cluster clust,
		      double *trans_vec)
{
  if ( clust == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function translate_cluster!",
	    "Cannot translate uninitialized cluster.");
    return -1; 
  }

  for (int i=0; i<clust->unit_num; i++)
    for (int j=0; j<3; j++)
      clust->unit_coord[i][j] += trans_vec[j];

  
  return 0;
}




/*****************************************************/


