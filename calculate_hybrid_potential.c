#include "header.h"

/* This function calculates the value of 
   the interaction potential between 
   an atom 1 and an atom 2 
   which are a distance (dist12_squared)^1/2 
   apart. The analytical form of the 
   interaction is as follows:
   V(r) = 4*eps*[(sigma/r)^10-(sigma/r)^5],    if r<r1
   V(r) = a0+a1*r+a2*r^2+a3*r^3+a4*r^4+a5*r^5, if r1<=r<r2
   V(r) = 4*eps*(sigma/r)^5                  , if r>r2
   The minimum of the 10-5 potential well is  
   located at r_min=2^(1/5)*sigma~=1.1487*sigma. 
   The value of r1 and r2 is chosen to be 
   equal to 1.1*r_min and 1.2*r_min, respectively.
   The potential is also derived with 
   respect to distance between atom 1 and 2.
   The value pointed to by argument 
   p_dpot12_times_dist is the derivative of 
   the potential with respect to atom 
   distance multiplied by the same distance. 
   (Apologies for this unnecessary complication!) */

int calculate_hybrid_potential(double dist12_squared,
			       double *p_pot12,
			       double *p_dpot12_times_dist,
			       double sigma12,
			       double epsilon12,
			       double r1, double r2,
			       double cutoff_dist12,
			       double *join_coeff,
			       double *truncation_param)
{
  

  /* contribution to potential 
     energy due to interaction 
     between atom 1 and 2 */
  double ener_12 = 0.0; 
  /* derivative of potential 
     with respect to distance 
     between each atom couple 
     multiplied by mutual atom
     distance */
  double potderiv_times_dist = 0.0;
    
  /* calculate distance between  
     atom 1 and 2 */
  double dist_12 = sqrt(dist12_squared);
/*   printf("Calculated distance is %.3lf\n", dist_12); */
  

  /* calculate minimum of 10-5 
     potential well as: 
     r_min = 2^(1/5)*sigma */
  //double r_min = pow(2.0, 0.2)*sigma12;
    /* set values of r1 and r2
     to 1.1*sigma  and 1.2*sigma,
     respectively */
  //double r1 = 1.1*r_min;
  //double r2 = 1.2*r_min;

  /* calculate distances */

  if ( dist_12 < r1 ) {/* use 10-5 potential */ 
    
    
    double sigma_over_r =  sigma12/dist_12;
    double sigma_over_r_5 = pow(sigma_over_r,5);
    double sigma_over_r_10 = pow(sigma_over_r_5,2);
    
    /* calculate contribution to potential 
       energy due to the interaction 
       between atom i and j */
    ener_12 = 4.0*epsilon12
      *(sigma_over_r_10-sigma_over_r_5);
    
    /* Calculate derivative of  
       potential with respect 
       to r_12 and multiply 
       by r_12 */
    potderiv_times_dist = 
      20.0*epsilon12
      *(sigma_over_r_5 - 2.0*sigma_over_r_10);
    
  }
  
  else if ( dist_12 < r2 ) {/* use polinomial 
				   join potential */   

    double coeff[6], 
      dist_pow[6];   
    
    for(int n=0; n<6; n++)
      /* coeff[n] = an, 
	 with 
	 pot(r) = 
	 a0+a1*r+a2*r^2+a3*r^3+...+a5*r^5 */
      coeff[n] = join_coeff[n];
    
    dist_pow[0] = 1;
    for(int n=1; n<6; n++)
      dist_pow[n] = dist_12*dist_pow[n-1];
    
    /* Calculate contribution 
       to potential energy
       due to interaction 
       between atom 1 and 2 */
    for(int n=0; n<6; n++)
      ener_12 += coeff[n]*dist_pow[n]; 	 
    
    /* Calculate derivative 
       of potential 
       with respect to r_ij 
       and multiply by r_ij */
    for (int n=1; n<6; n++) 
      potderiv_times_dist 
	+= n*coeff[n]*dist_pow[n];

  }
  
  else if ( dist_12 >= r2  
	    &&  
	    dist_12 < cutoff_dist12 ) { /* use r^-5 
					   repulsive 
					   potential */

    double sigma_over_r =  sigma12/dist_12;
    double sigma_over_r_5 = pow(sigma_over_r,5);
    
    /* calculate contribution to potential 
       energy due to the interaction 
       between atom i and j */
    ener_12 = 4.0*epsilon12*sigma_over_r_5;
    
    /* Calculate derivative of  
       potential with respect 
       to r_12 */
    potderiv_times_dist = -5.0*ener_12;
  
  }
  
  else if ( dist_12 > cutoff_dist12 ) { /* Interaction outside 
					   cutoff distance. Use 
					   truncation parameters */
    
    double coeff[6], 
      dist_pow[6];
    
    for(int n=0; n<6; n++)
      /* coeff[n] = an, 
	 with 
	 pot(r) = 
	 a0+a1*r+a2*r^2+a3*r^3+...+a5*r^5 */
      coeff[n] = truncation_param[n];
    
    dist_pow[0] = 1;
    dist_pow[1] = dist_12;
    dist_pow[2] = dist12_squared;
    for(int n=3; n<6; n++)
      dist_pow[n] = dist_12*dist_pow[n-1]; 
    
    /* Calculate contribution 
       to potential energy
       due to interaction 
       between atom 1 and 2 */
    for(int n=0; n<6; n++)
      ener_12 += coeff[n]*dist_pow[n]; 	 
    
    /* Calculate derivative 
       of potential 
       with respect to r_ij 
       and multiply by r_ij */
    for (int n=1; n<6; n++) 
      potderiv_times_dist 
	+= n*coeff[n]*dist_pow[n];
  }


  /* "export" calculated 
     quantities to 
     calling function */
  *p_pot12 = ener_12;
  *p_dpot12_times_dist = potderiv_times_dist;
  

  return 0;
}




/**************************************************************/  




/*  Given the analytical form of the 
    interaction is as follows:
    V(r) = 4*eps*[(sigma/r)^10-(sigma/r)^5],    if r<r1
    V(r) = a0+a1*r+a2*r^2+a3*r^3+a4*r^4+a5*r^5, if r1<=r<r2
    V(r) = 4*eps*(sigma/r)^5                  , if r>r2;
    this function calculates the second derivative of
    the potential 
*/
int calculate_hybrid_potential_second_deriv
(double dist_squared,
 double *p_pot_second_deriv,
 double sigma,
 double epsilon,
 double r1, double r2,
 double *join_coeff)
{

  double pot_second_deriv = 0;

  /* calculate distance between  
     atom 1 and 2 */
  double dist = sqrt(dist_squared);


  if ( dist < r1 ) { /* use 10-5 potential */
    
    double sigma_over_r =  sigma/dist;
    double sigma_over_r_5 = pow(sigma_over_r,5);
    double sigma_over_r_10 = sigma_over_r_5*sigma_over_r_5;
    
    pot_second_deriv = 
      20.0*epsilon*
      (22.0*sigma_over_r_10 - 6.0*sigma_over_r_5) / 
      dist_squared;
  
  }
  
  else if ( dist < r2 ) { /* use join polynomial */
  
    pot_second_deriv = 
      2.0*join_coeff[2] + 
      6.0*join_coeff[3]*dist +
      12.0*join_coeff[4]*dist_squared +
      20.0*join_coeff[5]*dist_squared*dist; 
  
  }

  else { /* use 1/r^5 repulsive potential */
    
    double sigma_over_r =  sigma/dist;
    double sigma_over_r_5 = pow(sigma_over_r,5);

    pot_second_deriv = 
      120.0*epsilon*sigma_over_r_5/dist_squared;
  
  }
  
  
  /* export second derivative to
     calling function */
  *p_pot_second_deriv = pot_second_deriv;

  return 0;
}



/**************************************************************/  


/* 
   Given the analytical form of the 
   interaction is as follows:
   V(r) = 4*eps*[(sigma/r)^10-(sigma/r)^5],    if r<r1
   V(r) = a0+a1*r+a2*r^2+a3*r^3+a4*r^4+a5*r^5, if r1<=r<r2
   V(r) = 4*eps*(sigma/r)^5                  , if r>r2;
   this functions calculates r1 and r2 first as 
   1.1*r_min and 1.2*r_min, respectively,
   where r_min is the minimum of the 10-5 potential well 
   located at r_min=2^(1/5)*sigma~=1.1487*sigma. 
   Subsequently, the function calculates the coefficients
   a_i of the join polynomial between r1 and r2,
   in such a way that the potential and its first
   derivative are continuous in r1 and r2.
*/
int calculate_join_params_hybrid_pot(double sigma,
				     double epsilon,
				     double *p_r1, 
				     double *p_r2,
				     double *join_coeff)
{  
  
  /* prototype of function calculating
     the factorial of an integer number */
  extern int fact(int n);
  

  /* calculate minimum of 10-5 
     potential well as: 
     r_min = 2^(1/5)*sigma */
  double r_min = pow(2.0, 0.2)*sigma;
  
  double r1 = 1.1*r_min;
  double r2 = 1.2*r_min;

  
  /* the linear system to solve is of 6 equations 
     in 6 unknowns:
     P(r1) = V_10_5(r1) 
     P'(r1) = V_10_5'(r1)
     P''(r1) = V_10_5''(r1)
     P(r2) = V_5(r2)
     P'(r2) = V_5'(r2)
     P''(r2) = V_5''(r2);
     where:
     V_10_5 = 4*eps*[(sigma/r)^10-(sigma/r)^5];
     V_5 =  4*eps*(sigma/r)^5;
     P(r) = a0 + a1*r + a2*r^2 + a3*r^3 + a4*r^4 + a5*r^5 
     The solution to find is the vector {a0,a1,a2,a3,a4,a5}
  */

  /* declare arrays of coefficients and 
     allocate memory for it */
  double  **coeff_matrix = calloc(6, sizeof(double*));
  for (int cnt=0; cnt<6; cnt++)
    coeff_matrix[cnt] = calloc(6, sizeof(double));
  /* declare arrays of known terms on 
     the right-hand-side of the linear system
     and allocate memory */
  double **rhs_vect = calloc(6, sizeof(double*));
  for (int cnt=0; cnt<6; cnt++)
    rhs_vect[cnt] = calloc(1, sizeof(double));
  
  /* calculate V_10_5 potential in r1,
     along with its first and second
     derivative */
  double sigma_over_r1 = sigma/r1;
  double sigma_over_r1_5 = pow(sigma_over_r1,5);
  double sigma_over_r1_10 = 
    sigma_over_r1_5*sigma_over_r1_5;
  double pot_r1 = 4.0*epsilon*(sigma_over_r1_10 - sigma_over_r1_5);
  double pot_first_deriv_r1 = 
    20.0*epsilon*(sigma_over_r1_5 - 2.0*sigma_over_r1_10)/r1;
  double pot_second_deriv_r1 = 
    20.0*epsilon*(22.0*sigma_over_r1_10 - 6.0*sigma_over_r1_5)/(r1*r1);
   /* calculate V_5 potential in r2,
      along with first and second 
      derivative of */
  double sigma_over_r2 = sigma/r2;
  double sigma_over_r2_5 = pow(sigma_over_r2, 5);
  double pot_r2 = 4.0*epsilon*sigma_over_r2_5;
  double pot_first_deriv_r2 = -20.0*epsilon*sigma_over_r2_5/r2;
  double pot_second_deriv_r2 = 
    120.0*epsilon*sigma_over_r2_5/(r2*r2);
  
  /* construct (6 x 6)-matrix of coefficients 
     of linear system */
  for(int row_ind=0; row_ind<3; row_ind++)
    for(int col_ind=0; col_ind<6; col_ind++) {
      if ( col_ind < row_ind )
	      coeff_matrix[row_ind][col_ind] = 0.0;
      else
	coeff_matrix[row_ind][col_ind] = 
	  fact(col_ind)/fact(col_ind-row_ind)
	  *pow(r1,col_ind-row_ind);
    }
  for(int row_ind=0; row_ind<3; row_ind++)
    for(int col_ind=0; col_ind<6; col_ind++) {
      if ( col_ind <  row_ind )
	coeff_matrix[row_ind+3][col_ind] = 0.0;
      else
	coeff_matrix[row_ind+3][col_ind] = 
	  fact(col_ind)/fact(col_ind-row_ind)
	  *pow(r2,col_ind-row_ind);
    }
  
  /* construct vector of known terms on the 
     right-hand-side of the equality */
  rhs_vect[0][0] = pot_r1;
  rhs_vect[1][0] = pot_first_deriv_r1;
  rhs_vect[2][0] = pot_second_deriv_r1;
  rhs_vect[3][0] = pot_r2;
  rhs_vect[4][0] = pot_first_deriv_r2;
  rhs_vect[5][0] = pot_second_deriv_r2;;
  
  /* solve linear system and find 
     polynomial coefficients */
  gauss_jordan(coeff_matrix, rhs_vect, 6, 1);
  

  /* copy solution in 
     array of coefficients
     of join polynomial */
  for (int i=0; i<6; i++)
    join_coeff[i] = rhs_vect[i][0];

  /* export calculated values
     of r1 and r2 to calling function */
  *p_r1 = r1;
  *p_r2 = r2;


  /* deallocate memory */
  for (int cnt=0; cnt<6; cnt++)
    free(coeff_matrix[cnt]);
  free(coeff_matrix);
  for (int cnt=0; cnt<6; cnt++)
    free(rhs_vect[cnt]);
  free(rhs_vect);

  return 0;
}





