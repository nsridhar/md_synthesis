#include "header.h"

/* This function performs some tasks on the 
   initial configuration. 
   First of  all, this function translates
   the initial structure along the x-, y-direction 
   in such a way that the geometrical 
   centre has coordinate of 0
   across the xy-plane. It is assumed
   that periodic boundary conditions are 
   applied along the x- and y-direction.
   If required, the structure is also translated
   along the vertical z-direction so that 
   the highest point in the substrate
   has z-coordinate of 0.
   The function also finds the particle positions
   in the structure with smallest x-, y-, and
   z-coordinate, respectively. It also
   determines the z-coordinate of the highest 
   point in the solid phase.
   Finally, some checks are performed in order to ensure
   that the box sizes provided are consistent 
   with the actual structure dimensions. */
int set_initial_structure(int particle_num,
			  int *part_species,
			  int translate_structure_vertically,
			  double bond_dist[][n_max_type],
			  double boxx_dim, 
			  double boxy_dim, 
			  double boxz_dim,
			  //double *p_min_x,
			  //double *p_min_y,
			  double *p_min_z,
			  double *p_solid_top_coord,
			  double *p_init_substrate_surf_z_pos,
			  double *x_pos,
			  double *y_pos,
			  double *z_pos)
{

 
  
  double min_x = x_pos[0], max_x = x_pos[0];
  double min_y = y_pos[0], max_y = y_pos[0];
  double min_z = z_pos[0], max_z = z_pos[0];
  double solid_top_coord = z_pos[0];
  double init_substrate_surf_z_pos = 
    *p_init_substrate_surf_z_pos;

  double x_centre, y_centre;

  double x_max_len, y_max_len, z_max_len;
  
  int *particle_in_substrate = 
    malloc(particle_num*sizeof(int));
  assert( particle_in_substrate != NULL );
  for (int i=0; i<particle_num; i++)
    particle_in_substrate[i] = -1;
  
   /* find which atoms are in solid
     and in gas phase */
  label_substrate_atoms(particle_num, 
			part_species,
			particle_in_substrate,
			bond_dist,
			boxx_dim, boxy_dim,
			x_pos, y_pos, 
			z_pos);

  
  /* find extrema of structure,
     as well as highest and lowest point 
     amongst all atoms in the solid phase. */
  for ( int i=1; i<particle_num; i++){
    
    if( x_pos[i] > max_x )
      max_x = x_pos[i];
    if ( x_pos[i] < min_x )
      min_x = x_pos[i];
    if ( y_pos[i] > max_y )
      max_y = y_pos[i];
    if ( y_pos[i] < min_y )
      min_y = y_pos[i];
    if ( z_pos[i] > max_z )
      max_z = z_pos[i];
    if ( z_pos[i] < min_z )
      min_z = z_pos[i]; 
    if ( particle_in_substrate[i] 
	 &&
	 z_pos[i] > solid_top_coord )
      solid_top_coord = z_pos[i];
    
  }
  assert( solid_top_coord <= max_z );
  
  /* calculate maximum distances
     among particles in all three
     directions */
  x_max_len = max_x - min_x;
  y_max_len = max_y - min_y;
  z_max_len = max_z - min_z;
  
  
  /* make sure simulation box dimensions are 
     compatible with distances among atoms */
  if ( x_max_len > boxx_dim ) {
    printf("\n%s\n%s\n%s%.3lf%s\n%s%.3lf%s\n%s\n\n",
	   "Fatal error in function set_initial_structure.",
	   "The length of the simulation unit cell along x",
	   "has been set equal to ", boxx_dim, " Angstr",
	   "whereas the maximum distance among all atoms is ",
	   x_max_len, " Angstr.",
	   "It is not possible to continue, sorry!");
    exit(1);
  }
  if ( y_max_len > boxy_dim ) {
    printf("\n%s\n%s\n%s%.3lf%s\n%s%.3lf%s\n%s\n\n",
	   "Fatal error in function set_initial_structure.",
	   "The length of the simulation unit cell along y",
	   "has been set equal to ", boxy_dim, " Angstr",
	   "whereas the maximum distance among all atoms is ",
	   y_max_len, " Angstr.",
	   "It is not possible to continue, sorry!");
    exit(1);
  }
  if ( z_max_len > boxz_dim ) {
    printf("\n%s\n%s\n%s%.3lf%s\n%s%.3lf%s\n%s\n\n",
	   "Fatal error in function set_initial_structure.",
	   "The length of the simulation unit cell along z",
	   "has been set equal to ", boxz_dim, " Angstr",
	   "whereas the maximum distance among all atoms is ",
	   z_max_len, " Angstr.",
	   "It is not possible to continue, sorry!");
    exit(1);
  }
  

  /* calculate coordinates of
     centre of structure 
     across xy-plane */
  x_centre = (max_x+min_x)/2.0;
  y_centre = (max_y+min_y)/2.0;

  

 /* translate along directions where 
     periodic boundary conditions 
     are applied */
  /* if option is set to 1, translate
     structure vertically so that 
     highest point in the substrate
     lies on the z=0 plane */ 
  for ( int i=0; i<particle_num; i++){
    x_pos[i] -= x_centre;
    y_pos[i] -= y_centre;
    if ( translate_structure_vertically )
      z_pos[i] -= solid_top_coord;
  }
  if ( translate_structure_vertically ) {
    min_z -= solid_top_coord;
    init_substrate_surf_z_pos -= 
      solid_top_coord;
    solid_top_coord = 0.0;
  }

    


  /* "export" calculated quantities 
     to calling function */
  *p_min_z = min_z;
  *p_solid_top_coord = solid_top_coord;
  *p_init_substrate_surf_z_pos = 
    init_substrate_surf_z_pos;

  free(particle_in_substrate);

  return 0;
}
