#include "header.h"


int update_nbr_list(int num_species,
		    int *atoms_per_species, int *atom_species,
		    int *neighbour_num, int neighbour_list[][n_max_atom],
		    int *verlet_nbr_num, int verlet_nbr_list[][n_max_atom],
		    double potential_distance[][n_max_type],
		    double x_box, double y_box, double z_box,
		    double distance_x[][n_max_atom], 
		    double distance_y[][n_max_atom],
		    double distance_z[][n_max_atom],
		    double *x, double *y, double *z)		
{

  int atom_num = 0;
  for (int i=0; i<num_species; i++)
    atom_num += atoms_per_species[i];
  
  /*Set to 0 Verlet neighbour lists of all atoms*/
  for(int i=0; i<atom_num; i++) {
    //for(int j=0; j<neighbour_num[i]; j++)
    for (int j=0; j<n_max_atom; j++)
      neighbour_list[i][j] = -1;
    neighbour_num[i] = 0;
  }
  
  /*Set to 0 bond lists of all atoms*/
  //for(int i=0; i<atom_num; i++) {
  //for(int j=0; j<n_max_atom; j++)
  //  bond_list[i][j] = -1;
  //bond_number[i] = 0;
  //}
  
  int at_lab = -1;
  /*update neighbour lists of atoms*/
  for(int i=0; i<atom_num; i++) {
    /*consider interactions between atom i and other atoms */
    for(int j=0; j<verlet_nbr_num[i]; j++) {
      
      
      //printf("i is %d, j is %d\n",i,j);
      at_lab = verlet_nbr_list[i][j];
      //printf("i is %d and at_lab is %d\n",
      //     i, at_lab);
      assert(at_lab>=0 && at_lab<n_max_atom);
      
      if (at_lab<i) { /*consider each couple of atoms only 
			once in the for cycle*/ 
	
	/*Calculate interatomic distances*/
	distance_x[i][at_lab] = x[i]-x[at_lab];
	distance_y[i][at_lab] = y[i]-y[at_lab];
	distance_z[i][at_lab] = z[i]-z[at_lab];
	
	/*Apply periodic boundary conditions on simulation box*/
	distance_x[i][at_lab] -= 
	  anint(distance_x[i][at_lab]/x_box)*x_box;
	distance_y[i][at_lab] -= 
	  anint(distance_y[i][at_lab]/y_box)*y_box;
	if( fabs(distance_x[i][at_lab]) > x_box/2.0 ) {
	  printf("%s\n%s%d%s%d%s%12.10lf\n%s%12.10lf\n%s\n",
		 "A fatal error occurred in function update_nbr_list.",
		 "x distance between atom ",i, " and ", at_lab, 
		 " has been found to be ", distx[i][at_lab],
		 "Half boxx is ", x_box/2.0,
		 "It is not possible to continue, sorry!");
	  exit(1);
	}
	if( fabs(distance_y[i][at_lab]) > y_box/2.0 ) {
	  printf("%s\n%s%d%s%d%s%12.10lf\n%s%12.10lf\n%s\n",
		 "A fatal error occurred in function update_nbr_list.",
		 "x distance between atom ",i, " and ", at_lab, 
		 " has been found to be ", distance_y[i][at_lab],
		 "Half boxx is ", y_box/2.0,
		 "It is not possible to continue, sorry!");
	  exit(1);
	}
  
	
	/*use symmetry to construct distance matrix*/
	distance_x[at_lab][i] = -distance_x[i][at_lab];
	distance_y[at_lab][i] = -distance_y[i][at_lab];
	distance_z[at_lab][i] = -distance_z[i][at_lab];
	//for(int coord=0; coord<3; coord++)
	//(atom_array+k)->dist[0][i][coord] = -(atom_array+i)->dist[0][k][coord];
	
	
	/*calculated squared distance between atom i and j*/
	double dist_ik = distance_x[i][at_lab]*distance_x[i][at_lab] + 
	  distance_y[i][at_lab]*distance_y[i][at_lab] +
	  distance_z[i][at_lab]*distance_z[i][at_lab];

	/* chech whether atom at_lab is neighbour to atom i */
	if ( dist_ik <= potential_distance[atom_species[i]][atom_species[at_lab]]*
	     potential_distance[atom_species[i]][atom_species[at_lab]] )  {
	  neighbour_num[i]++;
	  neighbour_list[i][neighbour_num[i]-1] = at_lab;
	  neighbour_num[at_lab]++;
	  neighbour_list[at_lab][neighbour_num[at_lab]-1] = i;
	}
       
	/* check whether atom at_lab forms a bond with atom i */
	//if ( dist_ik < 
	// bond_dist[atom_species[i]][atom_species[at_lab]]* 
	// bond_dist[atom_species[i]][atom_species[at_lab]] ) {
	//bond_number[i]++;
	//bond_list[i][bond_number[i]-1] = at_lab;
	//bond_number[at_lab]++;
	//bond_list[at_lab][bond_number[at_lab]-1] = i;
	//}   
	

      } /* end of if (at_lab<i) */
    } /* end of  for(int j=0; j<verlet_nbr_num[i]; j++) */
  } /* end of for(int i=0; i<atom_num; i++)   */
  
  
  
/*   /\* mark atom as belonging to substrate (atom_state = 0)  */
/*      in case it forms bonds with other substrate atoms,  */
/*      otherwise the atom  is marked as gas phase atom  */
/*      (atom_state = 1) *\/ */
/*   if ( atom_state[at_lab] == 0 ) */
/*     atom_state[i] = 0; */
/*   else */
/*     atom_state[i] = 1; */
  


  /*print neighbour list of all atoms 
    for debugging purposes */
  //for (int i=0; i<atom_num; i++) {
  //printf("Nbrs of atom %d is %d\n", i, neighbour_num[i]);
  //printf("Nbr list of atom %d:\n",i);
  //for (int j=0; j<neighbour_num[i]; j++)
  //  printf("%d ",neighbour_list[i][j]);
  //printf("\n");
  //}
  
  
  return(0);
}
