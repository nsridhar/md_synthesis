//#include "log_file.h"
#include "header.h"

int write_force(char *file_name, int initial, int atom_number)
{
  FILE *p_output_file;

  //*p_output_file = file_name;

  if( (p_output_file=fopen(file_name,"w")) == NULL )    
    {
      printf("A fatal error occured while opening file %s\n",file_name);
      printf("Cannot continue, sorry!\n");
      exit(1);
    }

  for (int i=initial; i<atom_number; i++)
    fprintf(p_output_file,"%6d %12.4f %12.4f %12.4f\n",i,fx[i],fy[i],fz[i]);

  fclose(p_output_file);

  return(0);

}
