#include "header.h"


/*calculates particle displacement*/






int therma(int num_species, int *atoms_per_species,
	   int *atom_species, double *atom_mass,
	   int *fix_atom_label,
	   double x_box, double y_box, double z_box, 
	   double max_z_coord,
	   //double slab_height, 
	   //double min_z,
	   double time_interval,
	   int this_block, int this_step,
	   int single_adatom_run, 
	   int *adatom_x_cross, int *adatom_y_cross,
	   double *force_x, double *force_y, double *force_z,
	   //double *displ_x, double *displ_y, double *displ_z,
	   double *vel_x, double *vel_y, double *vel_z,
	   double *x_coord, double *y_coord, double *z_coord)
{
  double conversion_factor =
    joule_per_ev/(kg_per_amu*metre_per_angstrom*metre_per_angstrom);
  
  double multipl_fact = 
    conversion_factor*0.5*time_interval*time_interval;
  
  double particle_multipl_fact = 0.0;
  //double displ_x, displ_y, displ_z;

  
  
  int atom_num = 0;
  for (int i=0; i<num_species; i++)
    atom_num += atoms_per_species[i];

  /*All lenghts are expressed in Angstroms*/
  
  /*Update positions of gas phase atoms*/
  for(int i=0; i<atom_num; i++) {
  
    if ( !fix_atom_label[i] ) {
  
      /*
	Calculate infinitesimal displacement of particle i
	displ = (1/2)*(dt)^2*f(t)/m + v(t)*dt 
	and add it to position of particle i
	Position update in Velocity-Verlet algorithm
	is given by following equation:
	r(t+dt) = r(t) +  (1/2)*(dt)^2*f(t)/m + v(t)*dt 
      */
      particle_multipl_fact = 
	multipl_fact/atom_mass[atom_species[i]];
      x_coord[i] += 
	particle_multipl_fact*force_x[i] + 
	vel_x[i]*time_interval;
      y_coord[i] += 
	particle_multipl_fact*force_y[i] + 
	vel_y[i]*time_interval;
      double displ_z = 
	particle_multipl_fact*force_z[i] + 
	vel_z[i]*time_interval;
      
      /*apply reflecting barrier on the top of simulation box*/
      //if( (z_coord[i] + displ_z) >= z_box  ) {
      if( z_coord[i]+displ_z >= max_z_coord  ) {
	/* invert z component of velocity 
	   and calculate displacement
	   along z axis as if particle bounced 
	   elastically onto upper boundary of 
	   simulation box */
	vel_z[i] *= -1.0;
	z_coord[i] -= displ_z;
      }
      else
	z_coord[i] += displ_z;
      
      
      
      /* Apply periodic boundary conditions on x and y direction */
      if( single_adatom_run   &&   i == atom_num-1  )
	*adatom_x_cross += (int)anint(x_coord[i]/x_box);
      x_coord[i] -= anint(x_coord[i]/x_box)*x_box;
      if( (fabs(x_coord[i])) > ((x_box/2)+1e-10) ) {
	printf("\n%s\n%s%d%s%d%s\n%s%d%s%.5lf\n%s%.5lf\n%s\n\n",
	       "A fatal error occurred in function therma",
	       "during block ", this_block, ", step ", this_step, ".",
	       "x position of atom ",i, " has been found to be ", x_coord[i],
	       "Half boxx is ", x_box/2.0,
	       "It is not possible to continue, sorry!");
	exit(1);
      }
      if( single_adatom_run  &&  i == atom_num-1 )
	*adatom_y_cross += (int)anint(y_coord[i]/y_box);
      y_coord[i] -= anint(y_coord[i]/y_box)*y_box;
      if( (fabs(y_coord[i])) > ((y_box/2)+1e-10) ) {
	printf("\n%s\n%s%d%s%d%s\n%s%d%s%.5lf\n%s%.5lf\n%s\n\n",
	       "A fatal error occurred in function therma",
	       "during block ", this_block, ", step ", this_step, ".",
	       "y position of atom ",i, " has been found to be ", 
	       y_coord[i],
	       "Half boxx is ", y_box/2.0,
	       "It is not possible to continue, sorry!");
	exit(1);
      }
      
    
      //if( (z[i] > z_box)  ||
      //(z[i] < -1.2*slab_height) ) {
      if  ( z_coord[i] > max_z_coord ) {
	printf("\n%s\n%s%d%s%d%s\n%s%d%s%.8lf%s\n%s%.8lf%s\n",
	       "A fatal error occurred in function therma,",
	       "during block ", this_block, ", step ", this_step, ".",
	       "z position of atom ", i, " has been found to be ", 
	       z_coord[i], ".",
	       "Maximum z-coordinate is ", max_z_coord, 
	       ".");
	printf("Force on atom %d is %lf, %lf, %lf\n",
	       i, force_x[i], force_y[i], force_z[i]);
	printf("Velocity of atom %d is %lf, %lf, %lf\n",
	     i, vel_x[i], vel_y[i], vel_z[i]);
	printf("Position of atom is: %lf, %lf, %lf\n",
	       x_coord[i], y_coord[i], z_coord[i]);
	printf("Calculated displacement along z: %lf.",
	     displ_z); 
	printf("%s\n\n",
	       "It is impossible to continue, sorry!");
	exit(1);
      }
      
      
      
    } /* end of  if ( !fix_atom_label[i] ) */
    
    
  }  /* end of for(int i=0; i<atom_num; i++)  */
  
  
  
  return 0;
  
}








/********************************************************************/













/* int therma_old(int num_species, int *atoms_per_species, */
/* 	       int *atom_species, double *atom_mass, */
/* 	       //int *fix_atom_label, */
/* 	       double x_box, double y_box, double z_box,  */
/* 	       double slab_height,  */
/* 	       double min_z, */
/* 	       double time_interval, */
/* 	       int this_block, int this_step, */
/* 	       int single_adatom_run,  */
/* 	       int *adatom_x_cross, int *adatom_y_cross, */
/* 	       double *force_x, double *force_y, double *force_z, */
/* 	       double *displ_x, double *displ_y, double *displ_z, */
/* 	       double *vel_x, double *vel_y, double *vel_z, */
/* 	       double *x, double *y, double *z) */
/* { */
/*   double conversion_factor = */
/*     joule_per_ev/(kg_per_amu*metre_per_angstrom*metre_per_angstrom); */
  
/*   int atom_num = 0; */
/*   for (int i=0; i<num_species; i++) */
/*     atom_num += atoms_per_species[i]; */

/*   /\*All lenghts are expressed in Angstroms*\/ */

/*   /\*Update positions of gas phase atoms*\/ */
/*   for(int i=0; i<atom_num; i++) { */
    
/*     /\*Calculate infinitesimal displacement of each particle*\/ */
/*     /\*displ = (1/2)*(dt)^2*f(t)/m + v(t)*dt *\/ */
/*     displ_x[i] =  */
/*       conversion_factor*0.5*time_interval*time_interval*force_x[i] /  */
/*       atom_mass[atom_species[i]] +  */
/*       vel_x[i]*time_interval; */
/*     displ_y[i] =  */
/*       conversion_factor*0.5*time_interval*time_interval*force_y[i] /  */
/*       atom_mass[atom_species[i]] + vel_y[i]*time_interval; */
/*     displ_z[i] =  */
/*       conversion_factor*0.5*time_interval*time_interval*force_z[i] /  */
/*       atom_mass[atom_species[i]] + vel_z[i]*time_interval; */
  
/*     /\*apply reflecting barrier on the top of simulation box*\/ */
/*     if( (z[i] + displ_z[i]) >= z_box  ) { */
/*       /\* invert z component of velocity  */
/* 	 and calculate displacement */
/* 	 along z axis as if particle bounced  */
/* 	 elastically onto upper boundary of  */
/* 	 simulation box *\/ */
/*       vel_z[i] *= -1.0; */
/*       //displ_z[i] += 2*(z_box-z[i]) - displ_z[i]; */
/*       displ_z[i] *= -1.0; */
/*     } */
    
/*     /\* freeze particle if it lies far below slab *\/ */
/*     //if ( z[i] < min_z-0.5*slab_thickness ) { fix_atom_label[i] = 1; */
/*     //force_x[i] = force_y[i] = force_z[i] = 0.0; vel_x[i] = vel_y[i] */
/*     //= vel_z[i] = 0.0; displ_x[i] = displ_y[i] = displ_z[i] = 0.0;  */
/*     //} */


/*     /\* advance positions by adding displacemente to previous position *\/ */
/*     /\* r(t+dt) = r(t) +  (1/2)*(dt)^2*f(t)/m + v(t)*dt   *\/ */
/*     x[i] += displ_x[i]; */
/*     y[i] += displ_y[i]; */
/*     z[i] += displ_z[i]; */
    
/*     /\* Apply periodic boundary conditions on x and y direction *\/ */
/*     if( single_adatom_run   &&   i == atom_num-1  ) */
/*       *adatom_x_cross += (int)anint(x[i]/x_box); */
/*     x[i] -= anint(x[i]/x_box)*x_box; */
/*     if( (fabs(x[i])) > ((x_box/2)+1e-10) ) { */
/*       printf("\n%s\n%s%d%s%d%s\n%s%d%s%.5lf\n%s%.5lf\n%s\n\n", */
/* 	     "A fatal error occurred in function therma", */
/* 	     "during block ", this_block, ", step ", this_step, ".", */
/* 	     "x position of atom ",i, " has been found to be ", x[i], */
/* 	     "Half boxx is ", x_box/2.0, */
/* 	     "It is not possible to continue, sorry!"); */
/*       exit(1); */
/*     } */
/*     if( single_adatom_run  &&  i == atom_num-1 ) */
/*       *adatom_y_cross += (int)anint(y[i]/y_box); */
/*     y[i] -= anint(y[i]/y_box)*y_box; */
/*     if( (fabs(y[i])) > ((y_box/2)+1e-10) ) { */
/*       printf("\n%s\n%s%d%s%d%s\n%s%d%s%.5lf\n%s%.5lf\n%s\n\n", */
/* 	     "A fatal error occurred in function therma", */
/* 	     "during block ", this_block, ", step ", this_step, ".", */
/* 	     "y position of atom ",i, " has been found to be ", y[i], */
/* 	     "Half boxx is ", y_box/2.0, */
/* 	     "It is not possible to continue, sorry!"); */
/*       exit(1); */
/*     } */
    
    
/*     //if( (z[i] > z_box)  || */
/*     //(z[i] < -1.2*slab_height) ) { */
/*     if  ( z[i] > z_box ) { */
/*       printf("%s\n%s%d%s%d%s\n%s%d%s%.8lf\n%s%.8lf\n%s%.8lf\n%s\n", */
/* 	     "A fatal error occurred in function therma,", */
/* 	     "during block ", this_block, ", step ", this_step, ".", */
/* 	     "z position of atom ", i, " has been found to be ", z[i], */
/* 	     "Simulation cell height is ", z_box, */
/* 	     "Slab thickness is ", slab_height, */
/* 	     "It is not possible to continue, sorry!"); */
/*       printf("Force on atom %d is %lf, %lf, %lf\n", */
/* 	     i, force_x[i], force_y[i], force_z[i]); */
/*       printf("Velocity of atom %d is %lf, %lf, %lf\n", */
/* 	     i, vel_x[i], vel_y[i], vel_z[i]); */
/*       printf("Position of atom is: %lf, %lf, %lf\n", */
/* 	     x[i], y[i], z[i]); */
/*       printf("Calculated displacement: %lf, %lf, %lf\n", */
/* 	     displ_x[i], displ_y[i], displ_z[i]);  */
/*       exit(1); */
/*     } */
    
    
    
      

    
/*   }  /\* end of for(int i=0; i<atom_num; i++)  *\/ */
      
  
 
  
  
  
/*   return(0); */
  
/* } */
