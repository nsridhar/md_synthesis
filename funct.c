#include "header.h"
#include <stdarg.h>
#include <ctype.h>


/* double anint(double x) */
/* /\* returns floor(x) if 0 <= x-floor(x) <= 1/2 *\/ */
/* /\* returns ceil(x) if 0 <= ceil(x)-x < 1/2    *\/ */
/* { */
/*   double xlow = floor(x); */
/*   double xup = ceil(x); */
  
/*   if ( (x-xlow) <= (xup-x) ) */
/*     return (xlow); */
/*   else  */
/*     return (xup); */

/* } */




/* double anint(double x) */
/* /\* returns floor(x) if 0 <= x-floor(x) <= 1/2 *\/ */
/* /\* returns ceil(x) if 0 <= ceil(x)-x < 1/2    *\/ */
/* { */

/*   return ( (int)(x < 0 ? x-0.5 : x+0.5) ); */

/* } */


char *int_to_string(int n)
{
  char *str; 
  int str_len=0, exp=0;  
  
  while( n/(int)pow(10,exp++) > 0 ) 
    str_len++;
  
  str = (char *)malloc(str_len*sizeof(char));
  
  for(int i=0; i<str_len; i++) {
    *(str+i) =  n/(int)pow(10,str_len-i-1) + 48; 
    /*Adding 48 to a digit converts it into a printable ASCII character*/
    n %= (int)pow(10,str_len-i-1);
  }
  
  *(str+str_len) = '\0';
  
  return(str);
  
}


/*breaks a string into tokens, by using a blank space*/
/*as a separator between two contiguous tokens*/
int break_line(char* string, char** token)
{

  int token_num = 0;
  char *delimiter = " ";

  token[0] = strtok(string, delimiter);
  token_num++;
  //printf("token[%d] is %s\n",0,token[0]);

  int j=0;
  while ( token[j] != NULL ) {   
    token[++j] = strtok(NULL, delimiter);
    token_num++;
    //printf("token[%d] is %s\n",j,token[j]);
  } 

  //printf("break_line is returning %d\n",token_num);
  return token_num;
 

}


/* calculates the factorial of an integer number*/
int fact(int n)
{

  return n > 1  ?  n*fact(n-1) : 1;
  //if ( n <= 0 )
  //return 1;
  //else 
  //return n*fact(n-1);

}





int assign_variable(const char* input_string, char* format, ...)
{
 
  int converted = 0;
 
  //char* string_stream;
 
  va_list variable_list;
  
  //string_stream = malloc(sizeof(char)*strlen(string));
  
  //strcpy(string_stream,string);

  va_start(variable_list,format);

  while ( *format != '\0' ) {
    if ( *format++ == '%' )
      switch ( *format ) {
      case 's':
	{
	  char* dstring = va_arg(variable_list,char*);
	  
	  while ( *input_string == ' ' ) /*disregard leading blank spaces*/
	    *input_string++;
	  
	  while( (*input_string != '\0')  &&  (!isspace(*input_string)) ) {
	    *dstring++ = *input_string++;
	    //*dstring = *input_string;
	    //*dstring++;
	    //*string++;
	  }
	  *dstring = '\0';

	  converted++;
	} //end of case 's'
	break;
      case 'd':
	{
	  int neg = 0;
	  int dint = 0;
	  int i;
	  
	  while ( *input_string == ' ' ) /*disregard leading blank spaces*/
	    *input_string++;

	  /*check whether integer is negative or not*/
	  if( (*input_string == '+')  ||  (*input_string == '-') ) {
	    neg = (*input_string=='-');
	    *input_string++;
	  }
	    

	  for(i=0;  (*(input_string+i) != '\0')  &&  isdigit(*(input_string+i)); i++) {
	    dint = 10*dint + (*(input_string+i)-'0');
	    //*input_string++;
	  }

	  if (neg)
	    dint = -dint;

	  *va_arg( variable_list, int* ) = dint;
	  
	  converted++;
	} //end of case 'd'
	break;
      case 'f':
	{
	  int neg = 0;
	  double ddouble = 0;
	  int i;
	  int decimal_point_cnt = 0;
	  
	  while ( *input_string == ' '  ) /*disregard leading blank spaces*/
	    *input_string++;
	  
	  /*check whether number is negative or not*/
	  if ( (*input_string == '+')  ||  (*input_string == '-') ) {
	    neg = (*input_string=='-');
	    *input_string++;
	  }
	  
	  for (i=0;  ( *(input_string+i) != '\0' )  &&  
		 ( isdigit(*(input_string+i)) || (*(input_string+i)=='.') ); 
	       i++) {
	    if ( *(input_string+i) == '.' ) 
	      decimal_point_cnt = 1;
	    else{
	      if ( !decimal_point_cnt )
		ddouble = 10*ddouble + (*(input_string+i)-'0');
	      else {
		ddouble = ddouble +  (*(input_string+i)-'0')*pow(10, -decimal_point_cnt);
		decimal_point_cnt++;
	      } 
	    }
	  }

	  if (neg)
	    ddouble = -ddouble;
	  
	  *va_arg( variable_list, double* ) = ddouble;
	  
	  converted++;  
	  
	} //end of case 'f'
	break;
      } //end of  switch ( *format )
    
  } //end of while ( *format != '\0' )
  
  va_end(variable_list);
  
  return converted;

}












 
