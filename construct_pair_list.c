//#include "header.h"
#include "math.h"
#include "stdio.h"
#include "stdlib.h"



#define anint(expr) (int)(expr < 0 ? expr-0.5 : expr+0.5)

#define max(a, b) ( (a) > (b)   ?   a : b ) 

/* cells are numbered progressively 
   the index m for a cell located at 
   (n_x, n_y, n_z)is determined by:
   m = N_x*N_y*n_z + N_x*n_y + n_x.
   N_x, N_y, and N_z are the number
   cells in the x-, y-, and z-direction,
   respectively. Thus, M = N_x*N_y*N_z is 
   the total number of cells in the 
   simulation box. Given that n_i can vary 
   in the range [0,N_i-1], 
   with i = x, y, and z; it follows that 
   m varies in the range [0, M-1]. 
   The function also allocates memory 
   for an array required by the cell list
   routine, i.e.
   mask_pointer, of size 2*stripe_num; 
   and
   cell_pointer, of size cell_num = cellx_num*celly_num*cellz_num.
   The memory is dynamically allocated, therefore
   a call to construct_pair_list must always be
   followed by a call to delete_pair_list, before
   the pair list goes out of scope.
   
   The array mask_array indicates which cell 
   interacts with which.
   mask_array[i]=1 means that cell m interacts 
   with cell m+i, where: 0 <= m < n_cell-i.
   Array mask has size M.
   Instead of storing mask_array in memory, 
   the array mask_pointer is used.
   Given an integer s such that 0 <= s < stripe_num-1, 
   mask_pointer[2*s]=m and  mask_pointer[2*s+1]=n > m, 
   means that all elements of 
   array mask_array in the range [m,n] are equal to 1. 
   Furthermore, mask_array[m-1] and mask_array[n+1] 
   must be equal to 0. The series of 1 in the array 
   cell_mask between m and n is called a stripe,
   and mask_pointer[2*s] and mask_pointer[2*s+1] are  
   the initial and final cell index of the stripe s,
   respectively.
   Size of mask_point is 2*stripe_num.
   Example:
   Let mask_array be:
   mask_array = {0 | 1 | 0 | 1 1 1 | 0 | 1 1 | 0  0 | 1  1  1 | 0};
   array index   0   1   2   3 4 5   6   7 8   9 10  11 12 13  14 
   stripe index      0         1          2              3        
   then the array mask_pointer will look like:
   mask_pointer = {1 1 | 3 5 | 7 8 | 11 13}
   stripe index     0     1     2      3    ;
   where the character "|" marks the boundary of each stripe
   
   For more details cfr.
   Tim H. Heinz, P. H. Huenberger, J. Comp. Chem. 25, 1474 (2004)
   
*/

int construct_pair_list(int cellx_num,
			int celly_num,
			int cellz_num,
			int *p_cell_tot_num,
			double boxx_size,
			double boxy_size,
			double boxz_size,
			int *p_stripe_num,
			int **p_mask_pointer,
			//int **p_cell_pointer,
			double cutoff_radius)
{


  int fill_mask(int cell_tot_num,
		int cellx_num,
		int celly_num,
		//int cellz_num,
		double cellx_len,
		double celly_len,
		double cellz_len,
		double cutoff_dist,
		int *p_stripe_cnt,
		int *mask_array);
  

  int fill_mask_pointer(int cell_num,
			int stripe_num,
			int *mask,
			int *mask_pointer);
  
  
  
  double cellx_len, celly_len, cellz_len;

  int cell_num = 0;
  int stripe_cnt;
  
  int *mask;

  if ( cellx_num < 1 ||
       celly_num < 1 ||
       cellz_num < 1 ) {
    printf("\n%s\n%s%d%s%d%s%d%s\n%s\n%s\n\n",
	   "Fatal error in function construct_pair_list!",
	   "Invalid triplet (", cellx_num, ", ", 
	   celly_num, ", ", cellz_num, ") passed",
	   "as number of cells along x, y, and z, respectively.",
	   "This program is exiting, sorry!");
    exit(1);
  }


  if ( boxx_size < 1.0e-5 ||
       boxy_size < 1.0e-5 ||
       boxz_size < 1.0e-5 ) {
    printf("\n%s\n%s%.5lf%s%.5lf%s%.5lf%s\n%s\n%s\n\n",
	   "Fatal error in function construct_pair_list!",
	   "Invalid triplet (", boxx_size, ", ", 
	   boxy_size, ", ", boxz_size, ") passed",
	   "as cell length along x, y, and z, respectively.",
	   "This program is exiting, sorry!");
    exit(1);
  }

  if ( cutoff_radius < 1.0e-5 ) {
    printf("\n%s\n%s%.6lf%s\n%s\n\n",
	   "Fatal error in function construct_pair_list!",
	   "Invalid value ", cutoff_radius, 
	   " passed as cutoff radius.",
	   "This program is exiting, sorry!");
    exit(1);
  }

  /* find cell edge length in all 
     three directions */
  cellx_len = boxx_size/cellx_num;
  celly_len = boxy_size/celly_num;
  cellz_len = boxz_size/cellz_num;
  
  /* calculate total number of cells */
  cell_num = 
    cellx_num * celly_num * cellz_num;
  
  
  /* allocate memory for mask */
  mask = calloc(cell_num, sizeof(int));
  
  fill_mask(cell_num,
	    cellx_num,
	    celly_num,
	    cellx_len,
	    celly_len,
	    cellz_len,
	    cutoff_radius,
	    &stripe_cnt,
	    mask);
  
  /* the number of stripe
     must be a positive integer.
     if the number of stripes is 0,
     then the whole system is 
     contained in only one cell. */
  if ( stripe_cnt < 0 ) {
    printf("\n%s\n%s%d%s\n%s\n\n",
	   "Fatal error in function construct_pair_list!",
	   "The number of stripes found is ",
	   stripe_cnt, ".",
	   "It is not possible to continue, sorry!");
    exit(1);
  }
  
  
  /* allocate memory for mask_pointer */
  int *mask_pointer =  
    malloc(2*stripe_cnt*sizeof(int));
  if ( stripe_cnt ) {  /* there are more than just one cell */
    if ( mask_pointer == NULL ) {
      printf("\n%s\n%s\n%s\n%s%d%s\n%s\n\n",
	     "Error in function construct_pair_list",
	     "while trying to allocate the pointer",
	     "mask_pointert with an area in the memory", 
	     "corresponding to ", 2*stripe_cnt, " integers.",
	     "It is impossible to continue, sorry!");
      exit(1);
      
    }
  }
  else
    mask_pointer = NULL;
  
  fill_mask_pointer(cell_num,
		    stripe_cnt,
		    mask,
		    mask_pointer);
  
  

   
   /* pass variables up
      onto calling function */
  *p_cell_tot_num = cell_num;
  *p_stripe_num = stripe_cnt;
  *p_mask_pointer = mask_pointer;
  
  
    
  /* deallocate memory 
     allocated for array 
     mask */
  free(mask);
  
  return 0;
}






/******************************************************/



/* Important warning!!!
   Make sure that all elements of array
   mask_array are initialized to 0 
   before the array is passed as an
   argument to this function. */
int fill_mask(int cell_tot_num,
	      int cellx_num,
	      int celly_num,
	      //int cellz_num,
	      double cellx_len,
	      double celly_len,
	      double cellz_len,
	      double cutoff_dist,
	      int *p_stripe_cnt,
	      int *mask_array)
{
  
  int stripe_cnt = 0;

  /* although each cell 
     interacts with itself,
     mask_array[0] = 0 because
     we do not want any stripe to
     begin with the value 0. 
     This corresponds to the 
     fact that in energy and
     force calculation, 
     interactions of a particle 
     with all particles in the same 
     cell are considered first 
     and, subsequently, interactions 
     of the particle with particles
     contained in all other cells. */
  mask_array[0] = 0;
  //mask_array[0] = 1;
  //stripe_cnt++;
  
  /* start loop over cells
     and fill in mask_array 
     with 0 or 1 */
  for (int i=0; i<cell_tot_num-1; i++) {
    
    /* find integer coordinates of
       this cell */
    int z_i = i/(cellx_num*celly_num);
    int y_i = (i%(celly_num*cellx_num))/cellx_num;
    int x_i = i%cellx_num;
    
    for (int j=i+1; j<cell_tot_num; j++) 
      
      if ( !mask_array[j-i] ) {
	
	/* find integer coordinates
	   of this cell */
	int z_j = j/(cellx_num*celly_num);
	int y_j = (j%(celly_num*cellx_num))/cellx_num;
	int x_j = j%cellx_num;
	
	
	/* consider closest image of 
	   cell j to cell i and 
	   calculate distance */
	int dn_x = x_j - x_i;
	/* apply periodic boundary conditions */
	dn_x -= 
	  anint((double)dn_x/(double)cellx_num)*cellx_num;
	int dn_y = y_j - y_i;
	/* apply periodic boundary conditions */
	dn_y -= 
	  anint((double)dn_y/(double)celly_num)*celly_num;
	int dn_z = z_j - z_i;
	
	/* find minimum distance between
	   cell i and minimum image of 
	   cell j */
	double min_dist_sq = 
	  ( max(abs(dn_x),1) - 1 )*
	  ( max(abs(dn_x),1) - 1 )*
	  cellx_len*cellx_len 
	  +
	  ( max(abs(dn_y),1) - 1 )*
	  ( max(abs(dn_y),1) - 1 )*
	  celly_len*celly_len 
	  +
	  ( max(abs(dn_z),1) - 1 )*
	  ( max(abs(dn_z),1) - 1 )*
	  cellz_len*cellz_len;
	
	
	if (  min_dist_sq < 
	      cutoff_dist*cutoff_dist ) {
	  mask_array[j-i] = 1;
	  /* increment or decrement 
	     number of stripes when 
	     necessary */
	  /* a new stripe is found when 
	     an element in the array mask with
	     a freshly inserted value of 1 
	     is surrounded by two 0s; 
	     conversely, when a mask
	     array element with freshly inserted
	     value 1 is surrounded by two other 1s, 
	     it means that the same stripe has 
	     been counted twice */
	  if ( j-i+1 < cell_tot_num ) {
	    if ( !(mask_array[j-i+1] - mask_array[j-i-1]) )   
	      if ( !mask_array[j-i-1] ) 
		stripe_cnt++;
	      else 
		stripe_cnt--;
 	  }
	  else  /* j-i = cell_tot_num-1 */ 
	    if ( !mask_array[j-i-1] ) /* if mask[cell_tot_num-2] = 0
					 increment stripe_cnt */
	    stripe_cnt++;
	} /* end of	if (  min_dist_sq < 
	     cutoff_radius*cutoff_radius )  */
	
      } /* end of  for (int j=i+1; j<cell_num; j++) 
	   if ( !cell[j-i]) ... */
    
    
  } /* end of  for (int i=0; i<cell_num-1; i++) */
  

  *p_stripe_cnt = stripe_cnt;

  return 0;
 
}






/******************************************************/





int fill_mask_pointer(int cell_num,
		      int stripe_num,
		      int *mask,
		      int *mask_pointer)
{
  
  /* list through array mask and 
     search for series of adjacent 1. */
  
  int stripe_cnt = 0;

  
  for (int i=1; i<cell_num; i++) 
    
    if ( mask[i] - mask[i-1] )   /* mask[i] != mask[i-1] */
      if ( mask[i] )  /* mask[i] is 1 and  mask[i-1] is 0 */  
	/* found a new stripe */ 
	/* write this cell index 
	   in mask_pointer at proper 
	   position */
	mask_pointer[2*stripe_cnt] = i;
      else /* mask[i] is 0  and mask[i-1] is 1 */
	/* reached end of stripe */
	/* write index of previous cell
	   in array mask_pointer at 
	   proper position and increment
	   counter of stripes */
	mask_pointer[1+2*stripe_cnt++] = i-1;

  /* end of  for (int i=1; i<cell_num; i++), 
     loop over cells */
  
  
  /* if last element of mask array is 1,
     either a new stripe has been found,
     or an exisisting stripe needs a 
     terminating cell value */
  if ( mask[cell_num-1] ) {
    
    if ( !mask[cell_num-2] ) 
      mask_pointer[2*stripe_cnt] = cell_num-1;
    
    mask_pointer[1+2*stripe_cnt++] = cell_num-1;  
  }
  
  if ( stripe_cnt != stripe_num ) {
    printf("\n%s\n%s%d%s%d\n%s\n\n", 
	   "Error in function fill_mask_pointer",
	   "stripe_cnt is ", stripe_cnt, 
	   " and stripe_num is ", stripe_num,
	   "Now exiting, sorry!");
    exit(1);
  }


  return 0;
}





/******************************************************/


/* Always refer this function before
   a pair list created with the function
   create_pair_list goes out of scope */
int delete_pair_list(int *mask_pointer)
//		     int *cell_pointer)
{

  free(mask_pointer);
  
  //free(cell_pointer);

  return 0;
}



/******************************************************/



/* This function dynamcally allocates memory for
   two arrays required by the pair list:
   (i) cell_list, of size particle_num,
   storing the particle list;
   (ii) cell_pointer, of size cell_num+1,
   storing the cell_list-index of the first particle
   in each cell. 
   cell_pointer[p+1]-cell_pointer[p] = n, means
   that cell p contains n particles and that 
   all particles between cell_list[cell_pointer[p]]
   and cell_pointer[cell_pointer[p+1]] make up 
   the list of cell p. 
   This function must be 
   followed by a call to function
   delete_cell_list_array before the
   created arrays goes out of scope */
int create_cell_arrays(int particle_num,
		       int cell_num,
		       int **p_cell_list,
		       int **p_cell_pointer)
{
  *p_cell_list = 
    malloc(particle_num*sizeof(int));
  if ( *p_cell_list == NULL ) {
    printf("\n%s\n%s\n%s\n%s\n%s%d%s\n%s\n\n",
	   "Error in function create_cell_arrays!",
	   "It was not possible to allocate",
	   "the pointer p_cell_list with", 
	   "an area in the memory corresponding",
	   "to ", particle_num, " integers.",
	   "It is impossible to continue, sorry!");
    exit(1);
  }
  //assert( *p_cell_list != NULL );

  *p_cell_pointer = 
    malloc((cell_num+1)*sizeof(int));
  if ( *p_cell_pointer == NULL ) {
    printf("\n%s\n%s\n%s\n%s\n%s%d%s\n%s\n\n",
	   "Error in function create_cell_arrays!",
	   "It was not possible to allocate",
	   "the pointer p_cell_pointer with", 
	   "an area in the memory corresponding",
	   "to ", cell_num+1, " integers.",
	   "It is impossible to continue, sorry!");
    exit(1);
  }
  //assert( *p_cell_pointer != NULL );

  return 0;
}  





/*********************************************************/




/* Always refer this function after an array
   of cell lists has been created by invoking 
   function create_cell_list_array. The call
   to this function must be made before the 
   list arrays created go out of scope */
int delete_cell_arrays(int *cell_list,
		       int *cell_pointer)
{

  free(cell_list);
  
  free(cell_pointer);

  return 0;
  
}





/*********************************************************/



/* Resizes the array cell_list. 
   This function must be invoked 
   every time when the number of 
   particles in the system changes */
int resize_cell_list(int current_size,
		     int new_size,
		     int** p_cell_list)
{
  if ( *p_cell_list == NULL ) {
    printf("\n%s\n%s\n%s\n\n",
	   "Fatal error in function resize_cell_list!",
	   "Cannot resize empty array.",
	   "It is not possible to continue, sorry!");
    exit(1);
  }

    
  int *new_cell_list;

  new_cell_list = realloc(*p_cell_list, new_size);

  if ( new_cell_list ) 
    *p_cell_list = new_cell_list;
  else {
    //if ( new_cell_list == NULL ) {
    printf("\n%s\n%s%d%s%d%s\n%s\n%s\n\n",
	   "Fatal error in function resize_cell_list!",
	   "Cannot resize cell list from size ", 
	   current_size, " to size ", new_size, ".",
	   "Insufficient amount of memory.",
	   "It is impossible to continue, sorry!");
    exit(1);
  }
  
  //*p_cell_list = new_cell_list;
  
  return 0;
}



/*********************************************************/


