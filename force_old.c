#include "header.h"

/* Calculates forces on each atom and potential energy. */
int force(int num_species, 
	  int potential_type[][n_max_type],
	  int *fix_atom_label,
	  int *neighbour_num, 
	  int neighbour_list[][n_max_atom],
	  int *atoms_per_species, int *atom_species,
	  double *pot_en,
	  double rho_bm, double *q_bm, 
	  double b_bm[][n_max_type], 
	  double c_bm[][n_max_type],
	  double d_bm[][n_max_type], 
	  double alpha_bm[][n_max_type],
	  //double fact_matsui[][n_max_type],
	  double epsilon_lj[][n_max_type], 
	  double sigma_lj[][n_max_type],
	  double sigma_mixed[][n_max_type], 
	  double epsilon_mixed[][n_max_type],
	  //double join_coeff_mixed[][n_max_type][6],
	  double pot_range[][n_max_type], 
	  double cutoff_distance[][n_max_type], 
	  double truncation_params[][n_max_type][6],
	  double distance_x[][n_max_atom], 
	  double distance_y[][n_max_atom], 
	  double distance_z[][n_max_atom],
	  double *force_x, double *force_y, double *force_z,
	  double *prev_fx, double *prev_fy, double *prev_fz)
{
 
  /* calculate number of atoms */
  int atom_num = 0;
  for (int i=0; i<num_species; i++)
    atom_num += atoms_per_species[i];

  /* Store old values of  
     forces in arrays */
  for(int i=0; i<atom_num; i++) {
    prev_fx[i] = force_x[i];
    prev_fy[i] = force_y[i];
    prev_fz[i] = force_z[i];
  }
  
  /* initialise potential
     energy to zero */
  *pot_en = 0.0;
  
  /*Set forces on each atom equal to 0*/
  for(int i=0; i<atom_num; i++) {
    force_x[i] = 0.0;
    force_y[i] = 0.0;
    force_z[i] = 0.0;
  }
  

  /*calculate force on atoms*/
  for(int i=0; i<atom_num; i++) {
    
    /* set forces on and energy 
       contribution from 
       atom i equal to 0 */ 
    double ener_i = 0.0;
    double fi_x=0.0, fi_y=0.0, fi_z=0.0;
    
    /* loop over all neighbours to atom i */
    for (int k=0; k<neighbour_num[i]; k++) {
      
      int j = neighbour_list[i][k];      
      
      if (j<i) { /*consider each couple of atoms 
		   only once in the loop*/
	
	/*calculate squared distance between atom i and j*/
	double dist_ij_2 = 
	  distance_x[i][j]*distance_x[i][j] + 
	  distance_y[i][j]*distance_y[i][j] +
	  distance_z[i][j]*distance_z[i][j];
	
  

	if (dist_ij_2 >
	    pot_range[atom_species[i]][atom_species[j]]*
	    pot_range[atom_species[i]][atom_species[j]]) {
	  printf( "Error in function force.c\n");
	  printf("The squared distance between atom %d and %d\n",
		 i, j);
	  printf("has been calculated to be %.6lf\n", dist_ij_2);
	  printf("whereas the squared potential range is %.6lf\n",
		 pot_range[atom_species[i]][atom_species[j]]*
		 pot_range[atom_species[i]][atom_species[j]]);
	}

	     
	
	double ener_ij; /*contribution to potential 
			  energy due to interaction 
			  between atom i and atom j */
	double dv_over_dr_times_dist; /* derivative of potential 
				       with respect to distance 
				       between atom i and j,
				       multiplied by distance 
				       between atom i and j */
	double fij_x, fij_y, fij_z; /* force arising as a result 
				       of interaction between atom 
				       i and atom j  */
	
	/* calculate strenght of interaction 
	 between atom i and j, as well as 
	 derivative of potential multiplied 
	 by atom distance */

	int type1 = atom_species[i];
	int type2 = atom_species[j];
	
	if ( potential_type[type1][type2] == 0 )
	  /* calculate Born-Mayer potential */
	  calculate_born_mayer_potential
	    (dist_ij_2,
	     &ener_ij,
	     &dv_over_dr_times_dist,
	     rho_bm,
	     q_bm[type1], q_bm[type2],
	     b_bm[type1][type2], 
	     c_bm[type1][type2],
	     d_bm[type1][type2], 
	     alpha_bm[type1][type2],
	     cutoff_distance[type1][type2],
	     pot_range[type1][type2],
	     truncation_params[type1][type2]);
	else if ( potential_type[type1][type2] == 1 )
	  /* calculate Lennard-Jones potential */
	  calculate_lj_potential(dist_ij_2,
				 &ener_ij,
				 &dv_over_dr_times_dist,
				 sigma_lj[type1][type2],
				 epsilon_lj[type1][type2],
				 cutoff_distance[type1][type2],
				 truncation_params[type1][type2]);
	else if  ( potential_type[type1][type2] == 2 )
	  /* calculate 10-5 potential */
	  calculate_10_5_potential(dist_ij_2,
				   &ener_ij,
				   &dv_over_dr_times_dist,
				   sigma_mixed[type1][type2],
				   epsilon_mixed[type1][type2],
				   cutoff_distance[type1][type2],
				   truncation_params[type1][type2]);
	else {
	  printf("%s\n%s%d%s%d\n%s%d%s%d\n%s%d%s\n%s\n%s\n",
		 "Fatal error in function force.",
		 "The interaction between atom ", i, 
		 " of type ", type1,
		 "and atom ", j, " of type ", type2,
		 "turned out to be of type ", 
		 potential_type[type1][type2], ".",
		 "Interaction type must be an integer between 0 and 3.",
		 "It is impossible to continue, sorry!");
	  exit(1);
	}
	
	/* Add to obtain contribution 
	   due to interaction
	   between atom i and 
	   all other atoms */
	ener_i += ener_ij;
		
	/* calculate opposite of force acting 
	   on atom i due to atom j */
	double dv_over_dr = dv_over_dr_times_dist/dist_ij_2;
	fij_x = distance_x[i][j]*dv_over_dr;
	fij_y = distance_y[i][j]*dv_over_dr;
	fij_z = distance_z[i][j]*dv_over_dr;
	
	
	/* add force on atom j 
	   due to atom i to 
	   total force on 
	   atom j */
	force_x[j] += fij_x;
	force_y[j] += fij_y;
	force_z[j] += fij_z;

	/* Add contribution due 
	   to atom j to opposite 
	   of total force on 
	   atom i */
	fi_x += fij_x;
	fi_y += fij_y;
	fi_z += fij_z;


      } /* end of if (j,i) */
    } /* end of loop over neighbours to atom i */
  

    /* Contribution due to 
       all atoms other than i 
       is added to force 
       on atom i */
    force_x[i] -= fi_x;
    force_y[i] -= fi_y;
    force_z[i] -= fi_z;
  
        
    /* Add contribution given by 
       atom i to potential energy */
    *pot_en += ener_i;
    
  } /*end of loop on all atoms*/
   
   

  /*set to zero forces on fixed atoms*/
  for (int i=0; i<atom_num; i++) 
    if ( fix_atom_label[i] ) {
      force_x[i] = 0.0;
      force_y[i] = 0.0;
      force_z[i] = 0.0;
    }
  

/*   /\*print all forces for debugging purposes*\/ */
/*   if ( current_step > 50 ) { */
    
/*     FILE *force_file; */
/*     char *step =  int_to_string(current_step); */
/*     char force_file_name[strlen(step)+15]; */
/*     strcpy(force_file_name,"step_"); */
/*     strcat(force_file_name, step); */
/*     strcat(force_file_name,"_force.dat"); */
    
/*     force_file = fopen(force_file_name,"a");  */
/*     for (int i=0; i<atom_num; i++) */
/*       fprintf(force_file, */
/* 	      "Force on atom %d: %.3lf, %.3lf, %.4lf\n", */
/* 	      i, force_x[i], force_y[i], force_z[i]); */
/*     fclose(force_file); */

/*   } */

  return(0);
  
}

