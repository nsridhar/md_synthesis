#include "header.h"

/* velocities are expressed in Angstr/s,
   masses in a.m.u.,
   temperature is calculated in Kelvin*/

int calculate_temperature(int atom_number,
			  //int *fix_atom_label,
			  int fixed_atom_num,
			  int *atom_species,
			  double *atom_mass,
			  double *p_temperature,
			  double *vel_x,
			  double *vel_y,
			  double *vel_z)
{


  double sum_vel_squared = 0.0;
  
  double conversion_fact = joule_per_ev/
    (kg_per_amu*metre_per_angstrom*metre_per_angstrom);

  for ( int i=0; i<atom_number; i++ )
    /* Calculate contribution of 
       atom i to kinetic energy */
    sum_vel_squared += 
      atom_mass[atom_species[i]] *
      (vel_x[i]*vel_x[i] 
       + vel_y[i]*vel_y[i] 
       + vel_z[i]*vel_z[i]);


  /* Calculate kinetic energy (in eV) */

  /*Kin_energy = 1/2 sum(i=1,2,...,N) mass[i]*v[i]^2 */
  double kinetic_energy = 0.5 * sum_vel_squared /
    conversion_fact; 

  /* Calculate current system 
     temperature in K */
  /* (3/2)*N*kb*T = kin_energy => 
     T = (2/3)*kin_energy/(N*kb)  */
  *p_temperature =  2.0 * kinetic_energy / 
    (3.0*(atom_number-fixed_atom_num)*kb); 


  return 0;

}
