#include "header.h"


int cluster_search(int atom_number, //int num_fixed_atom, 
		   int *atom_state,
		   int *atom_bond_number, 
		   int **atom_bond_list,
		   //int atom_bond_list[][n_max_atom],
		   int *clust_size, 
		   int **clust_list,
		   double *z_pos);
//int clust_list[][n_max_atom]);


int search_bond_list(int atom_index,
		     int clust_size_offset,
		     int *atom_bond_number,
		     //int atom_bond_list[][n_max_atom],
		     int **atom_bond_list,
		     int *bond_cnt, int *listed_atom, 
		     int *atom_in_cluster);

int assign_atom_phase(int cluster_number, 
		      int *cluster_size,  
		      //int cluster_list[][n_max_atom],
		      int **cluster_list,
		      int *atom_state);



/************************************************************/



/* After filling up the list of atom bonds,
   this function references function cluster_search
   which, among other things, fills array 
   atom_in_substrate with a 1 or a 0 for each atom, 
   depending on whether the atom belongs to 
   the solid or not */
int label_substrate_atoms(int atom_number, 
			  //int num_fixed_atom,
			  int *atom_species,
			  int *atom_in_substrate,
			  double bond_dist[][n_max_type],
			  double box_x_dim, double box_y_dim,
			  double *x_pos, double *y_pos, 
			  double *z_pos)
  
{
  
  /*   int bond_list[n_max_atom][n_max_atom], */
  /*     cluster_list[n_max_atom][n_max_atom]; */
  
  /*   int bond_num[atom_number], cluster_size[atom_number]; */
  /*   /\* initialise bond_num and cluster_size to 0 *\/ */
  /*   for ( int i=0; i<atom_number; i++) { */
  /*     bond_num[i] = 0; */
  /*     cluster_size[i] = 0; */
  /*   } */
  
  
  int *bond_num, *cluster_size,
    **bond_list, **cluster_list;
  
  /* allocate memory for array
     storing number of bonds of
     each atom */
  //int *bond_num = calloc(atom_number, sizeof(int));
  bond_num = calloc(atom_number, sizeof(int));
  if ( bond_num == NULL ) {
    printf("%s\n%s\n%s\n%s\n",
	   "Fatal error in function label_substrate_atoms.",
	   "The pointer bond_num has been assigned",
	   "with the NULL value due to lack of memory.",
	   "It is impossible to continue, sorry!");
    exit(1);
  }
  
  /* allocate memory for array
     storing list of atoms bound
     to each atom */
  bond_list = calloc(n_max_atom, sizeof(int*));
 //int **bond_list = calloc(atom_number, sizeof(int*));
  if ( bond_list == NULL ) {
    printf("%s\n%s\n%s\n%s\n",
	   "Fatal error in function label_substrate_atoms.",
	   "The pointer bond_list has been assigned",
	   "with the NULL value due to lack of memory.",
	   "It is impossible to continue, sorry!");
    exit(1);
  }
  for (int i=0; i<atom_number; i++) {
    *(bond_list+i) = calloc(n_max_atom, sizeof(int));
    //*(bond_list+i) = calloc(atom_number, sizeof(int));
    if ( *(bond_list+i) == NULL ) {
      printf("%s\n\n%s%d%s\n%s\n%s\n",
	     "Fatal error in function label_substrate_atoms.",
	     "The pointer bond_list[", i, "] has been assigned",
	     "with the NULL value due to lack of memory.",
	     "It is impossible to continue, sorry!");
      exit(1);
    }
  }
  
  //allocate memory for array
  //storing size of each cluster
  // found by cluster_search subroutine 
  
  cluster_size = calloc(atom_number, sizeof(int));
  if ( cluster_size == NULL ) {
    printf("%s\n%s\n%s\n%s\n",
	   "Fatal error in function label_substrate_atoms.",
	   "The pointer cluster_size has been assigned",
	   "with the NULL value due to lack of memory.",
	   "It is impossible to continue, sorry!");
    exit(1);
  }
  
  /* allocate memory for array
     storing list of atoms making
     up each found cluster */
  cluster_list =  calloc(atom_number, sizeof(int*));
  //int **cluster_list =  calloc(atom_number, sizeof(int*));
  if ( cluster_list == NULL ) {
    printf("%s\n%s\n%s\n%s\n",
	   "Fatal error in function label_substrate_atoms.",
	   "The pointer cluster_list has been assigned",
	   "with the NULL value due to lack of memory.",
	   "It is impossible to continue, sorry!");
    exit(1);
  }
  for (int i=0; i<atom_number; i++) {
    *(cluster_list+i) =  calloc(n_max_atom, sizeof(int));
    //*(cluster_list+i) =  calloc(atom_number, sizeof(int));
    if ( *(cluster_list+i) == NULL ) {
      printf("%s\n%s%d%s\n%s\n%s\n",
	     "Fatal error in function label_substrate_atoms.",
	     "The pointer cluster_list[", i, "] has been assigned",
	     "with the NULL value due to lack of memory.",
	     "It is impossible to continue, sorry!");
      exit(1);
    }
  }
  
  
  // search for atom bonds */
  for (int i=1; i<atom_number; i++) 
    for (int j=0; j<i; j++) {
      
      /* calculate distance betwenn 
	 atom i and j */
      double dist_x = x_pos[j]-x_pos[i];
      /* apply periodic boundary conditions */
      dist_x -= anint(dist_x/box_x_dim)*box_x_dim;
      double dist_y = y_pos[j]-y_pos[i];
      /* apply periodic boundary conditions */
      dist_y -= anint(dist_y/box_y_dim)*box_y_dim;
      double dist_z = z_pos[j]-z_pos[i];
      double dist_squared = 
	dist_x*dist_x +
	dist_y*dist_y +
	dist_z*dist_z;
      //if ( i == 3600 )
      //printf("%s%d\n%s%d%s%.4lf\n",
      //       "Calculated squared dist between atom ", i,
      //       "and ", j, " is ", dist_squared);
      
      /* if distance between atom i and j is 
	 less than bonding distance, then insert 
	 atom i among list of atoms bound to 
	 atom j, and viceversa */
      if ( dist_squared < 
	   bond_dist[atom_species[j]][atom_species[i]]*
	   bond_dist[atom_species[j]][atom_species[i]] ) {
	bond_list[j][bond_num[j]++] = i;
	  bond_list[i][bond_num[i]++] = j;
      }
      
    }
  
  
  /* assign each atom with a label 
     of 1 or 0, depending on whether they 
     are in solid or gas phase,
     respectively */
  cluster_search(atom_number, //num_fixed_atom, 
		 atom_in_substrate,
		 bond_num, bond_list, 
		 cluster_size, cluster_list, z_pos);
  
  
  /* free dynamically allocated memory */
  
  free(bond_num);
  
  for (int i=0; i<atom_number; i++)
    free(*(bond_list+i));
  
  free(bond_list);
    
  free(cluster_size);
  
  for (int i=0; i<atom_number; i++)
    free(*(cluster_list+i));
  
  free(cluster_list);
  
  return 0;
  
    
}



/************************************************************/





/* This function is passed on the following arguments:
   - atom_number is the number of atoms in the system;
   - atom_phase[i] = 1, 0 if atom is in the solid or gas phase, respectively;
   - atom_bond_number[i] = n, if atom i forms n bonds;
   - atom_bond_list[i] is the list of all atoms bonded to atom i; 
   - clust_size[j] = m, if cluster j is made up of m atoms;
   - clust_list[j] is list of atoms making up cluster j.
   The function fills up the arrays atom_phase,  clust_size,
   and clust_list, and returns the number of clusters found. */

int cluster_search(int atom_number, //int num_fixed_atom, 
		   int *atom_phase,
		   int *atom_bond_number,
		   int **atom_bond_list, 
		   int *clust_size, int **clust_list,
		   double *z_pos)
  
{

  /* initialise clust_size to 0 */
  for (int i=0; i<atom_number; i++)
    clust_size[i] = 0;

  /* array indicating the bond in the list of each
     atom which is being considered.
     All elements are initialsed to 0 */
  int *bond_counter = malloc(atom_number*sizeof(int));
  if ( bond_counter == NULL ) {
    printf("%s\n%s\n%s\n%s\n",
	   "Fatal error in function cluster_search.",
	   "The pointer bound_counter has been assigned",
	   "with the NULL value due to lack of memory.",
	   "It is impossible to continue, sorry!");
    exit(1);
  }
  for (int i=0; i<atom_number; i++) 
    bond_counter[i] = 0;
  
  
  /* atom_cluster is the array indicating 
     which cluster each atom belongs to.
     atom_cluster[i] = j, means that atom i belongs 
     to cluster j. 
     All elements of this array are initialised 
     to -1 */
  //int *atom_cluster = malloc(n_max_atom*sizeof(int));
  //for (int i=0; i<n_max_atom; i++)
  //atom_cluster[i] = -1;
  
  /* visited_atom is an array of flags
     needed by search_bond_list function.
     visited_atom[i] = 1 if atom i has 
     already been listed through by the 
     search_bond_list procedure, 
     visited_atom[i] = 0 otherwise.
     All elements are initialised to 0 */
  int *visited_atom =  malloc(atom_number*sizeof(int));
  if ( visited_atom == NULL ) {
    printf("%s\n%s\n%s\n%s\n",
	   "Fatal error in function cluster_search.",
	   "The pointer visited_atom has been assigned",
	   "with the NULL value due to lack of memory.",
	   "It is impossible to continue, sorry!");
    exit(1);
  } 
  for (int i=0; i<atom_number; i++)		
    visited_atom[i] = 0;

  int cluster_counter = 0;
  /* cycle through all atoms and associate
     each of them to a cluster */
  for (int i=0; i<atom_number; i++) {
    int this_cluster_size = 0;
    this_cluster_size =
      search_bond_list(i, 0, atom_bond_number, atom_bond_list,
		       bond_counter, visited_atom,
		       clust_list[cluster_counter]);

    
    if ( this_cluster_size > 0 ) {
      clust_size[cluster_counter] = this_cluster_size;
      cluster_counter++;
    }
  }

  
  /* set atom phase label to 0 unless 
     atom belongs to one of fixed planes */
  //for (int i=0; i<atom_number; i++)
  //if ( i >=  num_fixed_atom )
  //  atom_phase[i] = 0; 
  
  /* mark the first num_fixed_atom atoms 
     as belonging to solid */
  //for (int i=0; i<num_fixed_atom; i++)
  //atom_phase[i] = 1;
  
  /* set phase label of all atoms to 0 */
  for (int i=0; i<atom_number; i++)
    //if ( i >=  num_fixed_atom )
    atom_phase[i] = 0; 

  /* search for atom with lowest z coordinate
     in the system and set its phase to 1 */
  {
    int min_z_atom_ind = 0;
    double min_z = z_pos[min_z_atom_ind];
    
    for (int i=0; i<atom_number; i++)
      if ( z_pos[i] < min_z ) {
	min_z = z_pos[i];
	min_z_atom_ind = i;
      }
    atom_phase[min_z_atom_ind] = 1;
  }
  

  /* mark atom as belonging to solid (atom_phase = 1)  
     in case it forms bonds with other atoms in the solid, 
     otherwise the atom  is marked as gas phase atom  
     (atom_phase = 0) */
  assign_atom_phase(cluster_counter, 
		    clust_size,  
		    clust_list, 
		    atom_phase);
  
  
  free(bond_counter);
  free(visited_atom);
  
  /* return  number of clusters found */
  return cluster_counter;
  
  
}






/************************************************************/






/* Lists through all atoms belonging to same 
   cluster as  atom_index.
   Associates each atom with that cluster 
   and returns  cluster size. Returns 0
   if atom_index has already been considered. */

int search_bond_list(int atom_index, 
		     int clust_size_offset,
		     int *atom_bond_number,
		     //int atom_bond_list[][n_max_atom],
		     int **atom_bond_list,
		     int *bond_cnt, int *listed_atom, 
		     int *atom_in_clust)
  
  
{
  
  /*  printf("\ncluster_size is being called for atom %d and cluster index of %d\n", */
  /* 	 atom_index, cluster_index);    */
  /*   printf("bond count of atom %d is %d\n", */
  /* 	 atom_index, bond_cnt[atom_index]); */
  /*   printf("bond number of atom %d is %d\n\n", */
  /* 	 atom_index, bond_number[atom_index]); */
  
  if ( atom_bond_number[atom_index] < 0 ) {
    printf("%s\n%s%d\n%s%d\n%s\n",
	   "Error in function search_bond_list",
	   "The number of bonds of atom ", atom_index,
	   "has been found to be ", atom_bond_number[atom_index],
	   "This program is exiting. Sorry!");
    exit(-1);
  }
  
  assert( atom_bond_number[atom_index] >= 0   && 
	  atom_bond_number[atom_index] < n_max_atom );
  
  /* if atom has no bonds, then
     just return 0 */
  if ( atom_bond_number[atom_index] == 0 )
    return 0;
  
  int clust_size = clust_size_offset;

  /* Check whether atom atom_index has been 
     previously associated with a cluster or not.
     If not, add atom_index in list of atoms making
     up current cluster */
  if ( listed_atom[atom_index] <= 0 ) {
    listed_atom[atom_index] = 1;
    atom_in_clust[clust_size] = atom_index;
    //cluster_atom[atom_index] = cluster_index;
    //clust_list[cluster_index][cluster_size] = atom_index
    clust_size++;
  }
  
  /* go through all atoms bonded to atom_index */
  while ( bond_cnt[atom_index] < 
	  atom_bond_number[atom_index] ) {
    
    /* consider atom in position bond_cnt[atom_index]
       in bond list of atom atom_index */
    int bonded_at = 
      atom_bond_list[atom_index][bond_cnt[atom_index]]; 
    
    /* If atom bonded_at has not been associated
       with any clusters before, add it to same 
       cluster as atom_index, i.e. cluster_index.
       Then go through bond list of bonded_at. */
    if ( listed_atom[bonded_at] <= 0 ) {
      
      listed_atom[bonded_at] = 1;
      atom_in_clust[clust_size] = bonded_at;
      //cluster_atom[bonded_at] = cluster_atom[atom_index];
      //atom_in_cluster[cluster_index][cluster_size] = bonded_at; 
      clust_size++;
      
      /* go through bond list of atom bonded_at */
      clust_size = search_bond_list(bonded_at, clust_size,
				    atom_bond_number, atom_bond_list,
				    bond_cnt, listed_atom,
				    atom_in_clust);
      /*       printf("cluster_size has returned %d, after listing atom %d\n", */
      /* 	     cluster_size, bonded_at); */
      /*       printf("cluster_size had been called by atom %d\n", */
      /* 	     atom_index); */
      /*       printf("bond count of atom %d is %d\n", */
      /* 	     atom_index,bond_cnt[atom_index]); */
      /*       printf("bond number of atom %d is %d\n", */
      /* 	     atom_index, bond_number[atom_index]); */
    }   
    
    /*increment bond list counter of atom atom_index*/
    bond_cnt[atom_index]++;
    
  } /* end of   
       while (bond_cnt[atom_index] < bond_number[atom_index]) */
  
  
  /*    printf("cluster_size is returning %d, after listing atom %d\n", */
  /* 	     cluster_size, atom_index); */
  /*       printf("cluster_size had been called with cluster index of %d\n", */
  /* 	     cluster_index); */
  /*       printf("bond count of atom %d is %d\n", */
  /* 	     atom_index, bond_cnt[atom_index]); */
  /*       printf("bond number of atom %d is %d\n", */
  /* 	     atom_index, bond_number[atom_index]); */
  
  
  return clust_size;
  
}





/************************************************************/





/* Associates each atom with a label indicating the 
   atom phase: 1 and 0 is for atoms in solid and gas 
   phase, respectively. 
   The labels are written in array
   atom_state passed on as an argument. 
   It is assumed that the atom with lowest 
   z-coordinate has atom_state equal to 1,
   when the array is passed on to the function. */

int assign_atom_phase(//int atom_number,
		      int cluster_number, 
		      int *cluster_size,
		      //int cluster_list[][n_max_atom],
		      int **cluster_list,
		      int *atom_state)
{

  /* once the cluster forming the substrate 
     has been found, the flag is set equal to 1*/
  int found_slab = 0;
  
  
  for (int cluster_index = 0; 
       cluster_index<cluster_number; 
       cluster_index++) 
    
    /* cluster type can be either 1 in case of substrate
       or 0 in case of gas phase cluster */
    //int this_cluster_type = 0; 
    
    /* if one of the atoms making up current
       cluster is found to belong to the solid,
       then phase of all other atoms in that cluster
       are set to 1 */
    
    if ( found_slab == 0 ) {
      
      int cluster_atom_index = 0;
      
      while ( found_slab == 0   && 
	      cluster_atom_index < cluster_size[cluster_index] ) {
	
	int clust_atom_label = 
	  cluster_list[cluster_index][cluster_atom_index];
	
	/* in case one of the atoms making up 
	   current cluster is found to be an atom
	   in solid phase, current cluster is 
	   recognised to be the growing solid, 
	   and all other atoms belonging 
	   to the same cluster are marked as atoms 
	   in solid phase */
	if ( atom_state[clust_atom_label] == 1 ) {
	  
	  /* mark this cluster as the solid */
	  //this_cluster_type = 1;
	  
	  /* change value of flag */
	  found_slab = 1;
	  
	  /* mark all atoms belonging to this
	     cluster as atoms in solid phase */
	  for (int atom_index=0; 
	       atom_index<cluster_size[cluster_index]; 
	       atom_index++) {
	    int current_atom_label = 
	      cluster_list[cluster_index][atom_index];
	    atom_state[current_atom_label] = 1;
	  }
	  
	} /* endo of 	if ( atom_state[clust_atom_label] == 1 ) */
	
	
	cluster_atom_index++;
	
      } /* end of   while ( found_slab == 0   && 
	   cluster_atom_index < cluster_size[cluster_index] )   */
      
    } /* end of if ( found_slab == 0 )    */
  
  
  

  
  return 0;
  
}



/************************************************************/
