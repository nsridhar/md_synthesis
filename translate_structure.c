#include "header.h"

/* This function translate a structure 
   along the x-, y-direction 
   in such a way that the geometrical 
   centre has coordinate of 0
   across the xy-plane.
   This function is referenced when
   periodic boundary conditions are 
   applied along the x- and y-direction. */

int translate_structure_xy(int particle_number, 
			   //int x_periodic_cond,
			   //int y_periodic_cond,
			   //int z_periodic_cond,
			   double *p_xmin,
			   double *p_ymin,
			   //double *p_zmin,
			   double *x_coord,
			   double *y_coord)
//double *z_coord)
{
  
  double x_centre, y_centre,
    //z_centre,
    x_max = x_coord[0], x_min = x_coord[0], 
    y_max = y_coord[0], y_min = y_coord[0];
  //z_max = z_coord[0], 
  //z_min = z_coord[0];
  
  /* find extrema of structure */
  for ( int i=1; i<particle_number; i++){
    
    if( x_coord[i] > x_max )
      x_max = x_coord[i];
    if ( x_coord[i] < x_min )
      x_min = x_coord[i];
    if ( y_coord[i] > y_max )
      y_max = y_coord[i];
    if ( y_coord[i] < y_min )
      y_min = y_coord[i];
    //if ( z_coord[i] > z_max )
    //z_max = z_coord[i];
    //if ( z_coord[i] < z_min )
    //z_min = z_coord[i];
  }
  
  /* calculate coordinates of
     centre of structure */
  x_centre = (x_max+x_min)/2.0;
  y_centre = (y_max+y_min)/2.0;
  //z_centre = (z_max+z_min)/2.0;

  /* translate along directions where 
     periodic boundary conditions 
     are applied */
  for ( int i=0; i<particle_number; i++){
    
    //if ( x_periodic_cond )
    x_coord[i] -= x_centre;
    //if ( y_periodic_cond )
    y_coord[i] -= y_centre;
    //if ( z_periodic_cond )
    //z_coord[i] -= z_centre; 
  }
  
  
  /* passes values of smallest 
     coordinates found up onto 
     calling function */
  *p_xmin = x_min;
  *p_ymin = y_min;
  //*p_zmin = z_min;


  return 0;
}







/*************************************************/



/* Translate whole structure along z-axis
   by the quantity transl_dist */
int translate_structure_z(int particle_number, 
			  double transl_dist,
			  //double *x_coord,
			  //double *y_coord,
			  double *z_coord)
{

  /* translate whole structure 
     along z by distance trans_dirst*/
  for ( int i=0; i<particle_number; i++){
    
    z_coord[i] -= transl_dist; 
  }
  

  return 0;

}


/*************************************************/
