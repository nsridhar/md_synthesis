#include "header.h"





/* In the case of minimization run,
   this function is to be referenced 
   soon after velocities have been updated
   in an MD iteration.
   All the function  does is cancelling 
   those components of a particle velocity 
   which are antiparallel  to the force 
   acting on that particle. The 
   function also calculates temperature
   and kinetic energy of the system
   with the new velocities.
   The function returns 0 if the 
   minimisation cycle must continue,
   or 1 in the case that the target
   temperature has been achieved and
   the cycle must stop. */

int vel_quench(int atom_num,
	       //int *fix_atom_label,
	       int num_fixed_atoms,
	       int *atom_species, 
	       double *atom_mass, 
	       //double time_interval, 
	       double *force_x, 
	       double *force_y, 
	       double *force_z,
	       double *p_kin_energy,
	       double *p_temperature, 
	       double exit_temperature,
	       double *vel_x, 
	       double *vel_y, 
	       double *vel_z)
{
 
  
  double sum_vel_squared = 0.0;
  
  double velocity_verlet_conv_fact = joule_per_ev/
    (kg_per_amu*metre_per_angstrom*metre_per_angstrom);
  
  //double velocity_verlet_fact = 
  //0.5*time_interval*velocity_verlet_conv_fact;
  
  
  /* Update velocities. */
  
  for (int i=0; i<atom_num; i++) {
    
 
    /* cancel components of velocity wich are 
       antiparallel to force */
    if ( vel_x[i]*force_x[i] < 0.0 )
      vel_x[i] = 0.0;
    if ( vel_y[i]*force_y[i] < 0.0 )
      vel_y[i] = 0.0;  
    if ( vel_z[i]*force_z[i] < 0.0 )
      vel_z[i] = 0.0;
    
    /* Calculate contribution of atom i to kinetic energy */
    sum_vel_squared += atom_mass[atom_species[i]] *
      (vel_x[i]*vel_x[i] + vel_y[i]*vel_y[i] + vel_z[i]*vel_z[i]);
    
    
  } // end of  for (int i=n_fixed_atom; i<atom_num; i++)
  
  
  
  /* Calculate kinetic energy (in eV) */
  
  /* Kin_energy = 1/2 sum(i=1,2,...,N) mass[i]*v[i]^2 */
  *p_kin_energy = 0.5 * sum_vel_squared /
    velocity_verlet_conv_fact;
  
  assert( atom_num-num_fixed_atoms >= 0 );
  /* Calculate current system temperature in K */
  /* (3/2)*N*kb*T = kin_energy => T = (2/3)*kin_energy/(N*kb)  */
  if ( atom_num-num_fixed_atoms > 0 ) 
    *p_temperature =  2.0 * kinetic_energy / 
      (3.0*(atom_num-num_fixed_atoms)*kb); 
  else
    *p_temperature = 0.0;
  
  if ( *p_temperature < exit_temperature) /* exit minimisation */
    return 1; 
  else                                  /* continue with the cycle */
    return 0;
  
  
}




/********************************************************************/








/* Velocity update is performed according
   to the Velocity-Verlet scheme: 
   v(t) = v(t-dt) + dt*(a(t-dt)+a(dt))/2;
   where a(t) is the particle acceleration 
   at time t.
   In this code implementation, velocity
   update is carried out partially:
   once before and once after the forces
   have been updated. This corresponds
   to reassigning velocities as:
   v_i = v_i +  dt*f_i/(2*m) 
   twice per MD iteration. Here
   v_i, f_i, and m_i are
   the velocity, the force,
   and the mass of particle i in the 
   current configuration, respectively.
   This function is to be referenced 
   as the second half of velocity update,
   i.e. after the forces have been updated.
   In addition to partially updating 
   velocities in the same manner as function 
   vel_partial_update does, 
   this function  cancel all components 
   of a particle velocity which are 
   antiparallel  to the force acting 
   on that particle.
   The function returns 0 if the 
   minimisation cycle must continue,
   or 1 in the case that the target
   temperature has been achieved and
   the cycle must stop. */



int vel_quench_old(int atom_num,
	       int *fix_atom_label,
	       int num_fixed_atoms,
	       int *atom_species, 
	       double *atom_mass, 
	       double time_interval, 
	       double *force_x, 
	       double *force_y, 
	       double *force_z,
	       double *p_kin_energy,
	       double *p_temperature, 
	       double exit_temperature,
	       double *vel_x, 
	       double *vel_y, 
	       double *vel_z)
{
 
  
  double sum_vel_squared = 0.0;
  
  double velocity_verlet_conv_fact = joule_per_ev/
    (kg_per_amu*metre_per_angstrom*metre_per_angstrom);
  
  double velocity_verlet_fact = 
    0.5*time_interval*velocity_verlet_conv_fact;
  
  
  /* Update velocities. */
  
  for (int i=0; i<atom_num; i++) {
    
    //assert(atom_species[i]>=0 && atom_species[i]<num_species);
    
    if ( !fix_atom_label[i] ) {
      double mult_fact = 
	velocity_verlet_fact/atom_mass[atom_species[i]];
      
      /* The force calculated by function force is 
	 expressed in eV/A.
	 The force is converted into Newton=Joule/m 
	 by multplying it by factor of joule_per_eV/m_per_angstrom.
	 Multiplication of the atomic mass by factor of kg_per_amu 
	 gives mass in Kg. The incremental velocity is then 
	 calculated and  expressed in m/s. 
	 After dividing by factor of m_per_angstrom, 
	 the velocity is finally expressed in A/s. */
      
      vel_x[i] += mult_fact*force_x[i];
      vel_y[i] += mult_fact*force_y[i];
      vel_z[i] += mult_fact*force_z[i];
    }
    
    /* cancel components of velocity wich are 
       antiparallel to force */
    if ( vel_x[i]*force_x[i] < 0.0 )
      vel_x[i] = 0.0;
    if ( vel_y[i]*force_y[i] < 0.0 )
      vel_y[i] = 0.0;  
    if ( vel_z[i]*force_z[i] < 0.0 )
      vel_z[i] = 0.0;
    
    /* Calculate contribution of atom i to kinetic energy */
    sum_vel_squared += atom_mass[atom_species[i]] *
      (vel_x[i]*vel_x[i] + vel_y[i]*vel_y[i] + vel_z[i]*vel_z[i]);
    
    
  } // end of  for (int i=n_fixed_atom; i<atom_num; i++)
  
  
  
  /* Calculate kinetic energy (in eV) */
  
  /* Kin_energy = 1/2 sum(i=1,2,...,N) mass[i]*v[i]^2 */
  *p_kin_energy = 0.5 * sum_vel_squared /
    velocity_verlet_conv_fact;
  
  assert( atom_num-num_fixed_atoms >= 0 );
  /* Calculate current system temperature in K */
  /* (3/2)*N*kb*T = kin_energy => T = (2/3)*kin_energy/(N*kb)  */
  if ( atom_num-num_fixed_atoms > 0 ) 
    *p_temperature =  2.0 * kinetic_energy / 
      (3.0*(atom_num-num_fixed_atoms)*kb); 
  else
    *p_temperature = 0.0;
  
  if ( *p_temperature < exit_temperature) /* exit minimisation */
    return 1; 
  else                                  /* continue with the cycle */
    return 0;
  
  
}




/********************************************************************/


