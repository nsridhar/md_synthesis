#bin/icc
#CC = gcc
CC = icc -std=c99

CCFLAGS = -c 
#PROFFLAGS = -pg 
#OPTFLAGS = -O3 
DEBUGFLAGS = -g -Wall 
#MEMORYMODELFLAG = -mcmodel=small -O0


OBJ =  	arrays_management.o\
	calculate_10_5_potential.o\
	calculate_born_mayer_potential.o\
	calculate_coulomb_potential.o\
	calculate_hybrid_potential.o\
	calculate_coulomb_pluslj.o\
	calculate_temperature.o\
	calculate_truncation_params.o\
	cluster.o\
	cluster_linked_list.o\
	cluster_search.o\
	construct_pair_list.o\
	find_z_fix_atom.o\
	find_z_thermostat.o\
	force.o\
	funct.o\
	init_substrate_vel.o\
	introduce_new_atom.o\
	int_linked_list.o\
	label_fixed_atom.o\
	new_atom_subroutines.o\
	output_configuration.o\
	output_restart_file.o\
	output_velocity.o\
	print_header.o\
	print_summary.o\
	ran2.o\
	read_cluster_input.o\
	read_initial_configuration.o\
	read_initial_velocities.o\
	read_input_sridhar.o\
	set_initial_structure.o\
	set_potentials_sridhar.o\
	therma.o\
	update_cells.o\
	vel.o\
	vel_quench.o\
	velocity_rescaling.o\
	write_adatom_coords.o\
	write_energy.o


MAIN = main.o

EXEC = md_born_mayer_2012
VER = 3.3.2.1
NAMEEXE = $(EXEC)_$(VER)


#Directory where library files are
LIB_DIR = ./lib

#libraries to use when linking the main program
#the '-L' option is used to tell the compiler where to look for 
#libraries, in addition to the normal system library locations.
LIBS = -L$(LIB_DIR) -l_gauss_jordan

LINK = -o $(NAMEEXE)

main: $(OBJ) $(MAIN)
	$(CC)  $(OPTFLAGS) $(DEBUGFLAGS) $(PROFFLAGS)  $(MEMORYMODELFLAG) $(LINK) $(OBJ) $(MAIN) $(LIBS) 

main.o: main.c
	$(CC) $(CCFLAGS) $(OPTFLAGS) $(DEBUGFLAGS) $(PROFFLAGS)  $(MEMORYMODELFLAG)   main.c 

%o: %c
	$(CC) $(CCFLAGS) $(OPTFLAGS) $(DEBUGFLAGS) $(PROFFLAGS) $(MEMORYMODELFLAG) $< 

#$(OBJ): 
#	$(CC) $(CCFLAGS) $*.c


clean:
	rm *.o *.*~ $(NAMEEXE)
