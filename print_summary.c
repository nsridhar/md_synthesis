//#include "log_file.h"
#include "header.h"

//int print_summary(char *file_name, md_params *run_params)
int print_summary(char *file_name)
{
  FILE *p_file;

  if ( (p_file=fopen(file_name,"a")) == NULL ) {
    printf("%s\n%s%s.\n%s\n",
	   "A fatal error occurred in function write_summary(char *)",
	   "while trying to open file ",file_name,
	   "It is impossible to continue, sorry!");
    exit(1);
  }
  
  fprintf(p_file,"\nThe program terminated successfully!\n");
  fprintf( p_file,"Total number of iterations: %lu\n", 
	   (current_block-2)*n_step+(current_step-1) );
  fprintf(p_file, "Block counter at the end of iterations: %d\n",
	  current_block );
  fprintf(p_file, "Step counter at the end of iterations: %lu\n",
	  current_step );
  fprintf(p_file,"Number of blocks: %d\n", n_block);
  fprintf(p_file,"Number of steps per block: %lu\n", n_step);
  fprintf(p_file,"Configuration output every %lu iterations\n",
	  config_output_freq);
  fprintf(p_file,"Velocity output every %d iterations\n",
	  velocity_output_freq);
  fprintf(p_file,"Energy output every %d iterations\n",
	  energy_output_freq);
  fprintf(p_file,"Final system time: %.3e s\n", current_time);
  fprintf(p_file,"Atom number at end of simulation: %d\n",
	  n_tot_atom);
  fprintf(p_file,"Final structure temperature: %.3e K\n", 
	  final_system_temperature);
  fprintf(p_file,"Final structure potential energy: %.5lf eV\n",
	  final_pot_energy);
  if ( !energy_minimisation )
    fprintf(p_file,"Number of deposited atoms: %d\n",
	    n_tot_atom-n_initial_atom);
  if ( single_adsorbate ) {
    //fprintf(p_file, "Final coordinates of adatom: %.3lf, %.3lf, %.3lf\n",
    //    x[n_tot_atom-1], y[n_tot_atom-1], z[n_tot_atom-1]);
    fprintf(p_file, "Number of times of adatom border crossings along x direction: %d\n",
	    n_adatom_x_cross);
    fprintf(p_file, "Number of times of adatom border crossings along y direction: %d\n",
	    n_adatom_y_cross);
  }
  
  fprintf(p_file,"\n*************************************************************************************\n\n");
  
  
  fclose(p_file);
  
  return(0);
    

}
