#include "header.h"

/* This function calculates the Lennard-Jones 
   interaction between an atom 1 and an atom 2 
   which are a distance (dist12_squared)^1/2 
   apart. The potential is also derived with 
   respect to distance between atom 1 and 2.
   The value pointed to by argument 
   p_dlj12_times_dist is the derivative of LJ
   potential with respect to atom distance, 
   multiplied by the same distance. 
   (Apologies for this unnecessary complication!) */

int calculate_lj_potential(double dist12_squared,
			   double *p_lj_pot12,
			   double *p_dlj12_times_dist,
                           double q1, double q2, 
                           double alpha_coulomb_12,
			   double sigma12,
			   double epsilon12,
			   double cutoff_dist12,
                           double pot_range_12,
			   double *truncation_params)
{

  extern int 
    calculate_coulomb_potential
    (double r,
     double r_cut,
     double q1, double q2,
     double alpha,
     double *p_coulomb_pot,
     double *p_dpot_times_r);

  /* contribution to potential 
     energy due to interaction 
     between atom i and j */
  double ener_12 = 0.0; 
  /* derivative of potential 
     with respect to distance 
     between each atom couple 
     multiplied by distance */
  double dv12_over_dr12_times_dist = 0.0;

/*Calculate distance between atom 1 and 2 */
 double dist_12 = sqrt(dist12_squared) ;
  
 
  if ( dist_12 <= 
       cutoff_dist12 ) { /*Interaction within  cutoff radius*/

    /* calculate coulombic part of interaction 
       and its derivative multiplied by distance */
    double coulomb_pot, coulomb_pot_deriv_times_dist;
    calculate_coulomb_potential(dist_12,
				pot_range_12,
				q1, q2, 
				alpha_coulomb_12,
				&coulomb_pot,
				&coulomb_pot_deriv_times_dist);


/* calculate remaining parts of the interaction*/
   
    double sigma_over_r_2 = (sigma12*sigma12)/dist12_squared;
    double sigma_over_r_6 = pow(sigma_over_r_2,3);
    double sigma_over_r_12 = pow(sigma_over_r_6,2);
    /* calculate contribution to potential 
       energy due to the interaction 
       between atom i and j */
    ener_12 = coulomb_pot + 4.0*epsilon12
      *(sigma_over_r_12-sigma_over_r_6);




//printf("LJWC\t %f\t %f\t %f\t %f\t %f\t %f %f\t\n", q1, q2, dist_12, sigma12, epsilon12, coulomb_pot, ener_12);


    /* Calculate derivative of potential 
       with respect to r_12 and 
       multiply by r_12 */
    dv12_over_dr12_times_dist = coulomb_pot_deriv_times_dist +  24.0*epsilon12*(sigma_over_r_6 - 2.0*sigma_over_r_12);
    
  }
 





 
  else { /* Interaction outside cutoff radius. 
	    Use truncation parameters */
    
    double dist_12 = sqrt(dist12_squared),  
      coeff[6], 
      dist_pow[6];
    
    for(int n=0; n<6; n++)
      /* coeff[n] = an, 
	 with 
	 pot(r) = 
	 a0+a1*r+a2*r^2+a3*r^3+...+a5*r^5 */
      coeff[n] = truncation_params[n];
    
    dist_pow[0] = 1;
    dist_pow[1] = dist_12;
    dist_pow[2] = dist12_squared;
    for(int n=3; n<6; n++)
      /* dist_pow[n] = (r_12)^n */
      dist_pow[n] = dist_12*dist_pow[n-1]; 
    
    /* Calculate contribution 
       to potential energy
       due to interaction 
       between atom 1 and 2 */
    for(int n=0; n<6; n++)
      ener_12 += coeff[n]*dist_pow[n]; 	 
    
    /* Calculate derivative 
       of potential 
       with respect to r_ij 
       and multiply by r_ij */
    for (int n=1; n<6; n++) 
      dv12_over_dr12_times_dist += 
	n*coeff[n]*dist_pow[n];
  }
  
  
  /* "export" calculated 
     quantities to 
     calling function */
  *p_lj_pot12 = ener_12;
  *p_dlj12_times_dist =  dv12_over_dr12_times_dist;


//printf("LJOC\t %f\t %f\t %f\t %f\t %f\t %f %f\t\n", q1, q2, dist_12, sigma12, epsilon12,  dv12_over_dr12_times_dist, ener_12);
  return 0;
}



/*********************************************************/


int calculate_lj_second_deriv(double dist12_squared,
			      double *p_pot12_second_deriv,
                              double q1, double q2,
                              double alpha_coulomb_12,
                              double sigma12,
			      double epsilon12)
{

  extern int 
    calculate_coulomb_second_deriv
    (double r,
     double q1, double q2,
     double alpha,
     double *p_coulomb_second_deriv);


  double pot_second_deriv, coulomb_part_second_deriv;
 
   double dist_12 = sqrt(dist12_squared); 
   
  calculate_coulomb_second_deriv
    (dist_12,
     q1, q2,
     alpha_coulomb_12,
     &coulomb_part_second_deriv);


  
  double sigma_over_r_2 = (sigma12*sigma12)/dist12_squared;
  double sigma_over_r_6 = pow(sigma_over_r_2,3);
  /* calculate contribution to potential 
     energy due to the interaction 
     between atom i and j */
  pot_second_deriv = 
    24.0*epsilon12*sigma_over_r_6/dist12_squared*
    (26.0*sigma_over_r_6-7) + coulomb_part_second_deriv;
  
  *p_pot12_second_deriv = pot_second_deriv;

  return 0;
}
