#include "header.h"

/* This subroutine divides the simulation unit cell
   into cross sections (slices) of height "slice_height". 
   "max_height" and "min_height" indicate the maximum
   and minimum z coordinate of the simulation cell,
   respectively.
   The kinetic energy of all particles whithin each 
   slice is calculated and is used to work out the 
   temperature of the particles lying within that slice.
   The temperatures thus calculated are written in
   an output file. */

int output_temperature(int block_num, int step_num, int atom_num, 
		       int num_substr_plane, int atom_per_plane, 
		       double slice_height, 
		       double max_height, double min_height, 
		       double *z_pos, 
		       int *atom_species, double *atom_mass,
		       double *x_vel, double *y_vel, double *z_vel)
{

  /* check for slice_height not to exceed 
     total height of simulation cell */
  if ( (max_height-min_height) <= slice_height ) {
    printf("%s\n%s%.3lf%s\n%s%.3lf%s\n%s\n",
	   "Warning in function output_temperature!",
	   "The slice thickness chosen: ", slice_height, " Angstr",
	   "is not smaller than the simulation cell total height: ",
	   max_height-min_height, " Angstr.",
	   "This function is returning 1.");
    return(1);
  }

  char *step = int_to_string(step_num);
  char *block = int_to_string(block_num);
	
  char output_file_name[strlen(block)+strlen(step)+16];
  strcpy(output_file_name,block);
  strcat(output_file_name,"bl_");
  strcat(output_file_name,step);
  strcat(output_file_name,"step_temp.out");
  
  FILE *p_output_file;
  
  if( (p_output_file=fopen(output_file_name,"w")) == NULL )    
    {
      printf("Error in function temperature_output.\n");
      printf("A fatal error occured while opening file %s\n",
	     output_file_name);
      printf("Cannot continue, sorry!\n");
      exit(1);
    }

  fprintf(p_output_file, "%13s%15s%10s\n\n", 
	  "#z pos/Angstr", "Temp/K", "Atom num");
  //fprintf(p_output_file, "\n");

  assert( (max_height-min_height) >= 0 );
  assert( slice_height > 0 );
  int ratio = (int)((max_height-min_height)/slice_height);
  int slice_num = ( ratio > 0 ) ? ratio : 1;
  
  int *n_per_slice = malloc(slice_num*sizeof(int));
  if ( n_per_slice == NULL ) {
    printf("%s\n%s\n%s\n%s\n",
	   "Fatal error in function output_temperature.",
	   "The pointer n_per_slice has been assigned",
	   "with the NULL value due to lack of memory.",
	   "It is impossible to continue, sorry!");
    exit(1);
  }
  else
    for (int i=0; i<slice_num; i++)
      n_per_slice[i] = 0;
  
  int **slice_list = malloc(slice_num*sizeof(int*));
  if ( slice_list == NULL ) {
    printf("%s\n%s\n%s\n%s\n",
	   "Fatal error in function output_temperature.",
	   "The int* pointer slice_list has been assigned",
	   "with the NULL value due to lack of memory.",
	   "It is impossible to continue, sorry!");
    exit(1);
  }
  
  for (int i=0; i<slice_num; i++) {
    slice_list[i] = malloc(atom_num*sizeof(int));
    if ( slice_list[i] == NULL ) {
      printf("%s\n%s%d%s\n%s\n%s\n",
	     "Fatal error in function output_temperature.",
	     "The int pointer slice_list[",i,"] has been assigned",
	     "with the NULL value due to lack of memory.",
	     "It is impossible to continue, sorry!");
      exit(1);
    }
    //for (int j=0; j<atom_num; j++)
    //slice_list[i][j] = 0;
  }

  /* assign each atom with a simulation box slice */
  for ( int i=0; i<atom_num; i++) {
      int slice_i = (int)((z_pos[i]-min_height)/slice_height);
      if ( slice_i > slice_num-1 )
	slice_i = slice_num-1;
      slice_list[slice_i][n_per_slice[slice_i]] = i;
      n_per_slice[slice_i]++;
    }
  

  double conversion_factor = (kg_per_amu*metre_per_angstrom*
			      metre_per_angstrom) / joule_per_ev;    


  /* cycle over all slices */

  for (int j=0; j<slice_num; j++) {

    double vel_sum_sq = 0.0;

    for (int k=0; k<n_per_slice[j]; k++){
      
      int at_lab = slice_list[j][k];
      
      vel_sum_sq += atom_mass[atom_species[at_lab]] *
	(x_vel[at_lab]*x_vel[at_lab] + 
	 y_vel[at_lab]*y_vel[at_lab] + 
	 z_vel[at_lab]*z_vel[at_lab]);
    }
    
    /* Initially, velocities are expressed in A/m and
       masses in a.m.u.
       After multiplying velocities by (metre_per_angstr)^2
       and masses by kg_per_amu, the product m*v^2 is 
       expressed in Joule = Kg*m^2/s^2.
       Finally, after dividing by joule_per_ev, the product 
       m*v^2 is expressed in eV. */
       
    double kinetic_energy  = 0.5*vel_sum_sq*conversion_factor;
    
    /* calculate temperature for this slice */
    /* (3/2)*N*kb*T = kin_energy => T = (2/3)*kin_energy/(N*kb)  */
    double this_temp = 0;
    assert( n_per_slice[j] >= 0 );
    if ( n_per_slice[j] > 0 )
      this_temp = 2.0*kinetic_energy / 
	(3.0*n_per_slice[j]*kb);

    /* write temperature in output file */
    fprintf(p_output_file, "%13.2lf%15.2lf%10d\n", 
	    min_height+(j+1)*slice_height, 
	    this_temp, n_per_slice[j]);
    
  } /* end of for (int j=0; j<slice_num; j++)  */ 




  /* calculate temperature of each substrate layer */
  
  fprintf(p_output_file, "\n%6s %12s %13s %9s\n",
	  "#layer", "z pos/Angstr", "Temperature/K", "Atom Num");
  
  int substr_plane_index = 0;
  for (int i=0; i<num_substr_plane; i++) {
    
    double kinetic_energy = 0.0;

    double average_z_pos = 0.0;
    
    int atom_cnt = 0;
    
    /* loop over atoms in slice i */
    for (int j=substr_plane_index; 
	 j<substr_plane_index+atom_per_plane; j++) {
      
      atom_cnt++;
      /* accumulate sum of squared velocity times mass */
      kinetic_energy += atom_mass[atom_species[j]] *
	(x_vel[j]*x_vel[j] + y_vel[j]*y_vel[j] + 
	 z_vel[j]*z_vel[j]);
      /* accumulate sum of z positions of atoms for calculating 
	 average z coordinate of this plane */
      average_z_pos += z_pos[j];
    }
    
    /* calculate temperature of this layer */
    double layer_temp = conversion_factor*kinetic_energy /
      (3.0*atom_per_plane*kb);
    
    /* calculate average z coordinate of this plane */
    average_z_pos /= atom_per_plane;

    /* write temperature in output file */
    fprintf(p_output_file, "%6d %12.2f %13.3f %9d\n",
	    i+1, average_z_pos, layer_temp, atom_cnt);
    
    substr_plane_index += atom_per_plane;
    
  }
  
  


  fclose(p_output_file);


  /* free dinamically allocated memory */

  free(n_per_slice);

  for (int i=0; i<slice_num; i++)
    free(slice_list[i]);

  free(slice_list);


  return 0;

}
