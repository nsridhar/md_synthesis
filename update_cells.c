/* updates the particle list of 
   each cell stored in the array
   cell_list and 
   fill elements of array cell_pointer
   accordingly.
   cell_pointer[m+1]-cell_pointer[m] is the
   number of particles in cell m, 
   m = 0, 1, 2, 3... M-1;
   where M is the total number of cells.

   Example:
   Let's suppose that the array cell list
   looks like the following:
   cell_list = {12 36 8 | 92 3 62 12 17 | | 71 13 | 26 55  7  4}  
   array index   0  1 2    3 4  5  6  7      8  9   10 11 12 13 
   cell index      0            1        2    3          4     ;
   where the character "|" serves a separator between cells.
   Then, the cell_pointer array will look like the following
   cell_pointer =        {0 | 3 | 8 | 8 |10 | 14}
   array and cell index   0   1   2   3   4    5

   Please note that the length of the array cell_pointer 
   must be M+1, if the simulation box has been divided 
   into M cells. Values stored in cell_pointer vary in 
   the range [0,N+1], if N is the number of particles 
   in the system.  In particular, please notice that
   cell_pointer[M] must always be equal to N+1 */
   

//#include "header.h"
#include "int_linked_list.h"

int update_cells(int particle_num,
		 int cellx_num,
		 int celly_num,
		 int cellz_num,
		 double boxx_size,
		 double boxy_size,
		 double boxz_size,
		 double x_min,
		 double y_min,
		 double z_min,
		 int *cell_pointer,
		 int *cell_list,
		 double *x_pos, 
		 double *y_pos,
		 double *z_pos)
{
  
  /* find total number of cells */
  int cell_tot_num = 
    cellx_num*celly_num*cellz_num;
  
  /* calculate reciprocal of cell size
     in all three dimensions */
  double one_over_cellx_len = cellx_num/boxx_size;
  double one_over_celly_len = celly_num/boxy_size;
  double one_over_cellz_len = cellz_num/boxz_size;

  /* declare an array of integer-linked-lists,
     one list is associated with a cell */
  int_linked_list *cell_lnkd_list;
  cell_lnkd_list = malloc(cell_tot_num*sizeof(int_linked_list));
  /* initialize linked lists of all cells */
  for (int i=0; i<cell_tot_num; i++)
    cell_lnkd_list[i] = create_int_list();
  
  /* associate each particle with a cell */
  for (int i=0; i<particle_num; i++) {
    /* find "coordinates" of the cell
       this atom belongs to */
    int n_x = (int)((x_pos[i]-x_min)*one_over_cellx_len);
//  printf("n_x=%d, x_pos[i]= %f, x_min= %f , one_over_cellx_len = %f\n", n_x, x_pos[i], x_min, one_over_cellx_len);
    assert( n_x > -1  
	    && 
	    n_x < cellx_num+1 );
    if ( n_x == cellx_num ) 
      if ( fabs(x_pos[i]-x_min-boxx_size) < 1.0e-10 )
	n_x--;
      else{
	printf("\n%s\n%s%d%s%.3lf\n%s%d%s\n%s\n\n",
	       "Error in function update_cells!",
	       "Atom ", i, ", with x coordinate ", x_pos[i],
	       "has been calculated to have n_x equal to ",
	       n_x, ".",
	       "It is impossible to continue, sorry!");
	exit(1);
      }
    int n_y = (int)((y_pos[i]-y_min)*one_over_celly_len);
    assert( n_y > -1 
	    &&
	    n_y < celly_num+1 );
    if ( n_y == celly_num ) 
      if ( fabs(y_pos[i]-y_min-boxy_size) < 1.0e-10 )
	n_y--;
      else{
	printf("\n%s\n%s%d%s%.3lf\n%s%d%s\n%s\n\n",
	       "Error in function update_cells!",
	       "Atom ", i, ", with y coordinate ", y_pos[i],
	       "has been calculated to have n_y equal to ",
	       n_y, ".",
	       "It is impossible to continue, sorry!");
	exit(1);
      }
    int n_z = (int)((z_pos[i]-z_min)*one_over_cellz_len); 
    //assert( n_z > -1  
    //    && 
    //    n_z < cellz_num+1 );
    if  ( n_z < 0  
	  || 
	  n_z > cellz_num ) {
      	printf("\n%s\n%s%d%s%.3lf\n%s%d%s\n%s%d%s\n%s\n\n",
	       "Error in function update_cells!",
	       "Atom ", i, ", with z coordinate ", z_pos[i],
	       "has been calculated to have n_z equal to ",
	       n_z, ".",
	       "Number of cells along z is ", cellz_num, ".",
	       "This function is returning 1.");
	return 1;
    }
    if ( n_z == cellz_num ) 
      if ( fabs(z_pos[i]-z_min-boxz_size) < 1.0e-10 )
	n_z--;
      else{
	printf("\n%s\n%s%d%s%.3lf\n%s%d%s\n%s\n\n",
	       "Error in function update_cells!",
	       "Atom ", i, ", with z coordinate ", z_pos[i],
	       "has been calculated to have n_z equal to ",
	       n_z, ".",
	       "It is impossible to continue, sorry!");
	exit(1);
      }
    int cell_index = 
      cellx_num*celly_num*n_z +
      cellx_num*n_y + 
      n_x;
    if  ( cell_index > cell_tot_num-1 
	  || 
	  cell_index < 0 ) {
      printf("\n%s\n%s%d%s%.3lf%s%.3lf%s%.3lf%s\n%s%d%s\n%s%d%s\n%s\n\n",
	     "Fatal error in function update_cells!",
	     "The cell index of atom ", i,
	     ", in position (", x_pos[i], ",",
	     y_pos[i], ",", z_pos[i], ")",
	     "has been calculated to be ", cell_index, ".",
	     "The total number of cells is ", cell_tot_num, ".",
	     "This function is exiting and returning 1.");
      return 1;
    }
    //assert ( cell_index < cell_tot_num 
    //     && 
    //     cell_index > -1 );
    insert_element_int_list(i, cell_lnkd_list[cell_index]);
  }
  
  
  /* copy cell lists in 
     array cell_list and 
     update array cell_pointer 
     accordingly */
  cell_pointer[0] = 0;
  int cell_array_ind = 0;
  for (int i=0; i<cell_tot_num; i++) {
    for (int_linked_list list_iterator = cell_lnkd_list[i];
	 list_iterator->next != NULL;
	 list_iterator = list_iterator->next ) 
      cell_list[cell_array_ind++] = 
	list_iterator->next->d;
    cell_pointer[i+1] = cell_array_ind;
  }
  
  /* set last element of cell pointer
     equal to particle_num */
  //cell_pointer[cell_tot_num] = particle_num;
  
  /* free dynamically allocated memory */
  for (int i=0; i<cell_tot_num; i++)
    delete_int_list(cell_lnkd_list+i);
  free(cell_lnkd_list);
  
  return 0;
}
