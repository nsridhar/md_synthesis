/* This function initialises randomly atom velocities
in such a way that the system is initially at the 
specified temperature.
The function initialises all substrate atoms. */

#include "header.h"

int init_substrate_vel(int num_substrate_species, 
		       int fixed_atom_num,
		       int *fixed_atom_label,
		       int *atoms_per_species, int *atom_species,
		       double *atom_mass,
		       double temperature, 
		       double *vx, double *vy, double *vz) 
{
  
  int num_substrate_atom = 0;
  for (int i=0; i<num_substrate_species; i++)
    num_substrate_atom += atoms_per_species[i];

  double px_cm=0.0, py_cm=0.0, pz_cm=0.0, mass_cm=0.0, 
    sum_v2m = 0.0;  
  double target_kin_en, current_kin_en, rescaling_factor;

  /*set to zero all velocities*/
  for(int i=0; i<num_substrate_atom; i++){
    vx[i] = 0.0;
    vy[i] = 0.0;
    vz[i] = 0.0;
  }

  /* generate random velocities for each atom 
     between -1 and 1 */
  //for(int i=fixed_atom_num; i<num_substrate_atom; i++) {
  for(int i=0; i<num_substrate_atom; i++) 
    if ( !fixed_atom_label[i] ) {
      vx[i] =  2.0*ran2() - 1.0;
      vy[i] =  2.0*ran2() - 1.0;
      vz[i] =  2.0*ran2() - 1.0;
      px_cm += atom_mass[atom_species[i]]*vx[i];
      py_cm += atom_mass[atom_species[i]]*vy[i];
      pz_cm += atom_mass[atom_species[i]]*vz[i];
      mass_cm += atom_mass[atom_species[i]];
    }
  
 
  /*subtract center of mass velocity*/
  /*and calculate sum_i(mi*vi^2) */
  //for(int i=fixed_atom_num; i<num_substrate_atom; i++) {
  for(int i=0; i<num_substrate_atom; i++) 
    if ( !fixed_atom_label[i] ) {
      vx[i] -= px_cm/mass_cm;
      vy[i] -= py_cm/mass_cm;
      vz[i] -= pz_cm/mass_cm;
      sum_v2m += 
	(vx[i]*vx[i] + 
	 vy[i]*vy[i] + 
	 vz[i]*vz[i])*
	atom_mass[atom_species[i]];
    }
  
  /*calculate kinetic energy in Joule 
    corresponding to target energy*/
  target_kin_en = 
    3.0*(double)(num_substrate_atom-fixed_atom_num)*kb*
    temperature*joule_per_ev/2.0;                /* Kin = (3/2)*N*kb*T */
  printf("Calculated target kinetic energy in J is %8.4e\n",
	 target_kin_en);
  
  /*calculate current kinetic energy in Joule*/
  current_kin_en = 0.5*sum_v2m*kg_per_amu;
  printf("Calculated current kin energy in J is %8.4e\n",
	 current_kin_en);

  /*calculate factor for velocity rescaling*/
  if (fabs(current_kin_en) > 1e-50)
    rescaling_factor = sqrt(target_kin_en/current_kin_en);
  else {
    printf("%s\n%s%.3lf.\n%s%8.4e\n%s\n",
	   "Warning in function init_substrate_vel referenced with",
	   "temperature equal to ", temperature,
	   "The kinetic energy before rescaling has been found to be ", 
	   current_kin_en,
	   "All initial velocities are being set to 0.");
    rescaling_factor = 0.0;
  }
  printf("Rescaling factor is %8.4e\n",
	 rescaling_factor);
  
  /* multiply all velocities by rescaling factor in order
     to lead the system to target temperature */
  //for(int i=fixed_atom_num; 
  //  i<num_substrate_atom; i++){
  for(int i=0; i<num_substrate_atom; i++){
    vx[i] *= rescaling_factor;
    vy[i] *= rescaling_factor;
    vz[i] *= rescaling_factor;
    //for(int j=0; j<3; j++)
    //(atom_array+i)->velocity[j] *= rescaling_factor;
  }
  
  /* express velocities in A/s */
  //for(int i=fixed_atom_num; 
  //  i<num_substrate_atom; i++) {
  for(int i=0; i<num_substrate_atom; i++){
    vx[i] /= metre_per_angstrom;
    vy[i] /= metre_per_angstrom;
    vz[i] /= metre_per_angstrom;
  }
 
  
  

  /*calculate initial temperature for testing purposes*/
  {
    double sum_vsquared_m = 0.0;
    double initial_kin, initial_temp;
    
    //for(int i=fixed_atom_num; i<num_substrate_atom; i++)
    for(int i=0; i<num_substrate_atom; i++)
      sum_vsquared_m += 
	(vx[i]*vx[i] + 
	 vy[i]*vy[i] + 
	 vz[i]*vz[i])*
	atom_mass[atom_species[i]];
    
    initial_kin = 0.5*kg_per_amu*sum_vsquared_m*
      metre_per_angstrom*metre_per_angstrom;
    
    if ((num_substrate_atom-fixed_atom_num)>0)
      initial_temp = 2.0*initial_kin/
	(3.0*(num_substrate_atom-fixed_atom_num)*kb*joule_per_ev);
    else {
      printf("%s\n%s%d\n%s\n",
	     "Warning in function init_substrate_vel.",
	     "The number of atoms passed is ", 
	     num_substrate_atom-fixed_atom_num,
	     "Setting initial temperature to 0.");
      initial_temp = 0;
    }
    printf("Initial kinetic energy is %6.4e J, initial temperature %8.4e K\n",
	   initial_kin, initial_temp);
  }
  
  return (0);
}
