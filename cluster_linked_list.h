#ifndef _CLUST_LNKD_LIST_HEADER_H_
#define _CLUST_LINKD_LIST_HEADER_H_

#include <stdio.h>
#include <stdlib.h>
#include "cluster.h"


typedef cluster data;


/* definition of list node */  
typedef struct cluster_list_node{
  data d;
  struct cluster_list_node *next;
} cluster_list_node;


/* definition of linked 
   list of clusters */
typedef cluster_list_node *cluster_linked_list;



/*************************************************/



/* prototypes of functions on list */


/* returns number of elements
   in the list*/
int count_element_in_list(cluster_linked_list the_list);


/* makes an array of clusters contained
   in the list cluster_list,
   when referenced, this function must
   always be followed by the function
   delete_clust_array before the cluster
   array created gets out of scope */
cluster* 
create_clust_array(int cluster_num,
		   cluster_linked_list cluster_list);


/* Creates and initialises 
   an empty  list
   The first element in the list
   is just a sentinel */
cluster_linked_list create_list();


/* free memory allocated by function 
   create_cluster_array. Always invoke
   this function before the array created
   with create_cluster_array gets out of scope */
int delete_clust_arrray(int clust_array_size,
			cluster *clust_array);

/* eliminates one element from the list
   returns 1 if the elimination was successful,
   and 0 otherwise */
int delete_element(data the_data, 
		   cluster_linked_list the_list);

/* delete all elements of the list, 
   including the sentinel node
   Always invoke this function before 
   a cluster list created with function 
   create_list() gets out of scope */
int delete_list(cluster_linked_list *p_list);

/* returns 1 if element is in the list
   and 0 otherwise */
int element_in_list(data the_data, 
		    cluster_linked_list the_list);


/* Returns 1 if the list is empty 
   and 0 otherwise */
int empty_list(cluster_linked_list the_list);



/* inserts an element in the list, 
   the list is sorted in ascending order */
int insert_element_in_list(data the_data,
			   cluster_linked_list the_list);



/* returns data contained in 
   a specific position in the list 
   (not implemented) */
cluster 
get_list_data(int list_position,
	      cluster_linked_list the_list);


/* print information regarding data contained 
   in first node of the list */
int print_node_data(cluster_linked_list the_list);

#endif
