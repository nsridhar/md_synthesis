#include "header.h"


int update_nbr_list_verlet(int num_species, int *atoms_per_species,
			   int *atom_species, 
			   int *verlet_nbr_num, 
			   int verlet_nbr_list[][n_max_atom],
			   double* verlet_dist, 
			   double verlet_length[][n_max_type],
			   double verlet_shell_width[][n_max_type],
			   double x_box, double y_box, double z_box,
			   double distance_x[][n_max_atom], 
			   double distance_y[][n_max_atom], 
			   double distance_z[][n_max_atom],
			   double *x, double *y, double *z)

{

  //int solid_gas_interaction = 0;
  int n_atom = 0;
  int active_interaction[num_species][num_species];
  for ( int i=0; i<num_species; i++)
    for (int j=0; j<num_species; j++)
      active_interaction[i][j] = 0;

  for (int i=0; i<num_species; i++)
    n_atom += atoms_per_species[i];
  
  /*Set to 0 Verlet neighbour lists of all atoms*/
  for(int i=0; i<n_atom; i++) {
    for(int j=0; j<verlet_nbr_num[i]; j++)
      verlet_nbr_list[i][j] = 0;
    verlet_nbr_num[i] = 0;    
  }
  
  
  /*update Verlet neighbour lists of all atoms*/
  for(int i=0; i<n_atom; i++) 
    
    /*consider interactions between atom i and all other atoms */
    for(int j=0; j<i; j++) {
      
      
      /* keep track of different types of interaction */
      if ( !active_interaction[atom_species[i]][atom_species[j]] ) {
      active_interaction[atom_species[i]][atom_species[j]] = 1;
      active_interaction[atom_species[j]][atom_species[i]] = 1;
      }
      
      /* Verlet distance is minimum Verlet shell among
	 all interacting species */
      //if ( verlet_shell_width[atom_species[i]][atom_species[j]] 
      //      < *verlet_dist )
      //*verlet_dist = verlet_shell_width[i][j];
      //}      
      
      /* Calculate interatomic distances */
	distance_x[i][j] = x[i]-x[j];
	distance_y[i][j] = y[i]-y[j];
	distance_z[i][j] = z[i]-z[j];
	
	/*Apply periodic boundary conditions on simulation box*/
	distance_x[i][j] -= anint(distance_x[i][j]/x_box)*x_box;
      distance_y[i][j] -= anint(distance_y[i][j]/y_box)*y_box;
      if( fabs(distance_x[i][j]) > ((x_box/2)+1e-10) ) {
	printf("%s\n%s%d%s%d%s%12.10lf\n%s%12.10lf\n%s\n",
	       "A fatal error occurred in function update_nbr_list_verlet.",
	       "x distance between atom ",i, " and ", j, 
	       " has been found to be ", distance_x[i][j],
	       "Half boxx is ", x_box/2.0,
	       "It is not possible to continue, sorry!");
	exit(1);
      }
      if( fabs(distance_y[i][j]) > ((y_box/2)+1e-10) ) {
	printf("%s\n%s%d%s%d%s%12.10lf\n%s%12.10lf\n%s\n",
	       "A fatal error occurred in function update_nbr_list_verlet.",
	       "x distance between atom ",i, " and ", j, 
	       " has been found to be ", distance_y[i][j],
	       "Half boxx is ", y_box/2.0,
	       "It is not possible to continue, sorry!");
	exit(1);
      }
 
      
      /*use symmetry to construct distance matrix*/
      distance_x[j][i] = -distance_x[i][j];
      distance_y[j][i] = -distance_y[i][j];
      distance_z[j][i] = -distance_z[i][j];     
      
      /*calculate squared distance between atom i and j*/
      double dist_ij = distance_x[i][j]*distance_x[i][j] + 
	distance_y[i][j]*distance_y[i][j] + 
	distance_z[i][j]*distance_z[i][j]; 
      
      /*fill up Verlet neighbour lists*/ 
      if ( dist_ij <= 
	   verlet_length[atom_species[i]][atom_species[j]]*
	   verlet_length[atom_species[i]][atom_species[j]] ) {
	verlet_nbr_num[i]++;
	verlet_nbr_list[i][verlet_nbr_num[i]-1] = j;
	verlet_nbr_num[j]++;
	verlet_nbr_list[j][verlet_nbr_num[j]-1] = i;
      }
      
    }
  
  
  
  /*initialise shell thickness*/
  *verlet_dist = 1.0e20;
  /* Verlet distance is minimum Verlet shell among
     all interacting species */
  for (int i=0; i<num_species; i++)
    for (int j=0; j<i; j++)
      if ( active_interaction[i][j]  &&
	   verlet_shell_width[i][j] < *verlet_dist )
	  *verlet_dist = verlet_shell_width[i][j];
  /* in case of no interactions, Verlet distance is set
     equal to minimum Verlet shell among all species */
  if ( *verlet_dist > 1.0e10 ) {
    for (int i=0; i<num_species; i++)
      for (int j=0; j<num_species; j++)
	if ( verlet_shell_width[i][j] < *verlet_dist )
	  *verlet_dist = verlet_shell_width[i][j];
  }

  /*print Verlet neighbour list of all atoms 
   for debugging purposes */
  //for (int i=0; i<n_atom; i++) {
  //printf("Verlet nbrs of atom %d is %d\n", i, verlet_nbr_num[i]);
  //printf("Verlet nbr list of atom %d:\n",i);
  //for (int j=0; j<nbr_num_verlet[i]; j++)
  //  printf("%d ",verlet_nbr_list[i][j]);
  //printf("\n");
  //}
  
  
  return(0);  
  
}
