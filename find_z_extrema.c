#include "header.h"


/* This function finds highest and lowest 
   point of the structure comprised of all
   atoms in the solid phase. A call to 
   the function "label_substrate_atoms"
   is made to decide which atoms are in the 
   solid and which are in the gas phase */
int find_z_extrema(int atom_num,
		   //int fixed_atom_num,
		   int *atom_species,
		   double bond_dist[][n_max_type],
		   double boxx_dim, 
		   double boxy_dim,
		   //double boxz_dim,
		   double *p_z_bottom,
		   double *p_z_top,
		   double *x_pos, 
		   double *y_pos,
		   double *z_pos)
{
  int *atom_in_substrate = 
    malloc(atom_num*sizeof(int));
  assert( atom_in_substrate != NULL );
  for (int i=0; i<atom_num; i++)
    atom_in_substrate[i] = -1;

  double min_z = z_pos[0];
  double max_z = z_pos[0];

  /* find which atoms are in solid
     and in gas phase */
  label_substrate_atoms(atom_num, 
			atom_species,
			atom_in_substrate,
			bond_dist,
			boxx_dim, boxy_dim,
			x_pos, y_pos, 
			z_pos);
  
  
  
  /* find highest and lowest point
     amongst all atoms in the solid */
  for (int i=0; i<atom_num; i++)
    if ( atom_in_substrate[i] ) {
      if ( z_pos[i] < min_z )
	min_z = z_pos[i];
      if ( z_pos[i] > max_z )
	max_z = z_pos[i];
    }
  
  
  *p_z_bottom = min_z;
  *p_z_top = max_z;
  
  
  free(atom_in_substrate);
  
  return 0;
}
