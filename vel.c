#include "header.h"



/* Advances velocities by half a 
   Velocity-Verlet step. 
   In the Velocity-Verlet scheme, 
   velocity update is carried out 
   by the following equation:
   v(t) = v(t-dt) + dt*(a(t-dt)+a(dt))/2;
   where a(t) is the particle acceleration 
   at time t.
   This function reassigns velocities in
   the following manner:
   v_i = v_i + dt*f_i/(2*m_i);
   where v_i, f_i, and m_i are,
   respectively, the velocity, the force,
   and the mass of particle i in the 
   current configuration.
   Therefore, this function is to be 
   referenced twice per each MD iterations:
   once before and once after the forces 
   are updated. */

int vel_partial_update(int part_num,
		       int *fix_atom_label,
		       int num_fixed_atoms,
		       int *atom_species, 
		       double *atom_mass, 
		       double time_interval, 
		       double *force_x, 
		       double *force_y, 
		       double *force_z,
		       double *p_kin_energy, 
		       double *p_temperature,
		       double *vel_x, 
		       double *vel_y, 
		       double *vel_z)
{
  
  double sum_vel_squared = 0.0;
  
  double velocity_verlet_conv_fact = joule_per_ev/
    (kg_per_amu*metre_per_angstrom*metre_per_angstrom);

  double velocity_verlet_fact = 
    0.5*time_interval*velocity_verlet_conv_fact;

  /* partially update velocities. */
  
  for (int i=0; i<part_num; i++) {
    
    assert(atom_species[i]>=0 && atom_species[i]<n_species);
    
    if ( !fix_atom_label[i] ) {
      double mult_fact = 
	velocity_verlet_fact/atom_mass[atom_species[i]];
      /* The force calculated by function force is 
	 expressed in eV/A.
	 The force is converted into Newton=Joule/m 
	 by multplying it by factor of joule_per_eV/m_per_angstrom.
	 Multiplication of the atomic mass by factor of kg_per_amu 
	 gives mass in Kg.
	 The incremental velocity is then calculated and 
	 expressed in m/s. 
	 After dividing by factor of m_per_angstrom, the 
	 velocity is finally expressed in A/m. */
      vel_x[i] += mult_fact*force_x[i];
      vel_y[i] += mult_fact*force_y[i];
      vel_z[i] += mult_fact*force_z[i];    
    } /* end of if ( !fix_atom_label[i] ) */
    
    
    /* Calculate contribution of atom i to kinetic energy */
    sum_vel_squared += atom_mass[atom_species[i]] *
      (vel_x[i]*vel_x[i] + vel_y[i]*vel_y[i] + vel_z[i]*vel_z[i]);
    
    
  } // end of  for (int i=0; i<atom_num; i++)
  
  
  
  /* Calculate kinetic energy (in eV) */
  
  /*Kin_energy = 1/2 sum(i=1,2,...,N) mass[i]*v[i]^2 */
  *p_kin_energy = 0.5 * sum_vel_squared /
    velocity_verlet_conv_fact;
  
  assert( part_num-num_fixed_atoms >= 0 );
  /* Calculate current system temperature in K */
  /* (3/2)*N*kb*T = kin_energy => T = (2/3)*kin_energy/(N*kb)  */
  if ( part_num-num_fixed_atoms )
    *p_temperature =  2.0 * *p_kin_energy /
      (3.0*(part_num-num_fixed_atoms)*kb);
  else
    *p_temperature = 0.0;


  

  return 0;
  
}






/****************************************************************/






/* Advances velocities */
int vel_old(int atom_num,
	int *fix_atom_label,
	int num_fixed_atoms,
	int *atom_species, 
	double *atom_mass, double time_interval, 
	double *prev_fx, double *prev_fy, double *prev_fz,
	double *force_x, double *force_y, double *force_z,
	double *p_kin_energy, 
	double *p_temperature,
	double *vel_x, double *vel_y, double *vel_z)
{
  
  double sum_vel_squared = 0.0;

  double velocity_verlet_conv_fact = joule_per_ev/
    (kg_per_amu*metre_per_angstrom*metre_per_angstrom);
  
 

  /* Update velocities. */
  
  for (int i=0; i<atom_num; i++) {
    
    assert(atom_species[i]>=0 && atom_species[i]<n_species);
    
    if ( !fix_atom_label[i] ) {
      /* The force calculated by function force is 
	 expressed in eV/A.
	 The force is converted into Newton=Joule/m 
	 by multplying it by factor of joule_per_eV/m_per_angstrom.
	 Multiplication of the atomic mass by factor of kg_per_amu 
	 gives mass in Kg.
	 The incremental velocity is then calculated and 
	 expressed in m/s. 
	 After dividing by factor of m_per_angstrom, the 
	 velocity is finally expressed in A/m. */
      vel_x[i] += velocity_verlet_conv_fact * 0.5 * 
	time_interval * (prev_fx[i]+force_x[i]) /
	atom_mass[atom_species[i]];
      vel_y[i] += velocity_verlet_conv_fact * 0.5 * 
	time_interval * (prev_fy[i]+force_y[i]) / 
	atom_mass[atom_species[i]];
      vel_z[i] += velocity_verlet_conv_fact * 0.5 *
	time_interval * (prev_fz[i]+force_z[i]) / 
	atom_mass[atom_species[i]];    
    } /* end of if ( !fix_atom_label[i] ) */
    
    
    /* Calculate contribution of atom i to kinetic energy */
    sum_vel_squared += atom_mass[atom_species[i]] *
      (vel_x[i]*vel_x[i] + vel_y[i]*vel_y[i] + vel_z[i]*vel_z[i]);
    
    
  } // end of  for (int i=0; i<atom_num; i++)
  

  /* Calculate kinetic energy (in eV) */
  
  /*Kin_energy = 1/2 sum(i=1,2,...,N) mass[i]*v[i]^2 */
  *p_kin_energy = 0.5 * sum_vel_squared /
    velocity_verlet_conv_fact;
  
  assert( atom_num-num_fixed_atoms >= 0 );
  /* Calculate current system temperature in K */
  /* (3/2)*N*kb*T = kin_energy => T = (2/3)*kin_energy/(N*kb)  */
  if ( atom_num-num_fixed_atoms > 0 )
    *p_temperature =  2.0 * *p_kin_energy /
      (3.0*(atom_num-num_fixed_atoms)*kb);
  else
    *p_temperature = 0.0;


  return 0;

}
