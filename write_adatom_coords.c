#include "header.h"

int write_adatom_coords(char *position_output_file,
			char *velocity_output_file,
			int block,
			int step, double time, 
			double x, double y, double z,
			double vel_x, double vel_y, 
			double vel_z)
{
  FILE *p_position_file, *p_velocity_file;
  //fopen(f_output_file,);
  
  if ( (p_position_file=fopen(position_output_file,"a")) == NULL )
    {
      printf("%s\n%s%s\n%s\n",
	     "A fatal error occurred in function write_adatom_coords",
	     "while trying to open file ", position_output_file,
	     "Cannot continue, sorry!");
      exit(1);
    }
  
  if ( (p_velocity_file=fopen(velocity_output_file,"a")) == NULL )
    {
      printf("%s\n%s%s\n%s\n",
	     "A fatal error occurred in function write_adatom_coords",
	     "while trying to open file ", velocity_output_file,
	     "Cannot continue, sorry!");
      exit(1);
    }
  

  
  fprintf(p_position_file,"%5d %10d %12.3e %12.3lf %12.3lf %12.3lf\n",
	  block,  step, time, x, y, z);

  fprintf(p_velocity_file,"%5d %10d %12.3e %20.3lf %20.3lf %20.3lf\n",
	  block,  step, time, vel_x, vel_y, vel_z);
  
  fclose(p_position_file);
  fclose(p_velocity_file);

  return(0);

}
