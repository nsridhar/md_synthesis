#include "header.h"

/* allocates memory for arrays storing
   particle positions, velocities, 
   and forces */
int initialize_arrays(int init_part_num,
		      int deposit_clust_at_start,
		      int *p_max_final_part_num,
		      unsigned long int step_tot_num,
		      unsigned long int step_per_new_cluster,
		      int deposit_clust_max_size,
		      double **p_x_pos,
		      double **p_y_pos,
		      double **p_z_pos,
		      double **p_vx,
		      double **p_vy,
		      double **p_vz,
		      double **p_fx,
		      double **p_fy,
		      double **p_fz,
		      int **p_part_type,
		      int **p_fix_part_label)
{
  /* calculate maximum number of 
     deposited particles during 
     the whole simulation */
  int deposit_part_max_num =
    step_tot_num/step_per_new_cluster*deposit_clust_max_size;
  if ( deposit_clust_at_start )
    deposit_part_max_num += 
      deposit_clust_max_size;

  /* calculate maximum number of 
     particles at the end of the simulantion */
  int max_final_part_num = 
    init_part_num + deposit_part_max_num;
  

  /* allocate memory for particle positions,
     velocities, and forces */
  *p_x_pos = calloc(max_final_part_num, sizeof(double));
  assert( *p_x_pos != NULL);
  *p_y_pos = calloc(max_final_part_num, sizeof(double));
  assert( *p_y_pos != NULL );
  *p_z_pos = calloc(max_final_part_num, sizeof(double));
  assert( *p_z_pos != NULL );
  *p_vx = calloc(max_final_part_num, sizeof(double));
  assert( *p_vx != NULL );
  *p_vy = calloc(max_final_part_num, sizeof(double));
  assert( *p_vy != NULL );
  *p_vz = calloc(max_final_part_num, sizeof(double));
  assert( *p_vz != NULL );
  *p_fx = calloc(max_final_part_num, sizeof(double));
  assert( *p_fx != NULL );
  *p_fy = calloc(max_final_part_num, sizeof(double));
  assert( *p_fy != NULL );
  *p_fz = calloc(max_final_part_num, sizeof(double));
  assert( *p_fz != NULL );
  *p_part_type = calloc(max_final_part_num, sizeof(int));
  assert( *p_part_type != NULL );
  *p_fix_part_label = calloc(max_final_part_num, sizeof(int));
  assert( *p_fix_part_label != NULL );
  
  *p_max_final_part_num = max_final_part_num;

  return 0;
}






/*********************************************************/





int delete_arrays(double *x_pos,
		   double *y_pos,
		   double *z_pos,
		   double *vx,
		   double *vy,
		   double *vz,
		   double *fx,
		   double *fy,
		   double *fz,
		   int *part_type,
		   int *fix_part_label)
{
  
  free(x_pos);
  free(y_pos);
  free(z_pos);
  free(vx);
  free(vy);
  free(vz);
  free(fx);
  free(fy);
  free(fz);
  free(part_type);
  free(fix_part_label);

  return 0;
  
}  
