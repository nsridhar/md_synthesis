#include "header.h"

int label_fixed_atom(int atom_number,
		     double upper_z_coord,
		     int *fix_atom_label,
		     //double *x_pos,
		     //double *y_pos,
		     double *z_pos)
{
  int num_fixed_atom = 0;

  /* assign all frozen atoms with 
   a label of 1, and all the remaining
   atoms with a label of 0 */
  for (int i=0; i<atom_number; i++) 
    if ( z_pos[i] < upper_z_coord ) {
      fix_atom_label[i] = 1;
      num_fixed_atom++;
    }
    else
      fix_atom_label[i] = 0;

  return num_fixed_atom;
  
}
