# README #

MD_synthesis: Model deposition and tempering of clusters/atoms/ions/etc. on a substrate. 
Deposition is initiated on  a substrate.
   Difference with respect to 
   version 3.3.1 is that an error
   has been corrected in the way how
   to determine which particles
   in the solid are thermostatted 
   and which ones are fixed.
   Difference with respect to
   version 3.3.0 is that here
   local minimization is possible
   and is achieved by cancelling
   velocity components which 
   are antiparallel to the force.
   Difference with respect to 
   version 3.2 is that, besides
   interactions in MgF2, also
   interactions between a noble 
   gas and MgF2 can be calculated.
   The difference with respect 
   to version 0.x and 1.x is that
   electrostatic interactions between
   charged particles are calculated by means
   of Wolf's method, modified by Fennell and 
   Gezelter (J. Chem. Phys. Vol. 124, page 234104 (2006)). 
   Furthermore, the way how to calculate the 
   coefficients of the 5-th order join polynomial
   that smoothly brings the potential to 0 outside
   the cutoff has been changed with respect to 
   versions 1.x.
   Difference with respect to versions 2.x
   is that here a deposition routine is 
   implemented. (MgF2)n clusters can be 
   deposited at user's defined rate.
   Depositing clusters are introduced one by one
   with random initial x- and y-coordinates
   of their centre of mass
   and with a constant velocity normal to substrate.
   The only possible output is system configuration,
   particle velocities, and system potential energy.
   A simple thermostat based on velocity scaling
   The thermostat is applied to the bottom of slab. 
   Unless otherwise stated, lengths are 
   assumed to be expressed in Angstroms,
   masses in amu, times in seconds, and
   energies in electronvolts. */

See "http://pubs.acs.org/doi/abs/10.1021/ic501499k" where the code was used. 

### How do I get set up? ###

* Makefile provided. Make in terminal. 
* See folder run_test for example. 
