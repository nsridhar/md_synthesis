/* velocity input file is assumed to be 
   of the following form; 
   
   4772       //total particle number
   
   Mg     1228891653988.18      872094849551.68        6442595280.79
   F      566307319751.55     -469576532464.26      301165089741.19
   Mg    -1242405476077.51     -348925497627.52     1975749576744.54
   F      995551299608.08        9699362920.37      111544312418.52
   Al     -883378876385.57    -1146508865024.21      -41854458859.24
   Mg    -1743595083123.02     1273900969027.46     2652528054661.43
   .........
   
*/


#include "header.h"



int read_initial_velocities(char *vel_file_name, int n_atom,
			    double *x_vel, double *y_vel, 
			    double *z_vel)
{

  FILE *p_vel_file;

  int line_max_length = 200;
  

  char *line = malloc(line_max_length*sizeof(char));
  char** token = 
    malloc(line_max_length*sizeof(char*)); 
  assert( token != NULL );

  
  if ( (p_vel_file=fopen(vel_file_name, "r")) == NULL )  {
    printf("\nProblems encountered in function read_initial_velocities.\n");
    printf("could not open file %s\n", vel_file_name);
    printf("Cannot continue, sorry!\n\n");
    exit(1);
  }
 
  
  /* skip first and second line */
  for (int i=0; i<2; i++)
    fgets(line, 
	  line_max_length, 
	  p_vel_file);
  
  /* read input file line by line
     and assign particles
     with initial velocities   */
  for (int i=0; i<n_atom; i++) {

    fgets(line, 
	  line_max_length, 
	  p_vel_file);

    break_line(line, token);
    
    x_vel[i] = atof(token[1]);
    y_vel[i] = atof(token[2]);
    z_vel[i] = atof(token[3]);
  }

  free(line);
  free(token);
 
  return 0;
 
  
}
