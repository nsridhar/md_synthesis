#include "header.h"

/* Provides a new cluster with initial positions
   and velocities.
   Function returns 1 if total number of atoms
   reached maximum allowed atom number, or 
   if z coordinate of new atom cluster exceeds upper
   bound of simulation unit cell. Returns
   0 otherwise. */
int introduce_new_atom(int init_index,
		       int dep_clust_num,
		       cluster *dep_clust_array,
		       //int *p_tot_atom_number,
		       int particle_num,
		       int *p_deposit_part_num,
		       int num_fixed_atom,
		       double *species_masses,
		       //char atom_element_name[3],
		       char element_symbol[][3],
		       int *atom_species, 
		       int *atom_per_species,
		       double *vdw_radius,
		       double bond_distance[][n_max_type],
		       double init_distance_from_substrate,
		       double minimum_dist_from_substrate,
		       double *substrate_surface_z_position,
		       double clust_initial_temp,
		       double boxx, double boxy, 
		       double boxz,
		       double *x_pos, double *y_pos,
		       double *z_pos,
		       double *x_vel, double *y_vel, 
		       double *z_vel)
{
  
  /* decide which cluster is to be deposited */
  double *prob = calloc(dep_clust_num, 
			sizeof(double));
  for (int i=0; i<dep_clust_num; i++)
    prob[i] = dep_clust_array[i]->prob;
  /* randomly pick one cluster from
     the array dep_clust_array */
  cluster dep_clust = 
    create_copy_cluster
    (dep_clust_array[random_choice(dep_clust_num,prob)]);
  free(prob);

  int dep_part_num = dep_clust->unit_num;


  /* make sure index does not 
     correspond to any other 
     already existing atoms */
  for ( int i=0; i<dep_part_num; i++)
    if ( atom_species[init_index+i] >= 0 ) {
      printf("%s\n%s%d\n%s\n%s%d%s%d\n%s\n%s\n",
	     "Fatal error in function introduce_new_atom.",
	     "Cannot introduce atom with index ", init_index+i,
	     "as this position seems to be already occupied.",
	     "Atom type of array position ", init_index+i, 
	     " is ", atom_species[init_index+i],
	     "before the new atom has been introduced",
	     "Cannot continue, sorry!");
      exit(1);
    }
  
  //int atom_number = *p_tot_atom_number;  
   
  
  
  /* assign each atom in the cluster 
     with a chemical species */
  for (int i=0; i<dep_part_num; i++) {
    int species_cnt = 0;
    char atom_element_name[2];
    strncpy(atom_element_name, 
	    dep_clust->
	    species_symbol[dep_clust->unit_type[i]],
	    2);
    while ( species_cnt >= 0  &&
	    species_cnt < n_max_type ) 
      if ( strncmp(atom_element_name,
		   element_symbol[species_cnt],2) 
	   == 0 ) {
	atom_species[init_index+i] = species_cnt;
	//atom_per_species[species_cnt]++;
	species_cnt = -1;
      }
      else
	species_cnt++;
    
    /* make sure atom has been associated 
       with one of known species entering 
       the system */
    if ( species_cnt > 0   ||
	 atom_species < 0 ) {
      printf("%s\n%s%d%s\n%s\n%s%d\n%s\n",
	     "Fatal error in function introduce_new_atom!",
	     "Atom ", init_index+i, " could not be associated",
	     "with any known chemical species.",
	     "At the end of the cycle, the counter is ", species_cnt,
	     "It is not possible to continue, sorry!");
      exit(1);
    }
  }
  
  assign_atom_velocities(clust_initial_temp,
			 init_index,
			 dep_part_num,
			 atom_species,
			 species_masses,
			 x_vel, y_vel, z_vel);
  
  
  
  
  /* Assign new atom with coordinates.
     x and y coordinates are randomly chosen
     within the borders of the simulation unit box.
     z coordinate is such that distance between 
     substrate surface and atom is equal to setpoint */
  
  /* find position of cluster centre of mass */
  //double x_cm = 0.0, y_cm = 0.0, z_cm = 0.0, 
  double *cm_coord = calloc(3, sizeof(double));
  double *cm_coord_inv = calloc(3, sizeof(double));
  double mass_cm = 0.0;
  for (int i=0; i<dep_part_num; i++) {
    double this_ion_mass = 
      species_masses[atom_species[init_index+i]];
    for (int j=0; j<3; j++)
      cm_coord[j] += 
	this_ion_mass * dep_clust->unit_coord[i][j];
    mass_cm += this_ion_mass;
  }
  
  for (int j=0; j<3; j++) {
    cm_coord[j] /= mass_cm;
    cm_coord_inv[j] = -cm_coord[j];
  }
  
 
  
  /* translate cluster so that centre of mass 
     lies in origin of coordinates */
  translate_cluster(dep_clust,
		    cm_coord_inv);
  
  free(cm_coord_inv);

  /* rotate cluster about random axis 
     and by random angle */
  double phi, theta, psi;
  /* randomly choose phi and psi
     in the range (0,360], and
     theta in range (0,180].
     phi, psi, and theta are 
     Euler angles */
  phi = ran2();
  phi *= 360.0;
  theta = ran2();
  theta *= 180.0;
  psi = ran2();
  psi *= 360.0;
  rotate_cluster(dep_clust,
		 phi, theta, psi);
  
  
  /* rigidly translate cluster centre of mass
   xy-position is randomly chosen 
   z-coordinate is predetermined distance
   from solid phase */

  int assign_new_coordinates = 1;
  int clust_above_cell_top = 0;
  
  double z_coordinate =
    *substrate_surface_z_position +
    init_distance_from_substrate;
  
  while ( assign_new_coordinates ) { 
    
    clust_above_cell_top =  
      assign_cm_coordinates(z_coordinate, 
			    boxx, boxy, boxz, 
			    cm_coord,
			    cm_coord+1,
			    cm_coord+2);
    
    
    /* translate cluster by cm_coord,
       new centre-of-mass coordinates */
    translate_cluster(dep_clust, cm_coord);
    /* apply periodic boundary conditions */
    for (int i=0; i<dep_part_num; i++) {
      dep_clust->unit_coord[i][0] -= 
	anint(dep_clust->unit_coord[i][0]/boxx)*boxx;
      dep_clust->unit_coord[i][1] -= 
	anint(dep_clust->unit_coord[i][1]/boxy)*boxy;
    }
    
    
    /* return 1 if cluster z coordinate exceeds
       upper boundary of simulation unit cell */
    if ( clust_above_cell_top ) {
      printf("\n%s\n%s\n%s%.3lf%s\n%s%.3lf%s\n%s\n%s\n\n",
	     "Warning in function introduce_new_atom!",
	     "while creating new cluster.",
	     "z coordinate of incoming cluster is ",
	     z_coordinate, ".",
	     "Height of simulation unit cell is ",
	     boxz, ".",
	     "Function assign_cm_coordinates has returned 1.",
	     "This function is returning 1");
      free(cm_coord);
      return 1;
    }
    
    /* copy cluster coordinates in arrays 
       of system coordinates */
    for (int i=0; i<dep_part_num; i++) {
      x_pos[init_index+i] = dep_clust->unit_coord[i][0];
      y_pos[init_index+i] = dep_clust->unit_coord[i][1];
      z_pos[init_index+i] = dep_clust->unit_coord[i][2];
    }
    
    
    /* make sure new atom has been introduced
       above topmost surface of growing
       compound and atom is not too close
       to any of pre-existing atoms */
    assign_new_coordinates =
      check_new_clust_position(init_index,
			       dep_part_num,
			       particle_num,
			       num_fixed_atom,
			       substrate_surface_z_position,
			       minimum_dist_from_substrate,
			       atom_species,
			       vdw_radius,
			       bond_distance,
			       boxx, boxy, boxz,
			       x_pos, y_pos,
			       z_pos);
    
    
    
   /* move cluster upwards if function
       check_new_atom_position returns 1 */
    if ( assign_new_coordinates )
      z_coordinate += init_distance_from_substrate/2.0;
    
  } /* end of  while ( assign_new_coordinates ) */
  
  
  
  /* increment system atom counter
     and counter of atoms of the same
     species as new atoms */
  particle_num +=  dep_part_num;
  *p_deposit_part_num = dep_part_num;
  //*p_tot_atom_number = atom_number;
  for (int i=0; i<dep_part_num; i++)
    atom_per_species[atom_species[init_index+i]]++;
  
  free (cm_coord);
  delete_cluster(&dep_clust);
  
  /* if number of atoms equals maximum
     allowed muber of atoms, then return 1;
     otherwise return 0 */
  if ( particle_num >= n_max_atom ) {
    printf("\n%s\n%s%d%s\n%s%d%s\n%s%d%s\n%s\n",
	   "Warning in function introduce_new_atom",
	   "while creating new cluster from index ", init_index, ".",
	   "Number of atoms is currently ", particle_num, ",",
	   "whereas maximum allowed number of atoms is ",
	   n_max_atom, ".",
	   "This function is returning 1.\n");
    return 1;
  }
  else
     return 0; 

}
