//#include "header.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>


#define n_max_type 5


#define assert(expr)  if (!(expr)) {	\
    printf("\n%s%s\n%s%s\n%s%d\n\n",	\
	   "Assertion failed: ",#expr,  \
"in file ",__FILE__,			\
  "in line ",__LINE__);		        \
exit(1);		 	        \
}


#define anint(expr) (int)(expr < 0 ? expr-0.5 : expr+0.5)




/* Calculates forces on each atom and potential energy. */
int force(int atom_num,
	  int cell_num,
	  int stripe_num,
	  int *fix_atom_label, 
	  int potential_type[][n_max_type],
	  double box_x, double box_y, 
	  //double box_z,
	  int *atom_species,
	  double *p_pot_en,
	  double rho_bm[][n_max_type], double *q_bm, 
	  double b_bm[][n_max_type], 
	  double c_bm[][n_max_type],
	  double d_bm[][n_max_type], 
	  double alpha_bm[][n_max_type],
	  double epsilon_lj[][n_max_type], 
	  double sigma_lj[][n_max_type],
	  double sigma_mixed[][n_max_type], 
	  double epsilon_mixed[][n_max_type],
	  double join_area_limits_mixed[][n_max_type][2],
	  double join_coeff_mixed[][n_max_type][6],
	  double pot_range[][n_max_type], 
	  double cutoff_distance[][n_max_type], 
	  double truncation_params[][n_max_type][6],
	  int *mask_pointer,
	  int *cell_pointer,
	  int *cell_list,
	  double *force_x, double *force_y, double *force_z,
	  //double *prev_fx, double *prev_fy, double *prev_fz,
	  double *x_coord, double *y_coord, double *z_coord)
{

  
  
  int calculate_pair_potential
    (int atom_i, int atom_j,
     int type_i, int type_j,
     double dist_ij_sq,
     double dx_ij, 
     double dy_ij, 
     double dz_ij,
     //double xi, double yi, double zi,
     //double xj, double yj, double zj,
     int *fix_atom_label,
     int potential_type[][n_max_type],
     double rho_bm[][n_max_type], double *q_bm, 
     double b_bm[][n_max_type], 
     double c_bm[][n_max_type],
     double d_bm[][n_max_type], 
     double alpha_bm[][n_max_type],
     double epsilon_lj[][n_max_type], 
     double sigma_lj[][n_max_type],
     double sigma_mixed[][n_max_type], 
     double epsilon_mixed[][n_max_type],
     double join_area_limits_mixed[][n_max_type][2],
     double join_coeff_mixed[][n_max_type][6],
     double pot_range[][n_max_type], 
     double cutoff_distance[][n_max_type], 
     double truncation_params[][n_max_type][6],
     double *p_pot_ener_i,
     double *p_fx_i, 
     double *p_fy_i, 
     double *p_fz_i,
     double *fx_all_atoms,
     double *fy_all_atoms,
     double *fz_all_atoms);
  
  
  /* initialise potential
     energy to zero */
  *p_pot_en = 0.0;
  
  /*Set forces on each atom equal to 0*/
  for(int i=0; i<atom_num; i++) {
    force_x[i] = 0.0;
    force_y[i] = 0.0;
    force_z[i] = 0.0;
  }
  
  
  
  /* CALCULATE ENERGY AND FORCES ON PARTICLES */
  
  for( int m=0; m<cell_num; m++ ) /* loop over cells */  
    
    
    for (int i=cell_pointer[m];  /* loop over particles in cell m */
	 i<cell_pointer[m+1];
	 i++) {
      
      int part1 = cell_list[i];
      int type1 = atom_species[part1];
      assert(type1>-1 && type1<n_max_type);
      
      double ener_1 = 0.0;
      double f1_x=0.0, f1_y=0.0, f1_z=0.0;
      
      for ( int j=i+1; /* loop over particles other than part1 in cell m */
	    j<cell_pointer[m+1]; 
	    j++) { 
	
	int part2 = cell_list[j];
	assert(part2>-1 && part2<atom_num);
	int type2 = atom_species[part2];
	assert(type2>-1 && type2<n_max_type);
	
	double dx = x_coord[part1]-x_coord[part2];
	double dy = y_coord[part1]-y_coord[part2];
	double dz = z_coord[part1]-z_coord[part2];
	
	/* apply periodic boundary conditions */
	dx -= anint(dx/box_x)*box_x;
	dy -= anint(dy/box_y)*box_y;
	if( fabs(dx) > ((box_x/2)+1e-10) ) {
	  printf("%s\n%s%d%s%d%s%12.10lf\n%s%12.10lf\n%s\n",
		 "A fatal error occurred in function force.",
		 "x distance between atom ", part1, " and ", part2, 
		 " has been found to be ", dx,
		 "Half boxx is ", box_x/2.0,
		 "It is not possible to continue, sorry!");
	  exit(1);
	}
	if( fabs(dy) > ((box_y/2)+1e-10) ) {
	  printf("%s\n%s%d%s%d%s%12.10lf\n%s%12.10lf\n%s\n",
		 "A fatal error occurred in function force.",
		 "x distance between atom ", part1, " and ", part2, 
		 " has been found to be ", dy,
		 "Half boxx is ", box_y/2.0,
		 "It is not possible to continue, sorry!");
	  exit(1);
	}
	

	double dist_12_sq = 
	  dx*dx + dy*dy + dz*dz;
	
	if ( dist_12_sq < 
	     pot_range[type1][type2]*pot_range[type1][type2] ) {
	  
	  
	  calculate_pair_potential
	    (part1, part2,
	     type1, type2,
	     dist_12_sq,
	     dx, dy, dz,
	     fix_atom_label,
	     potential_type,
	     rho_bm, q_bm, 
	     b_bm, c_bm, d_bm, alpha_bm,
	     epsilon_lj, sigma_lj,
	     sigma_mixed, epsilon_mixed,
	     join_area_limits_mixed,
	     join_coeff_mixed,
	     pot_range, 
	     cutoff_distance, 
	     truncation_params,
	     &ener_1,
	     &f1_x, &f1_y, &f1_z,
	     force_x, force_y, force_z);
	  

	  
	} /* end of 
	     if ( dist_12_sq < 
	     pot_range[part1][part2]  */
	
      } /* end of 
	   for ( int j=i+1; 
	   j<cell_pointer[m+1]; 
	   j++) {  
	   loop over particles 
	   other than part1 in cell m  */
      
      
      for (int s=0; s<stripe_num; s++) { /* loop over mask stripes, 
					    i.e. loop over 
					    all cells 
					    other than m */
	
	int first_stripe_cell = m+mask_pointer[2*s];
	
	if ( first_stripe_cell < cell_num ) {
	  
	  int last_stripe_cell = 
	    m+mask_pointer[2*s+1] < cell_num ? 
	    m+mask_pointer[2*s+1] : cell_num-1;
	  

	  for (int j=cell_pointer[first_stripe_cell]; 
	       j < cell_pointer[last_stripe_cell+1];
	       j++) {
	    
	    int part2 = cell_list[j];
	    int type2 = atom_species[part2];
	    
	    
	    double dx = x_coord[part1]-x_coord[part2];
	    double dy = y_coord[part1]-y_coord[part2];
	    double dz = z_coord[part1]-z_coord[part2];
	    
	    /* apply periodic boundary conditions */
	    dx -= anint(dx/box_x)*box_x;
	    dy -= anint(dy/box_y)*box_y;
	    if( fabs(dx) > ((box_x/2)+1e-10) ) {
	      printf("%s\n%s%d%s%d%s%12.10lf\n%s%12.10lf\n%s\n",
		     "A fatal error occurred in function force.",
		     "x distance between atom ", part1, " and ", part2, 
		     " has been found to be ", dx,
		     "Half boxx is ", box_x/2.0,
		     "It is not possible to continue, sorry!");
	      exit(1);
	    }
	    if( fabs(dy) > ((box_y/2)+1e-10) ) {
	      printf("%s\n%s%d%s%d%s%12.10lf\n%s%12.10lf\n%s\n",
		     "A fatal error occurred in function force.",
		     "x distance between atom ", part1, " and ", part2, 
		     " has been found to be ", dy,
		     "Half boxx is ", box_y/2.0,
		     "It is not possible to continue, sorry!");
	      exit(1);
	    }
	    
	   
	    double dist_12_sq = 
	      dx*dx + dy*dy + dz*dz;
	    
	    if ( dist_12_sq < 
		 pot_range[type1][type2]*pot_range[type1][type2] ) {
	      

	      
	      calculate_pair_potential
		(part1, part2,
		 type1, type2,
		 dist_12_sq,
		 dx, dy, dz,
		 fix_atom_label,
		 potential_type,
		 rho_bm, q_bm, 
		 b_bm, c_bm, d_bm, alpha_bm,
		 epsilon_lj, sigma_lj,
		 sigma_mixed, epsilon_mixed,
		 join_area_limits_mixed,
		 join_coeff_mixed,
		 pot_range, 
		 cutoff_distance, 
		 truncation_params,
		 &ener_1,
		 &f1_x, &f1_y, &f1_z,
		 force_x, 
		 force_y, 
		 force_z);
	      
	      
	      

	      
	    } /* end of 
		 if ( dist_12_sq < pot_range[part1][part2] ) */
	    
	  } /* end of 
	       for (int j=cell_pointer[first_stripe_cell]; 
	       j < cell_pointer[last_stripe_cell+1]; j++) */
	  
	} /* end of if ( first_stripe_cell < cell_num ) */
	
      } /* end of  for (int s=0; s<stripe_num; s++) 
	   loop over all cells other than m*/
      
      
	/* Contribution due to 
	   all atoms other than part1 
	   is added to force 
	   on atom part1 */
      force_x[part1] -= f1_x;
      force_y[part1] -= f1_y;
      force_z[part1] -= f1_z;
      
      
      /* Add contribution given by 
	 atom part1 to potential energy */
      *p_pot_en += ener_1;
      
	//} /* end of if (!fix_atom_label[part1]) */
      
    } /* end of for(int i=cell_pointer[m]; 
	 i<cell_pointer[m+1]; i++) */
  
  

  return 0;
}




/************************************************************************/







/* This function calculates potential and forces 
   due to the interaction between two particles. 
   Particle i and j are the first and the second
   particle interacting with each other, respectively.
   The order is important, because the function 
   takes care of adding the force on j due to 
   the interaction with i to the total force on j.
   The function also adds the force contribution
   due to the interaction between i and j to the 
   total force on atom i which is passed to the function
   by means of the pointers p_fx_i, p_fy_i, and p_fz_i.
   The distance dx_ij, dy_ij, dz_ij are the distance
   of particle i with respect to j projected onto the
   three axes. In other words:
   dx_ij = x[i]-x[j]
   dy_ij = y[i]-y[j]
   dz_ij = z[i]-z[j];
   Since forces are to be calculated, the sign
   of these distances is important.
   Furthermore, the distance squared dist_ij_sq
   between i and j passed as a parameter onto the
   function must satisfy the following equality:
   dist_ij_sq = 
   dx_ij*dx_ij + dy_ij*dy_ij + dz_ij*dz_ij
   It is important to observe that, when periodic
   boundary conditions are applied, the interaction 
   must be considered between particle i and closest 
   image of particel j to particle i. */
int calculate_pair_potential(int atom_i, int atom_j,
			     int type_i, int type_j,
			     double dist_ij_sq,
			     double dx_ij, 
			     double dy_ij,
			     double dz_ij,
			     //double xi, double yi, double zi,
			     //double xj, double yj, double zj,
			     int *fix_atom_label,
			     int potential_type[][n_max_type],
			     double rho_bm[][n_max_type], double *q_bm, 
			     double b_bm[][n_max_type], 
			     double c_bm[][n_max_type],
			     double d_bm[][n_max_type], 
			     double alpha_bm[][n_max_type],
			     double epsilon_lj[][n_max_type], 
			     double sigma_lj[][n_max_type],
			     double sigma_mixed[][n_max_type], 
			     double epsilon_mixed[][n_max_type],
			     double join_area_limits_mixed[][n_max_type][2],
			     double join_coeff_mixed[][n_max_type][6],
			     double pot_range[][n_max_type], 
			     double cutoff_distance[][n_max_type], 
			     double truncation_params[][n_max_type][6],
			     double *p_pot_ener_i,
			     double *p_fx_i, 
			     double *p_fy_i, 
			     double *p_fz_i,
			     double *fx_all_atoms,
			     double *fy_all_atoms,
			     double *fz_all_atoms)
{
  
  int calculate_10_5_potential
    (double dist12_squared,
     double *p_pot12,
     double *p_dpot12_times_dist,
     double sigma12,
     double epsilon12,
     double cutoff_dist12,
     double *truncation_params);
  

  int calculate_born_mayer_potential
    (double dist12_squared,
     double *p_pot12,
     double *p_dpot12_times_dist,
     double rho_param,
     double q1, double q2,
     double b12, double c12,
     double d12, double alpha_coulomb,
     double cutoff_dist12,
     double pot_range_12,
     double *truncation_params); 

  int calculate_hybrid_potential
    (double dist12_squared,
     double *p_pot12,
     double *p_dpot12_times_dist,
     double sigma12,
     double epsilon12,
     double r1, double r2,
     double cutoff_dist12,
     double *join_coeff,
     double *truncation_param);
  
  int calculate_lj_potential
    (double dist12_squared,
     double *p_lj_pot12,
     double *p_dlj12_times_dist,
     double q1, double q2,
     double alpha_coulomb,
     double sigma12,
     double epsilon12,
     double cutoff_dist12,
     double pot_range_12,
     double *truncation_params);
  
  
  assert( dist_ij_sq > 1.0e-6 );
  

  
  double ener_ij = 0.0; /*contribution to potential 
			  energy due to interaction 
			  between atom i and atom j */
  double dv_over_dr_times_dist = 0.0; /* derivative of potential 
					 with respect to distance 
					 between atom i and j,
					 multiplied by distance 
					 between atom i and j */
  double fij_x = 0.0, 
    fij_y = 0.0, fij_z = 0.0; /* force arising as a result 
				 of interaction between atom 
				 i and atom j  */
  
  
  /* calculate strenght of interaction 
     between atom i and j, as well as 
     derivative of potential multiplied 
     by atom distance */
  
 
  
  if ( potential_type[type_i][type_j] == 0 )
    /* calculate Born-Mayer potential */
    calculate_born_mayer_potential
      (dist_ij_sq,
       &ener_ij,
       &dv_over_dr_times_dist,
       rho_bm[type_i][type_j],
       q_bm[type_i], q_bm[type_j],
       b_bm[type_i][type_j], 
       c_bm[type_i][type_j],
       d_bm[type_i][type_j], 
       alpha_bm[type_i][type_j],
       cutoff_distance[type_i][type_j],
       pot_range[type_i][type_j],
       truncation_params[type_i][type_j]);
  else if ( potential_type[type_i][type_j] == 1 )
    /* calculate Lennard-Jones potential */
    calculate_lj_potential(dist_ij_sq,
       			   &ener_ij,
			   &dv_over_dr_times_dist,
                           q_bm[type_i],q_bm[type_j],
                           alpha_bm[type_i][type_j],
                           sigma_lj[type_i][type_j],
			   epsilon_lj[type_i][type_j],
			   cutoff_distance[type_i][type_j],
                           pot_range[type_i][type_j],
                           truncation_params[type_i][type_j]);
  else if  ( potential_type[type_i][type_j] == 2 )
    /* calculate 10-5 potential */
    calculate_10_5_potential(dist_ij_sq,
			     &ener_ij,
			     &dv_over_dr_times_dist,
			     sigma_mixed[type_i][type_j],
			     epsilon_mixed[type_i][type_j],
			     cutoff_distance[type_i][type_j],
			     truncation_params[type_i][type_j]);
  else if ( potential_type[type_i][type_j] == 3 ) {
    /* calculate hybrid 10-5 and r^-5 potential */
    calculate_hybrid_potential
      (dist_ij_sq,
       &ener_ij,
       &dv_over_dr_times_dist,
       sigma_mixed[type_i][type_j],
       epsilon_mixed[type_i][type_j],
       join_area_limits_mixed[type_i][type_j][0],
       join_area_limits_mixed[type_i][type_j][1],
       cutoff_distance[type_i][type_j],
       join_coeff_mixed[type_i][type_j],
       truncation_params[type_i][type_j]);
  }
  else {
    printf
      ("\n%s\n%s%d%s%d\n%s%d%s\n%s%d%s\n%s\n\n",
       "Fatal error in function calculate_pair_potential!.",
       "The interaction between particle ", atom_i,
       " and particle ", atom_j,
       "turned out to be of type ", 
       potential_type[type_i][type_j], ".",
       "Interaction type must be an integer between 0 and ",
       n_max_type, ".",
       "It is impossible to continue, sorry!");
    exit(1);
  }
  
  
  /* calculate opposite of force acting 
     on atom i due to atom j */
  double dv_over_dr = 
    dv_over_dr_times_dist/dist_ij_sq;
  fij_x = dx_ij*dv_over_dr;
  fij_y = dy_ij*dv_over_dr;
  fij_z = dz_ij*dv_over_dr;
  
  
  /* Add contribution 
     due to interaction
     between atom i
     and j to potential 
     energy of atom i */
  *p_pot_ener_i += ener_ij;
  
  
  /* add force on atom i 
     due to atom j to 
     total force on 
     atom j */
  if ( !fix_atom_label[atom_j] ) {
    fx_all_atoms[atom_j] += fij_x;
    fy_all_atoms[atom_j] += fij_y;
    fz_all_atoms[atom_j] += fij_z;
  }
  
  /* Add contribution due 
     to atom j to opposite 
     of total force on 
     atom i */
  if ( !fix_atom_label[atom_i] ) {
    *p_fx_i += fij_x;
    *p_fy_i += fij_y;
    *p_fz_i += fij_z;  
  }


  
  return 0;
}
