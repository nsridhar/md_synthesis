#include "header.h"

/* Function calculating Born-Mayer potential in 
   the following forrm:
   V(r) =  V_coul(r) 
   + bij*exp(-r/rho) 
   + cij/r^12
   - dij/r^6
   The potential is also derived with respect 
   to interatomic distance r. The value 
   pointed to by argument p_dpot12_times_dist 
   is the derivative of potential with respect 
   to atom distance, multiplied by the same distance. 
   (Apologies for this unnecessary complication!)  
   V_coul(r) is calculated according to the 
   prescription of Fennell and Gezelter 
   (J. Chem. Phys. Vol. 124, page 234104 (2006)) 
   V_coul(r) = q1*q2*[ erfc(alpha*r)/r 
   - erfc(alpha*Rc)/Rc
   + ( erfc(alpha*Rc)/Rc^2 
   + 2*alpha/pi^0.5*exp(-alpha^2*Rc^2)/Rc )
   * (r-Rc) ] 
*/

int calculate_born_mayer_potential(double dist12_squared,
				   double *p_pot12,
				   double *p_dpot12_times_dist,
				   double rho_param,
				   double q1, double q2,
				   double b12, double c12,
				   double d12, double alpha_coulomb_12,
				   double cutoff_dist_12,
				   double pot_range_12,
				   double *truncation_params)
{
  
  extern int 
    calculate_coulomb_potential
    (double r,
     double r_cut,
     double q1, double q2,
     double alpha,
     double *p_coulomb_pot,
     double *p_dpot_times_r);
  

  /* contribution to potential 
     energy due to interaction 
     between atom 1 and 2 */
  double ener_12 = 0.0; 
  /* derivative of potential 
     with respect to distance 
     between each atom couple 
     multiplied by mutual atom 
     distance */
  double potderiv_times_dist = 0.0;
  
  /* calculate distance between 
     atom 1 and 2 */
  double dist_12 = sqrt(dist12_squared);
  
  

  

  if ( dist_12 <= 
       cutoff_dist_12 ) { /*interaction within cutoff distance*/
    
    
    /* calculate coulombic part of interaction 
       and its derivative multiplied by distance */
    double coulomb_pot, coulomb_pot_deriv_times_dist;
    calculate_coulomb_potential(dist_12,
				pot_range_12,
				q1, q2, 
				alpha_coulomb_12,
				&coulomb_pot,
				&coulomb_pot_deriv_times_dist);
    
    /* calculate remaining parts of interaction */
    double repulsive_exp_term = b12*exp(-dist_12/rho_param);
    double barrier = c12/pow(dist_12,12.0);
    double attractive_term = d12/pow(dist_12,6.0);
   
 


    ener_12 =
      coulomb_pot
      + repulsive_exp_term
      + barrier
      - attractive_term;
    

//    printf("BM\t %f\t %f\t %f\t %f \t %f\t %f \n", q1, q2, dist_12, rho, coulomb_pot, ener_12 );  


    /* Calculate derivative of  
       potential with respect 
       to r_12 and multiply 
       by r_12 */
    potderiv_times_dist = 
      coulomb_pot_deriv_times_dist
      - (dist_12/rho_param)*repulsive_exp_term
      - 12.0*barrier
      + 6.0*attractive_term;
    
  }
  
  else { /* Interaction beyond
	    cutoff distance. 
	    Use truncation parameters. */
    
    double coeff[6], 
      dist_pow[6]; 
    
    for(int n=0; n<6; n++)
      /* coeff[n] = an, 
	 with 
	 pot(r) = 
	 a0+a1*r+a2*r^2+a3*r^3+...+a5*r^5 */
      coeff[n] = truncation_params[n];
    
    dist_pow[0] = 1;
    for(int n=1; n<6; n++)
      /* dist_pow[n] = (r_12)^n */
      dist_pow[n] = dist_12*dist_pow[n-1]; 
    
    /* Calculate contribution 
       to potential energy
       due to interaction 
       between atom 1 and 2 */
    for(int n=0; n<6; n++)
      ener_12 += coeff[n]*dist_pow[n]; 	 
    
    
    /* Calculate derivative 
       of potential 
       with respect to r_ij 
       and multiply by r_ij */
    for (int n=1; n<6; n++) 
      potderiv_times_dist += 
	n*coeff[n]*dist_pow[n];
    
  }


  
  /* "export" calculated 
     quantities 
     to calling function */
  *p_pot12 = ener_12;
  *p_dpot12_times_dist = 
    potderiv_times_dist;
  
  
  return 0;
}





/*********************************************************/


int calculate_born_mayer_second_deriv
(double dist_12,
 //double *p_pot12,
 double *p_pot12_second_deriv,
 double rho_param,
 double q1, double q2,
 double b12, double c12,
 double d12, 
 double alpha_coulomb_12)
//double cutoff_dist_12,
//double pot_range_12)
//double *truncation_param)
{
  
  extern int 
    calculate_coulomb_second_deriv
    (double r,
     double q1, double q2,
     double alpha,
     double *p_coulomb_second_deriv);
  
  
  double pot_second_deriv, 
    coulomb_part_second_deriv;
  
  calculate_coulomb_second_deriv
    (dist_12,
     q1, q2,
     alpha_coulomb_12,
     &coulomb_part_second_deriv);
    

  pot_second_deriv = 
    156.0*c12/pow(dist_12,14)
    - 42.0*d12/pow(dist_12,8)
    + b12*exp(-dist_12/rho_param)/
    (rho_param*rho_param)
    + coulomb_part_second_deriv;

  *p_pot12_second_deriv = pot_second_deriv;
  
  
  return 0;
}
