#include "cluster_linked_list.h"
//#include "header.h"
#include <ctype.h>


/* reads in an input file 
   formatted like the following:

2 3 0.5 // species number,  unit number, probability 
Mg  5.0  7.0  0.0
F   2.0  7.0  0.0
F   8.0  7.0  0.0


//next cluster
2 4 0.25 
Mg  6.0 8.0 0.0
F   3.0 8.0 0.0
F   9.0 8.0 0.0
F   6.0 8.0 12.0

//next cluster
....

creates a cluster linked-list with the clusters
read in input file and returns number of clusters
read.   */





int read_dep_clust_input(cluster** p_clust_array,
			 char* input_file_name)
{

  int break_line(char* string, char** token);

  FILE *p_input_file;

  int line_max_length = 300;
  int file_max_length = 500;
  int line_cnt = 0;
  int infile_line_num = 0;

  int cluster_num = 0;

  char **line = calloc(file_max_length, sizeof(char*));
  for (int i=0; i<file_max_length; i++)
    line[i] =  calloc(line_max_length, sizeof(char));
  
  char* token[200];

  /* create an empty cluster list */
  cluster_linked_list the_list = create_list();
  
  
  if( (p_input_file=fopen(input_file_name,"r")) == NULL )
    {
      printf("Error in function read_dep_clustinput.\n");
      printf("A fatal error occured while opening file %s\n",
             input_file_name);
      printf("Cannot continue, sorry!\n");
      exit(1);
    }

  /* read input file line by line */
  while ( fgets(line[line_cnt],line_max_length, 
		p_input_file) != NULL 
	  &&
	  line_cnt++ < file_max_length ) 
    if ( strlen(line[line_cnt-1]) >= line_max_length ) 
      printf("\n%s\n%s%d%s\n%s%d%s\n\n", 
	     "Warning in function read_input!",
	     "Line ", line_cnt, "of input file might exceed",
	     "maximum allowed length of ", line_max_length, 
	     ".");
  
  
  fclose(p_input_file);

  infile_line_num = line_cnt;

  /* when reading is finished, make sure input 
     file has been entirely scanned  */
  if ( infile_line_num >= file_max_length )
    printf("\n%s\n%s\n%s%d%s\n\n",
	   "Warning in function read_input!",
	   "Input file might exceed maximum allowed",
	   "length of ", file_max_length, " files.");
  

  /******************************/

  
  /* process input file, search
     for clusters, and insert them 
     into cluster list */

  line_cnt = 0;

  while( line_cnt < infile_line_num ) {
    
    break_line(line[line_cnt], token);

    if ( isdigit(token[0][0]) 
	 &&
	 isdigit(token[1][0]) 
	 &&
	 isdigit(token[2][0]) ) {

      int species_num = atoi(token[0]);
      int ion_num = atoi(token[1]);
      double prob = atof(token[2]);

      char **ion_species = calloc(ion_num, sizeof(char*));
      for (int i=0; i<ion_num; i++)
	ion_species[i] = calloc(3, sizeof(char));
      double **ion_coord =  calloc(ion_num, sizeof(double*));
      for (int i=0; i<ion_num; i++)
	ion_coord[i] = calloc(3, sizeof(double));
      
      /*assign each ion with a chemical species 
	and coordinates */
      for(int i=0; i<ion_num; i++) {
	break_line(line[++line_cnt], token);
	strncpy(ion_species[i], token[0], 2);  
	ion_species[i][2] = '\0';
	ion_coord[i][0] = atof(token[1]);
	ion_coord[i][1] = atof(token[2]);
	ion_coord[i][2] = atof(token[3]); 
      }
      

      /* create a new cluster */
      cluster current_cluster = 
	create_cluster(species_num,
		       ion_num,
		       prob,
		       ion_species,
		       ion_coord);
      cluster_num++;
      
      /* insert cluster in list */
      insert_element_in_list(current_cluster,
			     the_list);
      

      /* free dynamically allocated memory */
      for (int i=0; i<ion_num; i++) {
	free(ion_species[i]);
	free(ion_coord[i]);
      }
      free(ion_coord);
      free(ion_species);
      /* destroy buffer cluster */
      delete_cluster(&current_cluster);
      
    } /* end of  
	 if ( isdigit(token[0]) && isdigit(token[1]) ) */
    
    line_cnt++;
    
  }/* end of   while( line_cnt < infile_line_num ) */
  
  
  /* make an array with all clusters 
     contained in the list */
  cluster* clust_array = 
    create_clust_array(cluster_num,
		       the_list);


  
  /* free dynamically allocated memory */
  for (int i=0; i<file_max_length; i++)
    free(line[i]);
  free(line);
  delete_list(&the_list);

  *p_clust_array = clust_array;
  return cluster_num;
}







/********************************************************/






/* /\*breaks a string into tokens, by using a blank space*\/ */
/* /\*as a separator between two contiguous tokens*\/ */
/* int break_line(char* string, char** token) */
/* { */

/*   int token_num = 0; */
/*   char *delimiter = " "; */

/*   token[0] = strtok(string, delimiter); */
/*   if ( token[0] != NULL ) */
/*     token_num++; */
  
/*   int j=0; */
/*   while ( token[j] != NULL ) { */
/*     token[++j] = strtok(NULL, delimiter); */
/*     token_num++; */
  
/*   } */

/*   return token_num; */

/* } */




