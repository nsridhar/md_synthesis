#include "header.h"


/* This function must be referenced
   after reading of input has been 
   accomplished. 
   The function decides which pair of 
   chemical species interacts with which
   potential. It also calculates
   the coefficients of the 5-th order
   polynomial used to smoothly bring to 0 
   the potentials outside the cutoff sphere. 
   Finally, the function also calculates 
   the coefficients of the join polynomial
   used in the hybrid potential
   to connect the 10-5 and r^(-5) areas. */
int set_potentials()
{
  
  
  /* prototype of function calculating
   the coefficients of the 5th-order 
   polynomial joining the two parts of 
   the hybrid 10-5 and r^(-5) potential */
  extern 
    int calculate_join_params_hybrid_pot(double sigma,
					 double epsilon,
					 double *p_r1, 
					 double *p_r2,
					 double *join_coeff);
  

  /* determine largest range among 
     all pair potentials used */
  max_pot_range = potential_range[0][0];
  for(int i=0; i<n_species; i++)
    for(int j=0; j<n_species; j++) 
      if ( potential_range[i][j] > max_pot_range )
	max_pot_range = potential_range[i][j];

  /* Assign each couple of atom species 
     with a particular type of interaction.
     The convention is as follows: 
     Id numb  Interaction type
     0        Born-Mayer (substrate) 
     1        Lennard-Jones plus Coulomb ( depositing noble gas )
     2        10-5 potential ( anion-noble gas )
     3        10-5 and r^-5 hybrid potential ( cation-nobles gas ) */
  
  /* initialise all elements of 
     matrix to -1 */
  for (int i=0; i<n_max_type; i++) 
    for (int j=i; j<n_max_type; j++) 
      interaction_type[i][j] = 
	interaction_type[j][i] = -1; 

  /* decide which pair of chemical species
     interacts with which potential */

/*****************************IMPORTANT****************************/
/* Hard coded for time being. Change every time system is changed*/
/**/
  interaction_type[0][1] = interaction_type[1][0] = 0;
  interaction_type[0][2] = interaction_type[2][0] = 1;
  interaction_type[0][3] = interaction_type[3][0] = 1;
  interaction_type[1][2] = interaction_type[2][1] = 1;
  interaction_type[1][3] = interaction_type[3][1] = 1;
  interaction_type[2][3] = interaction_type[3][2] = 0;
  interaction_type[0][0] = 0;
  interaction_type[1][1] = 0;
  interaction_type[2][2] = 0;
  interaction_type[3][3] = 0;

/*
  for (int i=0; i<n_species; i++) { 
   for (int j=i; j<n_species; j++) {
*Substrate species => BM
  if(i<n_substrate_species && j<n_substrate_species) 
  interaction_type[i][j] = interaction_type[j][i] = 0;

If gas phase species is Mg and substrate => BM  Mg-Al, Mg-O 
  if(j>n_substrate_species && i<n_substrate_species && strncmp(!element_name[j],"Mg",2 ))
  interaction_type[i][j] = interaction_type[j][i] = 0;
  printf("Element is %s" element_name[i]) ;

If gas phase species and F and Al in substrate => F-Al 
  if(j>n_substrate_species && i<n_substrate_species && strncmp(!element_name[j],"F",1 ) && strncmp(!element_name[i],"Al",2)
  interaction_type[i][j] = interaction_type[j][i] = 0

If gas phase species and F and O in substrate => F-O 
  if(j>n_substrate_species && i<n_substrate_species && strncmp(!element_name[j],"F",1 ) && strncmp(!element_name[i],"O",1)
 interaction_type[i][j] = interaction_type[j][i] = 1

Gas-phase-gas phase 
  if(j>n_substrate_species && i>n_substrate_species)
  interaction_type[i][j] = interaction_type[j][i] = 0 */



	

  /* and borders of range where the join
     polynomial is to be used. */
   for (int i=0; i<n_species; i++) 
    for (int j=i; j<n_species; j++)    
      if (interaction_type[i][j] == 3) {
	calculate_join_params_hybrid_pot(sigma_inter[i][j],
					 epsilon_inter[i][j],
					 &join_range_limits[i][j][0],
					 &join_range_limits[i][j][1],
					 join_coeff_inter[i][j]);
	
	join_range_limits[j][i][0] = 
	  join_range_limits[i][j][0];
	join_range_limits[j][i][1] = 
	  join_range_limits[i][j][1];
	for (int cnt=0; cnt<6; cnt++)
	  join_coeff_inter[j][i][cnt] = 
	    join_coeff_inter[i][j][cnt];
      }   



  /* calculates coefficients of 
     join polynomial outside 
     the cutoff range */
  calculate_truncation_params(n_species, 
			      q, alpha, b, c, d, rho, 
			      sigma_lj, epsilon_lj,
			      sigma_inter, epsilon_inter,
			      join_coeff_inter,
			      join_range_limits,
			      interaction_type,
			      a,
			      cutoff_radius,
			      potential_range);	  
  
  
  return 0;


}
