#include "header.h"


int calculate_truncation_params(int n_species,
				double *q_bm, 
				double alpha_bm[][n_max_type],
				double b_bm[][n_max_type], 
				double c_bm[][n_max_type],
				double d_bm[][n_max_type], 
				double rho_bm[][n_max_type],
				double sigma_lennard_jones[][n_max_type],
				double eps_lennard_jones[][n_max_type],
				double sigma_mixed_interact[][n_max_type],
				double eps_mixed_interact[][n_max_type],
				double hybrid_join_coeff[][n_max_type][6],
				double hybrid_join_interval_limits[][n_max_type][2],
				int interact_type[][n_max_type],
				double trunc_coeff[][n_max_type][6],
				double cutoff_distance[][n_max_type],
				double pot_range[][n_max_type])

/*   
     Convention for interaction 
     types is as follows: 
     Id numb  Interaction type
     0        Born-Mayer (interactions in MgF2) 
     1        Lennard-Jones ( Xe-Xe interactions )
     2,3      10-5 potential ( Xe-MgF2 interactions )
     2        Xe-F 10-5 interaction
     3        Xe-Mg 10-5 and r^(-5) hybrid interaction 
*/



{

  /* prototypes of external functions */
  
  /* functions calculating 
     analytical second derivative
     of potentials used */
  extern int calculate_born_mayer_second_deriv
    (double dist_12,
     double *p_pot12_second_deriv,
     double rho_param,
     double q1, double q2,
     double b12, double c12,
     double d12, 
     double alpha_coulomb_12);
  extern int calculate_lj_second_deriv
    (double dist12_squared,
     double *p_pot12_second_deriv,
     double q1, double q2,
     double alpha_coulomb_12,
     double sigma12,
     double epsilon12);
  extern int calculate_10_5_second_deriv
    (double dist12_squared,
     double *p_pot12_second_deriv,
     double sigma12,
     double epsilon12);
  extern int calculate_hybrid_potential_second_deriv
    (double dist_squared,
     double *p_pot_second_deriv,
     double sigma,
     double epsilon,
     double r1, double r2,
     double *join_coeff);
  
  /* prototype of function calculating
     the factorial of an integer number */
  extern int fact(int n);
  
  
  /* calculate parameters of  
     potential truncation */
  
  for (int i=0; i<n_species; i++)
    for (int j=i; j<n_species; j++) {
      
      double rcutoff = cutoff_distance[i][j];
      double rcutoff_squared =  
	cutoff_distance[i][j]* cutoff_distance[i][j];
      double potential_range = pot_range[i][j];
      double pot_rcutoff, deriv_pot_rcutoff, 
	second_deriv_pot_rcutoff;
      /* allocate memory for arrays */
      double *rcutoff_pow = calloc(6, sizeof(double));
      rcutoff_pow[0] = 1.0;
      for (int cnt=1; cnt<6; cnt++)
	rcutoff_pow[cnt] = rcutoff*rcutoff_pow[cnt-1];
      double *pot_range_pow = calloc(6, sizeof(double));
      pot_range_pow[0] = 1.0;
      for (int cnt=1; cnt<6; cnt++)
	pot_range_pow[cnt] = potential_range*pot_range_pow[cnt-1];
      /* allocate memory for  (6 x 6)-matrix 
	 of coefficients of linear system */
      double **coeff_matrix = calloc(6, sizeof(double*));
      for (int cnt=0; cnt<6; cnt++)
	coeff_matrix[cnt] = calloc(6, sizeof(double));
      /* allocate memory for vector of known terms 
	 on the right-hand-side of the equality */
      double **rhs_vect = calloc(6, sizeof(double*));
      for (int cnt=0; cnt<6; cnt++)
	rhs_vect[cnt] = calloc(1, sizeof(double));
      
      int interaction = interact_type[i][j];

      if ( interaction == 0 )  { /* Born-Mayer potential */ 
	/* calculate Born-Mayer potential 
	   and its first derivative  
	   at cutoff distance */
	calculate_born_mayer_potential(rcutoff_squared,
				       &pot_rcutoff, 
				       &deriv_pot_rcutoff,
				       rho_bm[i][j],
				       q_bm[i], q_bm[j],
				       b_bm[i][j], c_bm[i][j],
				       d_bm[i][j], alpha_bm[i][j],
				       rcutoff+10.0,
				       potential_range,
				       trunc_coeff[i][j]);
	/* calculate actual first derivative
	   of Born-Mayer potential at 
	   cutoff distance */
	deriv_pot_rcutoff /= rcutoff;
	
	
	/* calculate second derivative of Born-Mayer 
	   potential at cutoff distance */
	calculate_born_mayer_second_deriv(rcutoff,
					  &second_deriv_pot_rcutoff,
					  rho_bm[i][j],
					  q[i], q[j],
					  b_bm[i][j], c_bm[i][j],
					  d_bm[i][j], 
					  alpha_bm[i][j]);	      
	
      } /* end of if (interaction == 0) */
      else if ( interaction == 1 ) { /* Lennard-Jones potential */
      	/* calculate Lennard-Jones potential 
	   and its first derivative  
	   at cutoff distance */
	calculate_lj_potential(rcutoff_squared,
			       &pot_rcutoff, 
			       &deriv_pot_rcutoff,
                               q_bm[i], q_bm[j],
                               alpha_bm[i][j],
			       sigma_lennard_jones[i][i],
			       eps_lennard_jones[i][j],
			       rcutoff+10.0,
                               potential_range,
			       trunc_coeff[i][j]);
	
	/* calculate actual first derivative
	   of Lennard-Jones potential at 
	   cutoff distance */
	deriv_pot_rcutoff /= rcutoff;
	
	
	/* calculate second derivative 
	   of Lennard-Jones potential 
	   at cutoff distance */
	calculate_lj_second_deriv(rcutoff_squared,
				  &second_deriv_pot_rcutoff,
                                  q[i],q[j],
                                  alpha_bm[i][j],
     				  sigma_lennard_jones[i][i],
				  eps_lennard_jones[i][j]);
	
      } /* end of if (interaction == 1) */
      else if ( interaction == 2 ) { /* 10-5 potential */
	
	/* calculate 10-5 potential 
	   and its first derivative  
	   at cutoff distance */
	calculate_10_5_potential(rcutoff_squared,
				 &pot_rcutoff,
				 &deriv_pot_rcutoff,
				 sigma_mixed_interact[i][j],
				 eps_mixed_interact[i][j],
				 rcutoff+10.0,
				 trunc_coeff[i][j]);

	/* calculate actual first derivative
	   of 10-5 potential at 
	   cutoff distance */
	deriv_pot_rcutoff /= rcutoff;
	  
	/* calculate second derivative 
	   of 10-5 potential 
	   at cutoff distance */
	calculate_10_5_second_deriv(rcutoff_squared,
				    &second_deriv_pot_rcutoff,
				    sigma_mixed_interact[i][j],
				    eps_mixed_interact[i][j]);
	
      }  /* end of if (interaction == 2) */
      else if ( interaction == 3 ) {   /* hybrid 10-5 and 1/r^5  potential */
	
	/* make sure that cutoff distance is greater than 
	   right-hand-side limit of area where join 
	   polynomial between two parts of potential is used */
	if ( hybrid_join_interval_limits[i][j][1] > rcutoff 
	     ||
	     hybrid_join_interval_limits[i][j][0] > rcutoff ) {
	  printf("\n%s\n%s\n%s%.3lf%s%.3lf%s\n%s\n%s%.3lf%s\n%s\n%s\n",
		 "An error in your input set has been found by",
		 "function calculate_truncation_parameters.",
		 "The range [", 
		 hybrid_join_interval_limits[i][j][0], ",", 
		 hybrid_join_interval_limits[i][j][1], 
		 "] for the polynomial",
		 "part of the hybrid potential is not compatible",
		 "with the cutoff distance of ", rcutoff, ".",
		 "Please, increase the cutoff distance.",
		 "This program is exiting, sorry!");
	  exit(1);
	}
	
	
	/* calculate hybrid potential 
	   and its first derivative  
	   at cutoff distance */
	calculate_hybrid_potential(rcutoff_squared,
				   &pot_rcutoff,
				   &deriv_pot_rcutoff,
				   sigma_mixed_interact[i][j],
				   eps_mixed_interact[i][j],
				   hybrid_join_interval_limits[i][j][0],
				   hybrid_join_interval_limits[i][j][1],
				   rcutoff+10.0,
				   hybrid_join_coeff[i][j],
				   trunc_coeff[i][j]);

	
	/* calculate actual first derivative
	   of hybrid potential at 
	   cutoff distance */
	deriv_pot_rcutoff /= rcutoff;
	
	calculate_hybrid_potential_second_deriv
	  (rcutoff_squared,
	   &second_deriv_pot_rcutoff,
	   sigma_mixed_interact[i][j],
	   eps_mixed_interact[i][j],
	   hybrid_join_interval_limits[i][j][0],
	   hybrid_join_interval_limits[i][j][1],
	   hybrid_join_coeff[i][j]);
	
	  
	  }  /* end of if (interaction == 3) */
      
      
      
      /* the linear system to solve is of 6 equations 
	 in 6 unknowns:
	 P(r_cut) = V(r_cut) 
	 P'(r_cut) = V'(r_cut)
	 P''(r_cut) = V''(r_cut)
	 P(r_range) = 0
	 P'(r_range) = 0
	 P''(r_range) = 0;
	 where: P(r) = a0 + a1*r + a2*r^2 + a3*r^3 + a4*r^4 + a5*r^5 
	 The solution to find is the vector {a0,a1,a2,a3,a4,a5}
      */
      
      /* construct (6 x 6)-matrix of coefficients 
	 of linear system */
      for(int row_ind=0; row_ind<3; row_ind++)
	for(int col_ind=0; col_ind<6; col_ind++) 
	  if ( col_ind < row_ind )
	    coeff_matrix[row_ind][col_ind] = 0.0;
	  else
	    coeff_matrix[row_ind][col_ind] = 
	      fact(col_ind)/fact(col_ind-row_ind)
	      *rcutoff_pow[col_ind-row_ind];
      for(int row_ind=0; row_ind<3; row_ind++)
	for(int col_ind=0; col_ind<6; col_ind++) 
	  if ( col_ind <  row_ind )
	    coeff_matrix[row_ind+3][col_ind] = 0.0;
	  else
	    coeff_matrix[row_ind+3][col_ind] = 
	      fact(col_ind)/fact(col_ind-row_ind)
	      *pot_range_pow[col_ind-row_ind];
      
      
      
      /* construct vector of known terms on the 
	 right-hand-side of the equality */
      rhs_vect[0][0] = pot_rcutoff;
      rhs_vect[1][0] = deriv_pot_rcutoff;
      rhs_vect[2][0] = second_deriv_pot_rcutoff;
      rhs_vect[3][0] = rhs_vect[4][0] = rhs_vect[5][0] = 0.0;
      
      
      /* solve linear system and find 
	 polynomial coefficients */
      gauss_jordan(coeff_matrix, rhs_vect, 6, 1);
      
      /* copy solution into matrix 
	 of truncation parameters */
      for (int k=0; k<6; k++)
	trunc_coeff[i][j][k] = 
	  trunc_coeff[j][i][k] = rhs_vect[k][0];
      
      /* free dynamically allocated memory */
      free(rcutoff_pow);
      free(pot_range_pow);
      for (int cnt=0; cnt<6; cnt++)
	free(coeff_matrix[cnt]);
      free(coeff_matrix);
      for (int cnt=0; cnt<6; cnt++)
	free(rhs_vect[cnt]);
      free(rhs_vect);
      
     
      
      
      
    }  /* end of for (int j=i; j<n_species; j++) */
  





      
 
   
   /* symmetrise array of coefficients 
      of truncation polinomial */
   for (int i=0; i<6; i++) 
     for(int j=0; j<n_species; j++) 
       for(int k=0; k<j; k++)
	 trunc_coeff[j][k][i] = trunc_coeff[k][j][i];
  

   
   return 0;
}



