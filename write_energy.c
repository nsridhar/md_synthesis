#include "header.h"



/* function to reference in case of 
   ordinary MD run */


int write_energy(char *output_file_name)
{
  FILE *p_output_file;
 
  
  if ( (p_output_file=fopen(output_file_name,"a")) == NULL )
    {
      printf("\n%s\n%s%s\n%s\n\n",
	     "A fatal error occurred in function write_energy(char *)",
	     "while trying to open file ", output_file_name,
	     "Cannot continue, sorry!");
      exit(1);
    }
  
  
  //fprintf(p_output_file,"%6d %15lu %12.3e %18.5lf\n", 
  //  current_block,  current_step, current_time,
  //  potential_energy); 
  fprintf(p_output_file,
	  "%6d %15lu %12.3e %20.6lf %20.6lf %12.3e %12.3lf\n", 
	  current_block,  current_step, current_time,
	  potential_energy, kinetic_energy,
	  system_temperature, 
	  potential_energy+kinetic_energy); 
  


  fclose(p_output_file);
  

  return(0);

}




/****************************************************************/


/* function to reference in the 
   case of local optimisation 
   based on MD quench */

/* time step duration is to be 
   expressed in  seconds,
   potential energy in eV, 
   and temeperature in Kelvin */

int write_energy_quench(char *output_file_name, 
			int block, 
			unsigned long int step, 
			double time_over_s,
			double pot_en, 
			double kin_en,
			double temp)
{
  FILE *p_output_file;
  //fopen(f_output_file,);
  
  if ( (p_output_file=fopen(output_file_name,"a")) == NULL )
    {
      printf("\n%s\n%s%s\n%s\n\n",
	     "A fatal error occurred in function write_energy_quench",
	     "while trying to open file ", output_file_name,
	     "Cannot continue, sorry!");
      exit(1);
    }
  
  
  fprintf(p_output_file,
	  "%6d %15lu %12.3e %20.6lf %20.6lf %12.3e\n", 
	  block,  step, time_over_s,
	  pot_en, kin_en, temp); 
	 

  fclose(p_output_file);
  

  return(0);

}

