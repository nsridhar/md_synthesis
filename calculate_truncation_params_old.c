#include "header.h"


int calculate_truncation_params(int n_species,
				double *q_bm, 
				double miu_bm[][n_max_type],
				double b_bm[][n_max_type], 
				double c_bm[][n_max_type],
				double d_bm[][n_max_type], 
				double rho_bm,
				double sigma_lennard_jones[][n_max_type],
				double eps_lennard_jones[][n_max_type],
				double sigma_mixed_interact[][n_max_type],
				double eps_mixed_interact[][n_max_type],
				//double hybrid_join_coeff[][n_max_type][6],
				int interact_type[][n_max_type],
				double trunc_coeff[][n_max_type][6],
				double cutoff_distance[][n_max_type],
				double pot_range[][n_max_type])

/*   Convention for interaction 
     types is as follows: 
     Id numb  Interaction type
     0        Born-Mayer (substrate) 
     1        Lennard-Jones ( depositing species )
     2        10-5 potential ( substrate-depositing species ) */
/* */


{
  /* calculate parameters of  
     potential truncation */

  for (int i=0; i<n_species; i++)
    for (int j=i; j<n_species; j++) {

      int interaction = interact_type[i][j];
      
     
      if ( interaction == 0 )  { /* Born-Mayer potential */
	
	double rcut[16], range[16];
	
	rcut[0] = range[0] = 1;
	for (int power=1; power<16; power++) {
	  rcut[power] = cutoff_distance[i][j]*rcut[power-1]; 
	  range[power] = pot_range[i][j]*range[power-1]; 
	}
	
	double qi = q_bm[i];
	double qj = q_bm[j];
	double rho = rho_bm[i][j];
	double rho_squared = rho*rho;
	double miu = miu_bm[i][j];
	double bij = b_bm[i][j];
	double cij = c_bm[i][j];
	double dij = d_bm[i][j];
	double k = one_over_4pieps0;
	
	double rcut_minus_range_minus5 = 
	  pow(rcut[1]-range[1], -5.0);

	
	double fact_1 = rcut_minus_range_minus5 / 
	  (2.0*rcut[12]*rho_squared);
	double fact_2 = rcut_minus_range_minus5 / 
	  (2.0*rcut[13]*rho_squared);    
	double fact_3 = rcut_minus_range_minus5 / 
	  (2.0*rcut[14]*rho_squared);
	
	double exp1 = exp(-rcut[1]*(rcut[1]*miu+1.0/rho));
	double one_over_exp1 = 1.0/exp1;
	double exp_rcut_over_rho = exp(rcut[1]/rho);
	double exp_rcut_squared_miu = exp(rcut[1]*rcut[1]*miu);


	trunc_coeff[i][j][0] = - fact_1*exp1*range[3]
	  * ( 
	     2 * rho_squared  
	     * ( cij * one_over_exp1
		 *(136*rcut[2]
		   -221*rcut[1]*range[1]
		   +91*range[2])
		 + exp_rcut_over_rho*rcut[6]
		 *(-dij*exp_rcut_squared_miu
		   *(55*rcut[2]
		     -77*rcut[1]*range[1]
		     +28*range[2])
		   +k*qi*qj*rcut[5]
		   *(3*(5*rcut[2]
			-4*rcut[1]*range[1]
			+range[2])
		     +3*rcut[2]*miu
		     *(3*rcut[2]
		       -4*rcut[1]*range[1]
		       +range[2])
		     +2*rcut[4]*miu*miu
		     *(rcut[1]-range[1])
		     *(rcut[1]-range[1])))) 
	     + bij*exp_rcut_squared_miu*rcut[12]
	     *(rcut[2]
	       *(rcut[1]-range[1])
	       *(rcut[1]-range[1])
	       +2*rcut[1]*rho
	       *(4*rcut[2]
		 -5*rcut[1]*range[1]
		 +range[2])
	       +2*rho_squared
	       *(10*rcut[2]
		 -5*rcut[1]*range[1]
		 +rcut[2])) 
	 );
	
	
	trunc_coeff[i][j][1] = fact_2*exp1*range[2]
	  * (
	     2 * rho_squared 
	     * ( 6* cij * one_over_exp1
		 *(68*rcut[3]
		   -68*rcut[2]*range[1]
		   -23*rcut[1]*range[2]
		   +28*range[3])
		 + rcut[6]
		 *(-3*dij*one_over_exp1
		   *(55*rcut[3]
		     -44*rcut[2]*range[1]
		     -17*rcut[1]*range[2]
		     +16*range[3])
		   +exp_rcut_over_rho*k*qi*qj*rcut[5]
		   *(3*(15*rcut[3]
			-4*rcut[2]*range[1]
			-2*rcut[1]*range[2]
			+range[3])
		     +rcut[2]*miu
		     *(rcut[1]-range[1])
		     *(27*rcut[2]
		       +7*rcut[1]*range[1]
		       -4*range[2])
		     +2*rcut[4]*miu*miu
		     *(rcut[1]-range[1])
		     *(rcut[1]-range[1])
		     *(3*rcut[1]+2*range[1]))))
	     + bij*exp_rcut_squared_miu*rcut[13]
	     *(rcut[1]*(rcut[1]-range[1])*(rcut[1]-range[1])
	       *(3*rcut[1]+2*range[1])
	       +2*rho*(rcut[1]-range[2])*(6*rcut[1]-range[2])
	       *(2*rcut[1]+range[2])
	       +60*rcut[2]*rho_squared)
	     );
	
	trunc_coeff[i][j][2] =  -fact_3*exp1*range[1]
	  * (
	     2 * rho_squared
	     * ( 6 * cij * one_over_exp1
		 *(68*rcut[4]
		   +17*rcut[3]*range[1]
		   -140*rcut[2]*range[2]
		   +52*rcut[1]*range[3]
		   +13*range[4])
		 + rcut[6]
		 *(-3*dij*one_over_exp1
		   *(55*rcut[4]
		     +22*rcut[3]*range[1]
		     -92*rcut[2]*range[2]
		     +28*rcut[1]*range[3]
		     +7*range[4])
		   +exp_rcut_over_rho*k*qi*qj*rcut[5]
		   *(45*rcut[4]
		     +36*rcut[3]*range[1]
		     -26*rcut[2]*range[2]
		     +4*rcut[1]*range[3]
		     +range[4]
		     +rcut[2]*miu
		     *(rcut[1]-range[1])
		     *(9*rcut[1]+range[1])
		     *(3*rcut[2]
		       +4*rcut[1]*range[1]
		       -range[2])
		     +2*rcut[4]*miu*miu
		     *(rcut[1]-range[1])
		     *(rcut[1]-range[1])
		     *(3*rcut[2]
		       +6*rcut[1]*range[1]
		       +range[2]))))
	     + bij*exp_rcut_squared_miu*rcut[14]
	     *((rcut[1]-range[1])*(rcut[1]-range[1])
	       *(3*rcut[2]+6*rcut[1]*range[1]+range[2])
	       +12*rho*rcut[1]
	       *(rcut[1]-range[1])
	       *(2*rcut[1]+3*range[1])
	       +60*rcut[1]*rho_squared
	       *(rcut[1]+range[1]))
	     );
	
	
	
	trunc_coeff[i][j][3]=  fact_3*exp1
	  * (
	     2 * rho_squared
	     * ( 2 * cij * one_over_exp1
		 *(68*rcut[4]
		   +272*rcut[3]*range[1]
		   -391*rcut[2]*range[2]
		   -36*rcut[1]*range[3]
		   +117*range[4])
		 + rcut[6]
		 *(-dij*one_over_exp1
		   *(55*rcut[4]
		     +220*rcut[3]*range[1]
		     -242*rcut[2]*range[2]
		     -36*rcut[1]*range[3]
		     +63*range[4])
		   +exp_rcut_over_rho*k*qi*qj*rcut[5]
		   *(3*(5*rcut[4]
			+20*rcut[3]*range[1]
			-4*rcut[2]*range[2]
			-2*rcut[1]*range[3]
			+range[4])
		     +3*rcut[2]*miu
		     *(rcut[1]-range[1])
		     *(3*rcut[3]
		       +15*rcut[2]*range[1]
		       +3*rcut[1]*range[2]
		       -range[3])
		     +2*rcut[4]*miu*miu
		     *(rcut[1]-range[1])
		     *(rcut[1]-range[1])
		     *(rcut[2]
		       +6*rcut[1]*range[1]
		       +3*range[2]))))
	     + bij*exp_rcut_squared_miu*rcut[14]
	     *((rcut[1]-range[1])*(rcut[1]-range[1])
	       *(rcut[2]+6*rcut[1]*range[1]+3*range[2])
	       +4*rho
	       *(rcut[1]-range[1])
	       *(2*rcut[2]+10*rcut[1]*range[1]+3*range[2])
	       +20*rho_squared
	       *(rcut[2]+4*rcut[1]*range[1]+range[2]))
	     );
	
	
	trunc_coeff[i][j][4] = fact_3*exp1
	  * (
	     -2 * rho_squared
	     * ( 3 * cij * one_over_exp1
		 *(85*rcut[3]
		   -17*rcut[2]*range[1]
		   -136*rcut[1]*range[2]
		   +78*range[3])
		 + rcut[6]
		 *(-3*dij*one_over_exp1
		   *(33*rcut[3]
		     -44*rcut[1]*range[2]
		     +21*range[3])
		   +exp_rcut_over_rho*k*qi*qj*rcut[5]
		   *(3*(8*rcut[3]
			+5*rcut[2]*range[1]
			-4*rcut[1]*range[2]
			+range[3])
		     +rcut[2]*miu
		     *(16*rcut[3]
		       +rcut[2]*range[1]
		       -20*rcut[1]*range[2]
		       +3*range[3])
		     +2*rcut[4]*miu*miu
		     *(rcut[1]-range[1])
		     *(rcut[1]-range[1])
		     *(2*rcut[1]+3*range[1]))))
	     - bij*exp_rcut_squared_miu*rcut[14]
	     *((rcut[1]-range[1])
	       *(rcut[1]-range[1])
	       *(2*rcut[1]+3*range[1])
	       +2*rho
	       *(rcut[1]-range[1])
	       *(7*rcut[1]+8*range[1])
	       +30*rho_squared*(rcut[1]+range[1]))
	     );

 
	
	trunc_coeff[i][j][5] =  fact_3*exp1
	  * (
	     2 * rho_squared
	     * ( 6 * cij * one_over_exp1
		 *(20*rcut[2]
		   -32*rcut[1]*range[1]
		   +13*range[2])
		 + exp_rcut_over_rho*rcut[6]
		 *(-3*dij*exp_rcut_squared_miu
		   *(15*rcut[2]
		     -20*rcut[1]*range[1]
		     +7*range[2])
		   +k*qi*qj*rcut[5]
		   *(10*rcut[2]
		     -5*rcut[1]*range[1]
		     +range[2]
		     +rcut[2]*miu
		     *(7*rcut[2]
		       -8*rcut[1]*range[1]
		       +range[2])
		     +2*rcut[4]*miu*miu
		     *(rcut[1]-range[1])
		     *(rcut[1]-range[1]))))
	     + bij*exp_rcut_squared_miu*rcut[14]
	     *((rcut[1]-range[1])*(rcut[1]-range[1])
	       +6*rho*(rcut[1]-range[1])
	       +12*rho_squared)
	     );
	
	
      } /* end of if (interaction == 0) */
      
      
      else if ( interaction == 1 ) { /* Lennard-Jones potential */
	
	double rcut[15], range[15];
	
	rcut[0] = range[0] = 1;
	for (int power=1; power<15; power++) {
	  rcut[power] = cutoff_distance[i][j]*rcut[power-1]; 
	  range[power] = pot_range[i][j]*range[power-1]; 
	}
	
	double rcut_minus_range_minus5 = 
	  pow(rcut[1]-range[1], -5.0);
	
	double eps = eps_lennard_jones[i][j];

	double sigma_6  = pow(sigma_lennard_jones[i][j],6);
	//double sigma_12 = pow(sigma_lennard_jones[i][j],12);
	
	double prefact1 = eps*sigma_6*range[3]
	  *rcut_minus_range_minus5/rcut[12];
	double prefact2 = eps*sigma_6*range[2]
	  *rcut_minus_range_minus5/rcut[13];
	double prefact3 = eps*sigma_6
	  *rcut_minus_range_minus5/rcut[14];

	trunc_coeff[i][j][0] = 4.0*prefact1
	  *(sigma_6
	    *(-136*rcut[2]
	      +221*rcut[1]*range[1]
	      -91*range[2])
	    +rcut[6]
	    *(55.0*rcut[2]
	      -77.0*rcut[1]*range[1]
	      +28.0*range[2]));	
	
	trunc_coeff[i][j][1] = 12.0*prefact2
	  *(rcut[6]
	    *(-55.0*rcut[3]
	      +44.0*rcut[2]*range[1]
	      +17.0*rcut[1]*range[2]
	      -16.0*range[3])
	    +2.0*sigma_6
	    *(68.0*rcut[3]
	      -68.0*rcut[2]*range[1]
	      -23.0*rcut[1]*range[2]
	  +28.0*range[3]));

	trunc_coeff[i][j][2] = 12.0*prefact3*range[1]
	  *(rcut[6]
	    *(55.0*rcut[4]
	      +22.0*rcut[3]*range[1]
	      -92.0*rcut[2]*range[2]
	      +28.0*rcut[1]*range[3]
	      +7.0*range[4])
	    -2.0*sigma_6
	    *(68.0*rcut[4]
	      +17.0*rcut[3]*range[1]
	      -140.0*rcut[2]*range[2]
	      +52.0*rcut[1]*range[3]
	      +13.0*range[4]));

	trunc_coeff[i][j][3] = 4.0*prefact3
	  *(rcut[6]
	    *(-55.0*rcut[4]
	      -220.0*rcut[3]*range[1]
	      +242.0*rcut[2]*range[2]
	      +36.0*rcut[1]*range[3]
	      -63.0*range[4])
	    +2.0*sigma_6
	    *(68.0*rcut[4]
	      +272.0*rcut[3]*range[1]
	      -391.0*rcut[2]*range[2]
	      -36.0*rcut[1]*range[3]
	      +117.0*range[4]));	
	
	trunc_coeff[i][j][4] = 12.0*prefact3
	  *(sigma_6
	    *(-85*rcut[3]
	      +17*rcut[2]*range[1]
	      +136*rcut[1]*range[2]
	      -78*range[3])
	    +rcut[6]
	    *(33.0*rcut[3]
	      -44.0*rcut[1]*range[2]
	      +21.0*range[3]));
	
	trunc_coeff[i][j][5] = 12.0*prefact3
	  *(rcut[6]
	    *(-15*rcut[2]
	      +20*rcut[1]*range[1]
	      -7*range[2])
	    +sigma_6
	    *(40.0*rcut[2]
	      -64.0*rcut[1]*range[1]
	      +26.0*range[2]));
	
      } /* end of if ( interaction == 1 ) */ 

      
      else if ( interaction == 2 ) { /* 10-5 potential */
	
	double rcut[13], range[13];
	
	rcut[0] = range[0] = 1;
	for (int power=1; power<13; power++) {
	  rcut[power] = cutoff_distance[i][j]*rcut[power-1]; 
	  range[power] = pot_range[i][j]*range[power-1]; 
	}

	double rcut_minus_range_minus5 = 
	  pow(rcut[1]-range[1], -5.0);

	double eps = eps_mixed_interact[i][j];

	double sigma_5  = pow(sigma_mixed_interact[i][j],5);

	double prefact1 = eps*sigma_5
	  *rcut_minus_range_minus5/rcut[10];
	double prefact2 = eps*sigma_5
	  *rcut_minus_range_minus5/rcut[11];
	double prefact3 = eps*sigma_5
	  *rcut_minus_range_minus5/rcut[12];
	
	trunc_coeff[i][j][0] = 
	  12.0*prefact1*range[3]*
	  (sigma_5*(-35.0*rcut[2]
		    +55.0*rcut[1]*range[1]
		    -22.0*range[2])
	   + rcut[5]*(15.0*rcut[2]
		      -20.0*rcut[1]*range[1]
		      +7.0*range[2]));
	
	trunc_coeff[i][j][1] = 
	  20.0*prefact2*range[2]*
	  (rcut[5]*(-27.0*rcut[3]
		    +20.0*rcut[2]*range[1]
		    +8.0*rcut[1]*range[2]
		    -7.0*range[3])
	   + 3.0*sigma_5*(21.0*rcut[3]
			  -20.0*rcut[2]*range[1]
			  -7.0*rcut[1]*range[2]
			  +8.0*range[3]));

	trunc_coeff[i][j][2] = 
	  20.0*prefact3*range[1]*
	  (3.0*rcut[5]*(9.0*rcut[4]
			+4.0*rcut[3]*range[1]
			-14.0*rcut[2]*range[2]
			+4.0*rcut[1]*range[3]
			+range[4])
	   - sigma_5*(63.0*rcut[4]
		      +18.0*rcut[3]*range[1]
		      -124.0*rcut[2]*range[2]
		      +44.0*rcut[1]*range[3]
		      +11.0*range[4]));
	
	trunc_coeff[i][j][3] = 
	  60.0*prefact3*
	  (rcut[5]*(-3.0*rcut[4]
		    -12.0*rcut[3]*range[1]
		    +12.0*rcut[2]*range[2]
		    +2.0*rcut[1]*range[3]
		    -3.0*range[4])
	   + sigma_5*(7.0*rcut[4]
		      +28.0*rcut[3]*range[1]
		      -38.0*rcut[2]*range[2]
		      -4.0*rcut[1]*range[3]
		      +11.0*range[4]));
	
	trunc_coeff[i][j][4] =  
	  20.0*prefact3*
	  (sigma_5*(-39.0*rcut[3]
		    +6.0*rcut[2]*range[1]
		    +60.0*rcut[1]*range[2]
		    -33.0*range[3])
	   + rcut[5]*(16.0*rcut[3]
		      +rcut[2]*range[1]
		      -20.0*rcut[1]*range[2]
		      +9.0*range[3]));
	
	trunc_coeff[i][j][5] =  
	  4.0*prefact3*
	  (-3.0*rcut[5]*(12.0*rcut[2]
			 -15.0*rcut[1]*range[1]
			 +5.0*range[2])
	   + sigma_5*(91.0*rcut[2]
		      -140.0*rcut[1]*range[1]
		      +55.0*range[2]));
	
      }
      
      
      else if ( interaction == 3 ) { /* 10-5 and r^-5* hybrid potential */
	
	double eps = eps_mixed_interact[i][j];
	double sigma = sigma_mixed_interact[i][j];

	double sigma_5 = pow(sigma,5.0);
	
	double rcut[12], range[12];
	rcut[0] = range[0] = 1.0;
	for (int k=1; k<13; k++) {
	  rcut[k] = cutoff_distance[i][j]*rcut[k-1];
	  range[k] = pot_range[i][j]*range[k-1]; 
	}
	
	double rcut_minus_range_minus5 = pow(rcut[1]-range[1],
					     -5.0);
	
	double trunc_prefact1 = eps*sigma_5
	  *rcut_minus_range_minus5
	  /rcut[5];
	double trunc_prefact2 = eps*sigma_5
	  *rcut_minus_range_minus5
	  /rcut[6];
	double trunc_prefact3 = eps*sigma_5
	  *rcut_minus_range_minus5
	  /rcut[7];
	
	trunc_coeff[i][j][0] = 
	  -12.0*trunc_prefact1*range[3]
	  *(15.0*rcut[2]
	    -20.0*rcut[1]*range[1]
	    +7.0*range[2]);
	
	trunc_coeff[i][j][1] = 
	  20.0*trunc_prefact2*range[2]
	  *(27.0*rcut[3]
	    -20.0*rcut[2]*range[1]
	    -8.0*rcut[1]*range[2]
	    +7.0*range[3]);
	
	trunc_coeff[i][j][2] = 
	  -60.0*trunc_prefact3*range[1]
	  *(9.0*rcut[4]
	    +4.0*rcut[3]*range[1]
	    -14.0*rcut[2]*range[2]
	    +4.0*rcut[1]*range[3]
	    +range[4]);
	
	trunc_coeff[i][j][3] = 
	  60.0*trunc_prefact3
	  *(3.0*rcut[4]
	    +12.0*rcut[3]*range[1]
	    -12.0*rcut[2]*range[2]
	    -2.0*rcut[1]*range[3]
	    +3.0*range[4]);
	
	trunc_coeff[i][j][4] = 
	  -20.0*trunc_prefact3
	  *(16.0*rcut[3]
	    +rcut[2]*range[1]
	    -20.0*rcut[1]*range[2]
	    +9.0*range[3]);
	
	trunc_coeff[i][j][5] = 
	  12.0*trunc_prefact3
	  *(12.0*rcut[2]
	    -15.0*rcut[1]*range[1]
	    +5.0*range[2]);
      }
      
      
    }/* end of for (int j=i; j<n_species; j++) */
  

  
 
 

  /* symmetrise array of coefficients 
     of truncation polinomial */
  for (int i=0; i<6; i++) 
    for(int j=0; j<n_species; j++) 
      for(int k=0; k<j; k++)
	trunc_coeff[j][k][i] = trunc_coeff[k][j][i];
  
/*   /\* symmetrise array of coefficients  */
/*      of join polinomial for hybrid  */
/*      10-5 and r^(-5) potential *\/ */
/*   for (int i=0; i<6; i++)  */
/*     for(int j=0; j<n_species; j++)  */
/*       for(int k=0; k<j; k++) */
/* 	hybrid_join_coeff[j][k][i] =  */
/* 	  hybrid_join_coeff[k][j][i]; */
  
  return 0;
}
