#include "header.h"


/* Assign new-cluster centre-of-mass  
   with coordinates.
   x and y coordinate are randomly
   chosen within the limits 
   of the simulation unit cell. 
   z coordinate is passed on by 
   calling function */
int assign_cm_coordinates(double cm_z_coord,
			  double box_x_dim, 
			  double box_y_dim,
			  double box_z_dim,
			  double *x_pos, 
			  double *y_pos, 
			  double *z_pos)
{

  double x_coord;
  double y_coord;
  double z_coord = cm_z_coord;
  
  /* generate random number 
     between 0 and 1 */
  x_coord = ran2();
  y_coord = ran2();
  /* generate random number 
     between 0 and boxx-boxy */
  x_coord *= box_x_dim;
  y_coord *= box_y_dim;
  /* apply periodic boundary conditions */
  x_coord -= anint(x_coord/box_x_dim)*box_x_dim;
  y_coord -= anint(y_coord/box_y_dim)*box_y_dim;
  
  /* "export" coordinates 
     to calling function */
  *x_pos = x_coord;
  *y_pos = y_coord;
  *z_pos = z_coord;
 
  /* if z coordinate of atom is above 
     upper boundary of unit cell, then 
     return 1 */
  if ( z_coord > box_z_dim ) {
    printf("\n%s\n%s%.3lf%s\n%s%.3lf%s\n%s\n\n",
	   "Warning in function assign_new_coordinates!",
	   "z coordinate of c.m. passed is ", z_coord, ".",
	   "Upper boundary of simulation cell is at z=", 
	   box_z_dim, ".",
	   "This function is returning 1.");
    return 1;
  }
  else 
    return 0;

}






/*********************************************************************/

/* Given a certain temperature, all 
   atoms constituting a depositing 
   cluster are assigned with a random 
   velocity in such a way that 
   the temperature of the cluster 
   is equal to the input temperature.
   Atoms in the depositing cluster are 
   assumed to be those with indices 
   in the range [init_index,init_index+dep_atom_num-1].
   Arrays species_symbol and species_mass
   contain chemical symbol and mass in 
   a.m.u. of the elements present in the 
   cluster. Indices in the two arrays
   must be consistent, i.e., species_mass[i]
   is mass of element species_symbol[i]. */
int assign_atom_velocities(double init_temp,
			   int init_index,
			   int dep_atom_num,
			   int *atom_type,
			   //cluster dep_clust,
			   //char species_symbol[][3],
			   double *species_mass,
			   double *x_vel, double *y_vel,
			   double *z_vel)
{
  //double *atom_mass = 
  //calloc(dep_clust->unit_num, sizeof(double));
  double mass_cm = 0.0;

  /* calculate mass of cluster centre of mass */
  for (int i=0; i<dep_atom_num; i++)
    mass_cm += species_mass[atom_type[init_index+i]];
  
  
  /* associate each atom in the cluster with
     a mass and calculate mass 
     of centre of mass */
/*   for (int i=0; i<dep_clust->unit_num; i++) { */
    
/*     int species_cnt = 0; */
    
/*     while ( species_cnt >= 0   */
/* 	    && */
/* 	    species_cnt < n_max_type )  */
/*       if ( strncmp */
/* 	   (dep_clust-> */
/* 	    species_symbol[dep_clust->unit_type[i]], */
/* 	    species_symbol[species_cnt], 2)  */
/* 	   == 0 ) { */
/* 	atom_mass[i] = species_mass[species_cnt]; */
/* 	mass_cm += species_mass[species_cnt]; */
/*       } */
/*       else */
/* 	species_cnt++; */
    
/*     /\* calculate mass of centre of mass *\/ */
/*     mass_cm /= dep_clust->unit_num; */

/*     /\* make sure atom has been associated  */
/*        with a mass  *\/ */
/*     if ( species_mass < 1.0e-10 ) { */
/*       printf("%s\n%s%d%s\n%s\n%s%d\n%s\n", */
/* 	     "Fatal error in function assign_atom_velocities!", */
/* 	     "Atom ", i, " could not be associated", */
/* 	     "with any known chemical species.", */
/* 	     "At the end of the cycle, the counter is ", species_cnt, */
/* 	     "It is not possible to continue, sorry!"); */
/*       exit(1); */
/*     } */
  
    
/*   } */
  

  /* Each atom is given the same initial velocity
     along the vertical z-direction. The initial 
     velocity is (3*N*kB*T/mass_cm)^(1/2) for each atom;
     where: N is the number of atoms, T is the
     input temperature, and mass_cm is the mass of
     the centre of mass. In this way, the initial
     cluster energy turns out to be (3/2)*N*kB*T */
  /* calculate velocity expressed in m/s */
  double init_vel = 0.0;
  if ( mass_cm > 1.0e-5) 
    init_vel = 
      sqrt(3.0*dep_atom_num*kb*joule_per_ev*init_temp/
	   (mass_cm*kg_per_amu));
  /* express velocity in Angstr/s */
  init_vel /= metre_per_angstrom;
  
  
  /* assing each depositing atom with initial 
     velocity directed along negative
     z direction only. */
  for (int i=0; i<dep_atom_num; i++) {
    x_vel[init_index+i] = 0.0;
    y_vel[init_index+i] = 0.0;
    z_vel[init_index+i] = -init_vel;
  }
  
  /* calculates temperature for checking */
  double cluster_temp = 0.0;
  double cluster_kin_en = 0.0;
  for (int i=0; i<dep_atom_num; i++) 
    cluster_kin_en += 
      species_mass[atom_type[init_index+i]] *  
      (x_vel[init_index+i]*x_vel[init_index+i] + 
       y_vel[init_index+i]*y_vel[init_index+i] +
       z_vel[init_index+i]*z_vel[init_index+i]);
  /* express kinetic energy in Joule*/
  cluster_kin_en *= 
    0.5*kg_per_amu*metre_per_angstrom
    *metre_per_angstrom;
  cluster_temp = 
    2.0*cluster_kin_en/(3.0*dep_atom_num*kb*joule_per_ev);
  
  
  return 0;
}


/*********************************************************************/



/* This functions is referenced after 
   a new cluster is introduced.
   The main task of this function is 
   to perform two cheks on the 
   position of the new atom:
   (i) The cluster lies above the surface
   of the growing solid;
   (ii) The cluster is not too close to
   other atoms. 
   The function returns 1 if at least one 
   of the two checks turns out to be positive. 
   The function also searches for the quota of
   the highest point of the growing solid. */
int check_new_clust_position(int init_index,
			     int dep_atom_num,
			     int atom_number,
			     int num_fixed_atom,
			     double *solid_highest_quota,
			     double minimum_atom_solid_distance,
			     int *atom_species,
			     double *vdw_radius,
			     double bond_distance[][n_max_type],
			     double box_x_dim, 
			     double box_y_dim,
			     double box_z_dim,
			     double *x_pos, 
			     double *y_pos, 
			     double *z_pos)
{
  
  /* allocate memory for array 
     labelling atoms on the basis 
     of their belonging to substrate: 
     substrate_atom[i] = 1 if atom i belongs to
     substrate, substrate_atom[i] = 0 if atom
     i is in gas phase */
  int *substrate_atom = calloc(atom_number, sizeof(int));
  if ( substrate_atom == NULL ) {
    printf("%s\n%s\n%s\n%s\n",
	   "\nFatal error in function check_new_atom!",
	   "The pointer substrate_atom has been assigned",
	   "with the NULL value due to lack of memory.",
	   "It is impossible to continue, sorry!");
    free(substrate_atom);
    exit(1);
  }
  
  
  
  /* assign atoms in the substrate with
     a label of 1, and  atoms in gas phase 
     with a label of 0 */
  label_substrate_atoms(atom_number,
			//num_fixed_atom,
			atom_species, 
			substrate_atom,
			bond_distance,
			box_x_dim, box_y_dim,
			x_pos, y_pos, z_pos);
  
  
  /* make sure cluster has been introduced 
     above topmost surface of growing 
     compound */ 
  

  /* search for highest point of growing solid */
  double max_z = z_pos[0];
  for ( int i=0; i<atom_number; i++ ) 
    if ( substrate_atom[i]  &&
	 z_pos[i] > max_z ) 
      max_z = z_pos[i];   
  *solid_highest_quota = max_z;
  
  for ( int atom_index = init_index; 
	atom_index < init_index + dep_atom_num; 
	atom_index++ )
    if ( z_pos[atom_index] <= max_z+minimum_atom_solid_distance  
	 || 
	 z_pos[atom_index] >= box_z_dim ) {
      printf("\n%s\n%s%.3lf%s\n%s%d\n%s%.3lf%s\n%s%.3lf\n%s\n\n",
	     "Warning in function check_new_atom_position!",
	     "Highest z coordinate of surface has been found to be: ",
	     max_z, ".",
	     "z coordinate of incoming adatom ", atom_index,
	     "is ", z_pos[atom_index], ".",
	     "Vertical dimension of simulation cell is ", box_z_dim,
	     "This function is returning 1.");
      free(substrate_atom);
      return 1;
    }
   
  
  /* make sure distance of deposited atoms 
     from  all other atoms is not too small */

  for ( int atom_index = init_index; 
	atom_index < init_index + dep_atom_num; 
	atom_index++ ) {
    /* find minimum distance between 
       new atom and all others */
    double min_dist_squared = 1.0e20;
    int closest_atom = -1;
    for (int i=0; i<atom_number; i++)
      if ( i != atom_index ) {
	double dist_x = x_pos[i]-x_pos[atom_index];
	/* apply periodic boundary conditions */
	dist_x -= anint(dist_x/box_x_dim)*box_x_dim;
	double dist_y = y_pos[i]-y_pos[atom_index];
	/* apply periodic boundary conditions */
	dist_y -= anint(dist_y/box_y_dim)*box_y_dim;
	double dist_z = z_pos[i]-z_pos[atom_index];
	double dist_squared = 
	  dist_x*dist_x +
	  dist_y*dist_y +
	  dist_z*dist_z;
	if ( dist_squared < min_dist_squared ) {
	  min_dist_squared = dist_squared;
	  closest_atom = i;
	}
      }
    

    if ( closest_atom == -1 ) {
      printf("\n%s\n%s%d%s\n%s\n\n",
	     "Fatal error in function check_new_clust_position",
	     "while checking position of new atom ", 
	     atom_index, ".",
	     "It is impossible to continue, sorry!");
      free(substrate_atom);
      exit(1);
    }
    
  

    /* minimum distance between atoms 
       cannot be less than a fraction
       of the sum of the Van der Waals 
       radii of the two species */
    if ( min_dist_squared < 
	 0.7 * (vdw_radius[atom_species[atom_index]] +
		vdw_radius[atom_species[closest_atom]]) *
	 0.7 * (vdw_radius[atom_species[atom_index]] +
		vdw_radius[atom_species[closest_atom]]) ) {
      free(substrate_atom);
      return 1;  
    }
  
    
  } /* end of  for ( int atom_index = init_index; 
       atom_index < init_index + dep_atom_num; 
       atom_index++ ) */
  
  
  /* clear dyamically allocated memory */
    free(substrate_atom);
  
  
  /* return 0 if atom lies high enough
     above growing solid, and no 
     other atom lies close to new atom */
  return 0;
  
}
