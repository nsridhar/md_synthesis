#include "header.h"

int output_restart_file(int atom_num, char *file_name,
			int *atom_species, char element_symbol[][3],
			int num_x_cell, int num_y_cell, int num_z_cell,
			double *x_pos, double *y_pos, double *z_pos)
{
   FILE *p_output_file;
  
  
  if( (p_output_file=fopen(file_name,"w")) == NULL )    
    {
      printf("Error in function output_restart_file.\n");
      printf("A fatal error occured while opening file %s\n",file_name);
      printf("Cannot continue, sorry!\n");
      exit(1);
    }
  
  fprintf(p_output_file, "%d\n", atom_num);

  fprintf(p_output_file, "%d %d %d\n", num_x_cell, num_y_cell, num_z_cell);

  /* print current position of all atoms */
  for (int i=0; i<atom_num; i++)   
    fprintf(p_output_file, "%2s %20.2f %20.2f %20.2f\n",
	    element_symbol[atom_species[i]], 
	    x_pos[i], y_pos[i],z_pos[i]);
  
  
  fclose(p_output_file);
  
  return(0);


}
