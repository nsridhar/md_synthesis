/* This function returns the value of 
   the coordinate z below which all 
   atoms are affected by the thermostat 
   Input parameters are number of atoms,
   fraction of slab where thermostat is 
   applied, slab thickness, and z-coordinate
   of all atoms in the slab. */ 

/*IGNORE COMMENT BELOW
  The function calculates the lowest 
  z-coordinate among all atoms in the slab
  and returns it as *min_z. */


#include "header.h"

double find_z_thermostat(int atom_number,
			 double slab_fraction,
			 double slab_height,
			 //double *min_z,
			 double z_min,
			 double *z_coord)
{
  
  //double z_min = z_coord[0];

  /* find minimum z coordinate of 
     all atoms in the structure */
  //for ( int i=0; i<atom_number; i++)
  //if ( z_coord[i] < z_min )
  //  z_min = z_coord[i];

  
  double max_z_thermostat = 
    z_min + slab_fraction*slab_height;

  //*min_z = z_min;
  
  return max_z_thermostat;
  
}
