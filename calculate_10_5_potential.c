#include "header.h"

/* This function calculates the value of 
   the interaction potential between 
   an atom 1 and an atom 2 
   which are a distance (dist12_squared)^1/2 
   apart, and which interact via 
   a potential of type 
   V(r) = 4*eps*[(sigma/r)^10-(sigma/r)^5]. 
   The potential is also derived with 
   respect to distance between atom 1 and 2.
   The value pointed to by argument 
   p_dpot12 is the derivative of the
   potential with respect to atom distance
   multiplied by the same distance. 
   (Apologies for this unnecessary complication!)*/

int calculate_10_5_potential(double dist12_squared,
			     double *p_pot12,
			     double *p_dpot12_times_dist,
			     double sigma12,
			     double epsilon12,
			     double cutoff_dist12,
			     double *truncation_params)
{
  /* contribution to potential 
     energy due to interaction 
     between atom 1 and 2 */
  double ener_12 = 0.0; 
  /* derivative of potential 
     with respect to distance 
     between each atom couple 
     multiplied by mutual atom
     distance */
  double potderiv_times_dist = 0.0;
    
  /* calculate distance  
     between atom 1 and 2 */
  double dist_12 = sqrt(dist12_squared);
  
  if ( dist12_squared <= 
       cutoff_dist12*cutoff_dist12 ) { /*Interaction within cutoff radius*/
    
    double sigma_over_r =  sigma12/dist_12;
    double sigma_over_r_5 = pow(sigma_over_r,5);
    double sigma_over_r_10 = pow(sigma_over_r_5,2);
    /* calculate contribution to potential 
       energy due to the interaction 
       between atom i and j */
    ener_12 = 4.0*epsilon12
      *(sigma_over_r_10-sigma_over_r_5);
    /* Calculate derivative of  
       potential with respect 
       to r_12 and multiply 
       by r_12 */
    potderiv_times_dist = 
      20.0*epsilon12
      *(sigma_over_r_5 - 2.0*sigma_over_r_10);
    
  }

  else { /* Interaction outside cutoff radius. 
	    Use truncation parameters */
    
    double coeff[6], 
      dist_pow[6];
    
    for(int n=0; n<6; n++)
      /* coeff[n] = an, 
	 with 
	 pot(r) = 
	 a0+a1*r+a2*r^2+a3*r^3+...+a5*r^5 */
      coeff[n] = truncation_params[n];
    
    dist_pow[0] = 1;
    dist_pow[1] = dist_12;
    dist_pow[2] = dist12_squared;
    for(int n=3; n<6; n++)
      /* dist_pow[n] = (r_12)^n */
      dist_pow[n] = dist_12*dist_pow[n-1]; 
    
    /* Calculate contribution 
       to potential energy
       due to interaction 
       between atom 1 and 2 */
    for(int n=0; n<6; n++)
      ener_12 += coeff[n]*dist_pow[n]; 	 

    /* Calculate derivative 
       of potential 
       with respect to r_ij 
       and multiply by r_ij */
    for (int n=1; n<6; n++) 
      potderiv_times_dist 
	+= n*coeff[n]*dist_pow[n];
  }
  
  
  /* "export" calculated 
     quantities 
     to calling function */
  *p_pot12 = ener_12;
  *p_dpot12_times_dist = potderiv_times_dist;
  
    
  return 0;
}



/*********************************************************/


int calculate_10_5_second_deriv
(double dist12_squared,
 double *p_pot12_second_deriv,
 double sigma12,
 double epsilon12)
{
  double pot_second_deriv;
  
  double dist12 = sqrt(dist12_squared);
  double sigma_over_r = sigma12/dist12;
  double sigma_over_r_2 = sigma_over_r*sigma_over_r;
  double sigma_over_r_5 = sigma_over_r_2*sigma_over_r_2*sigma_over_r;
  /* calculate contribution to potential 
     energy due to the interaction 
     between atom i and j */
  pot_second_deriv = 
    20.0*epsilon12*sigma_over_r_5/dist12_squared*
    (22.0*sigma_over_r_5-6);
  
  *p_pot12_second_deriv = pot_second_deriv;
  
  return 0;
}


