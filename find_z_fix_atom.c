/* This function returns the value of 
   the coordinate z below which all 
   atoms are kept fixed  */

#include "header.h"

double find_z_fix_atom(int atom_number,
		       double slab_fraction,
		       double slab_height,
		       double z_min,
		       double *z_coord)
{
  
  //double z_min = z_coord[0];

  /* find minimum z coordinate of 
     all atoms in the structure */
  //for ( int i=0; i<atom_number; i++)
  //if ( z_coord[i] < z_min )
  //  z_min = z_coord[i];

  
  double max_z_fix = 
    z_min + slab_fraction*slab_height;

  
  return max_z_fix;
  
}
