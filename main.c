/*************************************************/
/*  MD program for simulating dynamics of        */
/*        solid synthesis                        */
/*       Born-Mayer Version 3.4.1                */
/*    Last modified: April 2012                  */
/*************************************************/

/* Deposition is initiated on  a 
   substrate.
   Difference with respect to 
   version 3.3.1 is that an error
   has been corrected in the way how
   to determine which particles
   in the solid are thermostatted 
   and which ones are fixed.
   Difference with respect to
   version 3.3.0 is that here
   local minimization is possible
   and is achieved by cancelling
   velocity components which 
   are antiparallel to the force.
   Difference with respect to 
   version 3.2 is that, besides
   interactions in MgF2, also
   interactions between a noble 
   gas and MgF2 can be calculated.
   The difference with respect 
   to version 0.x and 1.x is that
   electrostatic interactions between
   charged particles are calculated by means
   of Wolf's method, modified by Fennell and 
   Gezelter (J. Chem. Phys. Vol. 124, page 234104 (2006)). 
   Furthermore, the way how to calculate the 
   coefficients of the 5-th order join polynomial
   that smoothly brings the potential to 0 outside
   the cutoff has been changed with respect to 
   versions 1.x.
   Difference with respect to versions 2.x
   is that here a deposition routine is 
   implemented. (MgF2)n clusters can be 
   deposited at user's defined rate.
   Depositing clusters are introduced one by one
   with random initial x- and y-coordinates
   of their centre of mass
   and with a constant velocity normal to substrate.
   The only possible output is system configuration,
   particle velocities, and system potential energy.
   A simple thermostat based on velocity scaling
   The thermostat is applied to the bottom of slab. 
   Unless otherwise stated, lengths are 
   assumed to be expressed in Angstroms,
   masses in amu, times in seconds, and
   energies in electronvolts. */

#include "header.h"



int main()
{
  
  double *x, *y, *z,
    *vx, *vy, *vz,
    *fx, *fy, *fz;
  
  int *atom_type, /*atom_type[i]=0,1, ..., n_max_type 
		    for each i=0,1,2, ... ,natom_total-1*/
    *fixed_atom; /* fixed_atom[i]=0,1 
		    for each i=0,1,2... n_max_atom */
  
  
  int *mask_point, /* array indicating which "stripes" of mask_array 
		      are to be considered. 
		      mask_array indicates which cells interact 
		      with a given cell.
		      mask_array[i]=1 means that cell m interacts 
		      with cell m+i, 0 <= m < n_cell-i. 
		      Instead of storing mask_array in memory, 
		      the array mask_point is used.
		      Given an integer s such that 0 <= s < n_stripe-1, 
		      mask_point[2*s]=m and  mask_point[2*s+1]=n > m, 
		      means that all elements of 
		      array mask_array in the range [m,n] are equal to 1. 
		      At the same time, mask_array[m-1] and mask_array[n+1] 
		      must be equal to 0. The series of 1 in the array 
		      cell_mask between m and n is called a stripe,
		      and mask_point[2*s] and mask_point[2*s+1] are  
		      the initial and final cell index of the stripe s,
		      respectively.
		      Size of mask_point is 2*n_stripe.
		      Example:
		      Let mask_array be:
		      mask_array = {1 | 0 0 | 1 1 1 | 0 | 1 1 | 0  0 | 1  1  1 | 0};
		      array index   0   1 2   3 4 5   6   7 8   9 10  11 12 13  14 
		      stripe index  0           1          2             3        
		      then the array mask_pointer will look like:
		      mask_pointer = {0 0 | 3 5 | 7 8 | 11 13};
		      stripe index     0     1     2      3 
		      where the | marks the boundary of each stripe
		   */ 
    *cell_point, /* mask_point[m+1]-mask_point[m] = n <==> 
		    cell m contains n particles, 0 <= m < n_cell-1. 
		    size of array is n_cell+1 */
    *cell_list; /* array storing lists of atoms in each cell. Size is n_tot_atom.
		   Every time when number of particle in the system changes,
		   array cell_list must be resized */
  
  
  read_input("input_file.inpt", "atom_params_file.inpt");


  set_potentials();
 

  /* read types and configuration 
     of clusters to be
     deposited */
  n_deposit_clust = 
    read_dep_clust_input(&deposit_clust,
			 "dep_clust_file.inpt");
  
  /* initialise seed of random number generator */
  set_seed(random_number_generator_seed);

  /* assign initial number of particles */
  if ( initial_positions )
    n_initial_atom = 
      read_initial_part_num(initial_positions_file);
  else {
    printf("\n%s\n%s\n%s\n\n",
	   "Error in main!",
	   "Cannot initialize particle number",
	   "This program is exiting, sorry!");
    exit(1);
  }
  
  
  int deposit_clust_max_size = 0;
  for (int i=0; i<n_deposit_clust; i++)
    deposit_clust_max_size = 
      deposit_clust[i]->unit_num > deposit_clust_max_size ?
      deposit_clust[i]->unit_num : deposit_clust_max_size;
  
  
  initialize_arrays(n_initial_atom,
		    single_adsorbate,
		    &n_max_atom,
		    n_step,
		    step_per_atom_release,
		    deposit_clust_max_size,
		    &x, &y, &z,
		    &vx, &vy, &vz,
		    &fx, &fy, &fz,
		    &atom_type,
		    &fixed_atom);
  
  

  /* initialise all elements of atom_type to -1 */
printf("Maximum number of atoms allowed in simulation cell = %d", n_max_atom) ;
  for (int i=0; i<n_max_atom; i++)
    atom_type[i] = -1;
  
  
  /*Assign atoms with initial positions*/
  
  if ( initial_positions ) { /*start from given initial configuration */
    /* In case of restart from initial configuration, 
       the initial configuration file has to be a xyz file with 
       the first line bearing the total number of atoms, and the 
       second line displaying the length of the cell 
       in each direction. */
    
    read_initial_configuration(initial_positions_file, 
			       &n_initial_atom,
			       n_species, 
			       n_atom_type,  
			       atom_type, 
			       element_name,
			       &min_z,
			       //&n_cellx, &n_celly, &n_cellz, 
			       &boxx, &boxy, &boxz, 
			       x, y, z); 

   
  } // end of if ( initial_positions )

  else { /*let program initialise atom positions*/
    
    
    
    
  } // end of if (initial_positions) {...} else {...}
  
  
  
  /*initialise total number of atoms in the 
    system to number of atoms initially found */
  n_tot_atom = n_initial_atom;
  
  
  /* set simulation box dimensions 
     equal to values input by user, if 
     option set_box_dimensions is 1 */
  if ( set_box_dimensions ) {
    boxx = boxx_input;
    boxy = boxy_input;
    boxz = boxz_input;
  }
  
  
  /* In order for the minimum image convention to make sense,
     the length of each box edge has to be at least twice
     as large as the potential range  */
  for (int i=0; i<n_species; i++)
    for (int j=0; j<n_species; j++)
      if ( boxx/2.0 <= potential_range[i][j] || 
	   boxy/2.0 <= potential_range[i][j] ) {
	printf("%s\n%s\n%s%.2lf, %.2lf%s\n%s%.2lf%s\n%s\n%s\n",
	       "Error in function main().",
	       "An irregularity has been found in your input set.",
	       "The x and y box dimensions ", boxx, boxy, 
	       " Angstr, are not compatible",
	       "with the provided potential range of ", 
	       potential_range[i][j], " Angstr.",
	       "Please, increase the length of the cell or reduce the interaction range.",
	       "This programme is exiting, sorry!");
	exit(1);
      }
  
  


  
  /*check for number of atoms not to exceed*/
  /*maximum allowed atom number*/
  if (n_tot_atom > n_max_atom) {
    printf("%s\n%s\n%s%d%s\n%s%d%s\n%s\n%s\n",
	   "Error in function main().",
	   "An irregularity has been found in your input set.",
	   "The total number of atoms ",  n_tot_atom, " is greater than",
	   "the maximum allowed number of atoms ", n_max_atom, ".",
	   "Please, decrease the number of atoms.",
	   "This programme is exiting, sorry!");
    exit(1);
  }
  
  

   /* set vertical dimension of simulation unit cell
      to 10 times the initial distance between an 
      impinging atom and the substrate surface */
  if ( !initial_positions )
    boxz = 25.0*initial_atom_surface_distance;
  
  
  /* initialize structure */
  set_initial_structure(n_tot_atom,
			atom_type,
			initial_struct_z_translation,
			species_bond_distance,
			boxx, boxy, boxz,
			//&min_x, &min_y, 
			&min_z,
			&substrate_current_height,
			&init_substrate_surface_z_coord,
			x, y, z);


  slab_thickness = substrate_current_height - min_z;
  init_slab_thickness = 
    init_substrate_surface_z_coord - min_z;
  max_z = boxz + min_z;
  
  /* assign coordinates of edge of simulation
     box belonging to first cell in cell list */
  min_x_box_corner = -boxx/2.0;
  min_y_box_corner = -boxy/2.0;
  min_z_box_corner = min_z;
  
  /* Find portion of substrate where
     thermostat applies.
     Thermostat applies to all particles with
     z < z_slab_thermostat. */
  z_slab_thermostat = 
    find_z_thermostat(n_tot_atom, 
		      z_slab_thermostat,
		      init_slab_thickness,
		      min_z,
		      z);  
  if ( z_slab_thermostat <= min_z ) 
    printf("\n%s\n%s\n%s%.3lf%s\n%s\n%s%.3lf%s\n%s\n\n",
	   "Warning issued by main!",
	   "The uppermost z-coordinate of",
	   "thermostatted atoms was found to be ",
	   z_slab_thermostat, ".",
	   "Lowermost point in the structure is",
	   "at z=", min_z, ".",
	   "No particle is affected by the thermostat.");
  
  
  /* Find z coordinate below which 
     atoms are kept fixed  */
  z_fix_atom = 
    find_z_fix_atom(n_tot_atom,
		    z_fix_atom,
		    init_slab_thickness,
		    min_z, z);
  if ( z_fix_atom <= min_z ) 
    printf("\n%s\n%s\n%s%.3lf%s\n%s\n%s%.3lf%s\n%s\n\n",
	   "Warning issued by main!",
	   "The uppermost z-coordinate of",
	   "fixed atoms was found to be ",
	   z_fix_atom, ".",
	   "Lowermost point in the structure is",
	   "at z=", min_z, ".",
	   "No particle in the system is fixed.");
  if ( z_fix_atom > z_slab_thermostat-1.0e-3 ) 
    printf("\n%s\n%s\n%s%.3lf%s\n%s\n%s%.3lf%s\n%s\n\n",
	   "Warning issued by main!",
	   "The uppermost z-coordinate of",
	   "fixed atoms was found to be ",
	   z_fix_atom, ".",
	   "The uppermost z-coordinate of",
	   "thermostatted atoms was found to be ",
	   z_slab_thermostat, ".",
	   "No particle is affected by the thermostat.");
  
  /* Assign fixed atom with a label */
  n_fix_atom = 
    label_fixed_atom(n_tot_atom,
		     z_fix_atom,
		     fixed_atom,
		     z);   
  
  
  /* allocate memory for mask pointer
     and find which cells interact
     with which */
  construct_pair_list(n_cellx, n_celly, n_cellz,
		      &n_cell,
		      boxx, boxy, boxz,
		      &n_stripe,
		      &mask_point,
		      max_pot_range);
  
  
  
  /* Assign substrate atoms with 
     initial velocities */
  if( initial_velocities ) {
    read_initial_velocities(initial_velocities_file,
			    n_tot_atom, 
			    vx, vy, vz);
    /*set to zero velocities of fixed atoms*/
    for(int i=0; i<n_tot_atom; i++) 
      if ( fixed_atom[i] ) {
	vx[i] = 0.0;
	vy[i] = 0.0;
	vz[i] = 0.0;
      }
    /* calculate system initial 
       temperature */
    calculate_temperature(n_tot_atom,
			  n_fix_atom,
			  atom_type,
			  dmas,
			  &initial_system_temperature,
			  vx, vy, vz);
  }
  else 
    /*assign substrate atoms with 
      random initial velocities*/
    init_substrate_vel(n_substrate_species, 
		       n_fix_atom,
		       fixed_atom,
		       n_atom_type, 
		       atom_type, 
		       dmas,
		       initial_system_temperature, 
		       vx, vy, vz);  
  
  system_temperature = 
    initial_system_temperature;
  

  
  /* create initial impinging atom
     or cluster if option is set to 1 */
  if ( single_adsorbate ) {
    int dep_part_num;
    introduce_new_atom(n_tot_atom,
		       n_deposit_clust,
		       deposit_clust,
		       n_tot_atom,
		       &dep_part_num,
		       n_fix_atom,
		       dmas,
		       element_name,
		       atom_type, n_atom_type,
		       van_der_waals_radius,
		       species_bond_distance,
		       initial_atom_surface_distance,
		       min_initial_atom_surface_distance,
		       &substrate_current_height,
		       initial_clust_temp,
		       boxx, boxy, boxz,
		       x, y, z,
		       vx, vy, vz);
    n_tot_atom += dep_part_num;
  }



  
  /* allocate memory for cell list arrays */
  create_cell_arrays(n_max_atom, n_cell,
		     &cell_list, &cell_point);
  
  
  /* Print initial configuration in file */
  output_configuration(n_tot_atom, current_block, 
		       current_step,
		       current_time, 
		       "initial_configuration.xyz",
		       atom_type, //atom_phase, 
		       element_name,
		       x, y, z);
  
  
  /****Preliminary MD step*****/
  
  /* initialize array storing cell lists */
  
  
  /* update cell lists for first time  */
  update_cells(n_tot_atom,
		       n_cellx, n_celly, n_cellz,
	       boxx, boxy, boxz,
	       min_x_box_corner, 
	       min_y_box_corner, 
	       min_z_box_corner,
	       cell_point,
	       cell_list,
	       x, y, z);
  
  
  
  
  /*Calculate forces in initial configuration*/
  printf("\nIn main. Calculating forces in initial configuration.\n");
   force
    (n_tot_atom,
     n_cell,
     n_stripe,
     fixed_atom,
     interaction_type,
     boxx, boxy,
     atom_type,
     &initial_pot_energy,
     rho, q, b, c, d, alpha,
     epsilon_lj, sigma_lj,
     sigma_inter, epsilon_inter,
     join_range_limits,
     join_coeff_inter,
     potential_range,
     cutoff_radius, 
     a,
     mask_point, cell_point, cell_list,
     fx, fy, fz,
     x, y, z);
  potential_energy = initial_pot_energy;
//  printf("Calculated forces in initial config, rho = %f \n", rho);
 
  
  print_header("summary.out","energies.out");
  

  /* write initial energy and temperature 
     in output file */
  if ( energy_minimisation )
    write_energy_quench("energies.out",
			current_block,
			current_step,
			current_time,
			potential_energy,
			kinetic_energy,
			system_temperature);
  else
    write_energy("energies.out");
  
  
  /*Update atom positions for first time*/
  therma(n_species, n_atom_type, 
	 atom_type, dmas,
	 fixed_atom,
	 boxx, boxy, boxz, 
	 max_z,
	 time_step,
	 current_block, 
	 current_step,
	 single_adsorbate,
	 &n_adatom_x_cross, 
	 &n_adatom_y_cross,
	 fx, fy, fz,
	 vx, vy, vz,
	 x, y, z);
  
  
  
  
  
  /*******************************************/
  /*                                         */
  /*      MD iterations start here           */
  /*                                         */
  /*******************************************/
  
  printf("\n**************************\n");
  printf("MD iterations start here\n");
  printf(  "**************************\n\n");
  
  /* declare flag for interrupting
     MD iterations in following cases:
     (i) structure 
     grows beyond upper limit of 
     simulation unit cell;
     (ii) new atom z coordinate has 
     been found to exceed upper limit 
     of cell;
     (iii) minimum temperature is 
     achieved when performing local 
     structure optimisation */
  int interrupt_run = 0;
  
  for (current_block=1; current_block<=n_block; current_block++) {
    
    for (current_step=1; current_step<=n_step; current_step++) {
      
      //printf("\n********************\n");
      //printf("Current block is %d\n",current_block);
      //printf("Current step is %lu\n",current_step);
      //printf("********************\n\n");
      
      /*calculate current system time in s*/
      current_time = current_step*time_step +
	(current_block-1)*n_step*time_step;
      
      
      /* introduce a new atom if necessary */
      if ( current_step%step_per_atom_release == 0 ) {
	int n_deposit_part;
	interrupt_run = 
	  introduce_new_atom(n_tot_atom,
			     n_deposit_clust,
			     deposit_clust,
			     n_tot_atom,
			     &n_deposit_part,
			     n_fix_atom,
			     dmas,
			     element_name,
			     atom_type, n_atom_type,
			     van_der_waals_radius,
			     species_bond_distance,
			     initial_atom_surface_distance,
			     min_initial_atom_surface_distance,
			     &substrate_current_height,
			     initial_clust_temp,
			     boxx, boxy, boxz,
			     x, y, z,
			     vx, vy, vz);
	n_tot_atom += n_deposit_part;
      }
      
      /* interrupt inner cycle if function 
	 introduce_new_atom returns integer 
	 different from 0 */
      if ( interrupt_run ){
	printf("%s%lu\n%s%d%s\n%s%d%s\n%s%d%s\n%s\n",
	       "Warning in function main at step ", current_step,
	       "block ", current_block, ".",
	       "After introducing new atom, total number of atoms is ",
	       n_tot_atom, ".",
	       "Function introduce_new_atom has returned ", interrupt_run, ".",
	       "Exiting inner MD cycle.");
	break;
      }      
  
      

      

      /* first half of velocity update */
      vel_partial_update(n_tot_atom, 
			 fixed_atom,
			 n_fix_atom,
			 atom_type,
			 dmas, time_step,   
			 fx, fy, fz,
			 &kinetic_energy,
			 &system_temperature,
			 vx, vy, vz); 
      
      /* update cell list */
      if ( update_cells
	   (n_tot_atom,
	    n_cellx, n_celly, n_cellz,
	    boxx, boxy, boxz,
	    min_x_box_corner, 
	    min_y_box_corner, 
	    min_z_box_corner,
	    cell_point,
	    cell_list,
	    x, y, z) ) {
	printf("\n%s%d%s%lu%s\n%s\n%s\n\n",
	       "Error in main at block ", current_block,
	       ", step ", current_step, ".",
	       "Function update_cells returned with an error.",
	       "Interrupting main MD cycle and terminating the run!");
	break;
      }
      
      /* calculate forces in current 
	 configuration */
      if ( force
	   (n_tot_atom,
	    n_cell,
	    n_stripe,
	    fixed_atom,
	    interaction_type,
	    boxx, boxy,
	    atom_type,
	    &potential_energy,
	    rho, q, b, c, d, alpha,
	    epsilon_lj, sigma_lj,
	    sigma_inter, epsilon_inter,
	    join_range_limits,
	    join_coeff_inter,
	    potential_range,
	    cutoff_radius, 
	    a,
	    mask_point, cell_point, cell_list,
	    fx, fy, fz,
	    x, y, z) ) {
	printf("\n%s%d%s%lu%s\n%s\n%s\n\n",
	       "Error in main at block ", current_block,
	       ", step ", current_step, ".",
	       "Function force returned with an error code.",
	       "Exiting from MD and cycle and program.");
	break;
      }

      
      /*second half of velocity update */
      vel_partial_update(n_tot_atom, 
			 fixed_atom,
			 n_fix_atom,
			 atom_type,
			 dmas, time_step,   
			 fx, fy, fz,
			 &kinetic_energy,
			 &system_temperature,
			 vx, vy, vz);
      
      if ( energy_minimisation )
	interrupt_run = 
	  vel_quench(n_tot_atom,
		     n_fix_atom,
		     atom_type,
		     dmas, 
		     fx, fy, fz,
		     &kinetic_energy,
		     &system_temperature,
		     minimisation_bottom_temperature,
		     vx, vy, vz);
      
      /* keep system temperature constant by applying 
	 thermostat based on velocity rescaling */
      if ( !energy_minimisation 
	   &&
	   current_step%thermostat_freq == 0  )
	velocity_rescaling(n_tot_atom,
			   fixed_atom,
			   //n_fix_atom,
			   atom_type, dmas,
			   substrate_thermostat_temperature,
			   z_slab_thermostat,
			   z, vx, vy, vz);
      

      
      /*Write output if needed*/
      
      /* write atom positions */
      if (current_step%config_output_freq == 0) {
	char *step = int_to_string(current_step);
	char *block = int_to_string(current_block);
	
        char output_file_name[strlen(block)+strlen(step)+11];
	strcpy(output_file_name,block);
	strcat(output_file_name,"bl_");
	strcat(output_file_name,step);
	strcat(output_file_name,"step.xyz");
	output_configuration(n_tot_atom, 
			     current_block, current_step,
			     current_time, output_file_name,
			     atom_type, element_name, 
			     x, y, z);
      }
      
      /* write atom velocities */
      if (current_step%velocity_output_freq == 0) {
	char *step = int_to_string(current_step);
	char *block = int_to_string(current_block);
	
        char output_file_name[strlen(block)+strlen(step)+15];
	strcpy(output_file_name,block);
	strcat(output_file_name,"bl_");
	strcat(output_file_name,step);
	strcat(output_file_name,"step_vel.out");
	output_velocity(n_tot_atom, output_file_name,
			atom_type, element_name,
			vx, vy, vz);
	free(block);
	free(step);
      }
      

      /* write energies */
      if (current_step%energy_output_freq == 0)
	if ( energy_minimisation )
	  write_energy_quench("energies.out",
			      current_block,
			      current_step,
			      current_time,
			      potential_energy,
			      kinetic_energy,
			      system_temperature);
	else
	  write_energy("energies.out");
      
      if ( single_adsorbate 
	   &&
	   current_step%adatom_position_freq == 0 ) 
	write_adatom_coords(adsorbate_position_file,
			    adsorbate_velocity_file,
			    current_block,
			    current_step,
			    current_time,
			    x[n_tot_atom-1], 
			    y[n_tot_atom-1], 
			    z[n_tot_atom-1],
			    vx[n_tot_atom-1],
			    vy[n_tot_atom-1],
			    vz[n_tot_atom-1]);
      
      /* update positions */
      therma(n_species, n_atom_type,
	     atom_type, dmas,
	     fixed_atom,
	     boxx, boxy, boxz, 
	     max_z,
	     time_step,
	     current_block, current_step,
	     single_adsorbate,
	     &n_adatom_x_cross,
	     &n_adatom_y_cross,
	     fx, fy, fz,
	     vx, vy, vz,
	     x, y, z);
      
      
    } /* end of for (current_step=1; current_step<n_step; current_step++) */
    
    /* exit outer cycle if function introduce_new_atom 
       returns integer different from 0 
       or if bottom temperature has been reached in
       case of local minimisation */
    if ( interrupt_run ) {
      if ( !energy_minimisation )
	printf("%s%lu\n%s%d%s\n%s%d%s\n%s\n",
	       "Warning in function main at step ", current_step,
	       "block ", current_block, ".",
	       "After introducing new atom, total number of atoms is ",
	       n_tot_atom, ".",
	       "Exiting outer MD cycle.");
      else 
	printf("%s%lu\n%s%d%s\n%s%.5lf%s\n%s\n",
	       "Warning in function main at step ", current_step,
	       "block ", current_block, ".",
	       "Current system temperature is  ",
	       system_temperature, " K.",
	       "Exiting outer MD cycle.");
      break;
    }
  } /* end of  for (current_block=1; current_block<=n_block; current_block++) */
  
  

  /************************************/
  /*                                  */
  /*   MD iterations terminate here   */
  /*                                  */
  /************************************/

  
  printf("\n**************************\n");
  printf("MD iterations terminate here\n");
  printf("**************************\n\n");
  
  final_system_temperature = system_temperature;
  final_pot_energy = potential_energy;
  
  
  /* output final configuration */
  output_configuration(n_tot_atom, 
		       current_block-1, current_step-1,
  		       current_time, "final_configuration.xyz",
  		       atom_type, 
		       element_name, 
		       x, y, z);
  
  /* output velocities in final configuration */
  output_velocity(n_tot_atom, "final_velocities.out",
		  atom_type, element_name, vx, vy, vz);
  
  print_summary("summary.out");
  
  
  delete_clust_array(n_deposit_clust,
		     deposit_clust);

  
  delete_pair_list(mask_point);
  delete_cell_arrays(cell_list, cell_point);

  delete_arrays(x, y, z,
		vx, vy, vz,
		fx, fy, fz,
		atom_type,
		fixed_atom);
  
  return(0);  
  
}
