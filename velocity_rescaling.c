/* This function rescales the velocities of those atoms
   which are affected by the thermostat, in such a way
   to bring the system temperature to target value.
   The thermostat is applied to the bottom part of
   the substrate. In particular all points with 
   z < lower_height are affected by the thermostat. 
   The temperature of the thermostat is target_thermostat_temp */

#include "header.h"

int velocity_rescaling(//int num_species,
		       //int n_per_plane,  int n_fixed_plane, 
		       //int *atoms_per_species,
		       int atom_number,
		       int *fix_atom_label,
		       //int fix_atom_number,		       
		       int *atom_species, 
		       double *atom_mass,
		       double target_thermostat_temp,  
		       double thermostat_area_upper_boundary,
		       double *z_coord,
		       double *x_vel, double *y_vel, double *z_vel)
{

  /* find number of species making up the surface*/
  //int num_substrate_species = num_species - num_gas_species;

  /* find total number of atoms */
  //int num_tot_atom = 0;
  //for(int i=0; i<num_species; i++)
  // num_tot_atom += atoms_per_species[i];

  /* find number of atoms which are kept fixed */
  //int n_fixed_atom = n_per_plane*n_fixed_plane;
  
  /* create array where atoms subject to thermostat are stored */
  int *atom_in_thermostat = malloc(atom_number*sizeof(int));
  if (atom_in_thermostat == NULL ) {
    printf("%s\n%s\n%s\n%s\n",
	   "Fatal error in function velocity_rescaling.",
	   "The int pointer atom_in_thermostat points to null",
	   "after being assigned. Shortage of memory.",
	   "It is not possible to continue, sorry!");
    exit(1);
  }
  /* initialise all elements of array  
     atom_in_thermostat to -1 */
  memset(atom_in_thermostat, -1, sizeof(int)*atom_number);
  
  
  
  double current_kinetic_energy = 0.0, 
    current_temp,
    scaling_factor;
  
  int thermostatted_atom_num = 0;
  
  
  /* calculate kinetic energy of atoms in the thermostat affected areas */
  current_kinetic_energy = 0.0;
  for(int i=0; i<atom_number; i++) 
    if ( (z_coord[i] < thermostat_area_upper_boundary) &&
	 !fix_atom_label[i] ) {
	 //(i >= fix_atom_number) ) {
      current_kinetic_energy += atom_mass[atom_species[i]] * 
	(x_vel[i]*x_vel[i] + y_vel[i]*y_vel[i] + z_vel[i]*z_vel[i]);
      atom_in_thermostat[thermostatted_atom_num++] = i;
    }
    

  double conversion_factor = kg_per_amu*metre_per_angstrom*
    metre_per_angstrom/joule_per_ev;
  /* calculate temperature of atoms in thermostatted areas 
     Ekin = (3/2)NkT  =>  T = (2/3)E_kin/(Nk).
     Kinetic energy is initially expressed in a.m.u.*Angstr^2/s^2.
     After multplying by factor of kg_per_amu*(metre_per_angstrom)^2,
     kinetic energy is expressed in joule. Finally, after dividing by
     joule_per_ev, kinetic energy is expressed in eV */
  if ( thermostatted_atom_num > 0 )
    current_temp = conversion_factor*current_kinetic_energy/
      (3.0*thermostatted_atom_num*kb);
  else if ( thermostatted_atom_num < 0 ) {
    printf("%s\n%s\n%s%d%s\n%s\n",
	   "Error in function velocity_rescaling.",
	   "The number of slab atoms affected by the thermostat",
	   "has been found to be ", thermostatted_atom_num, ".",
	   "It is impossible to continue, sorry!");
    exit(1); 
  }
  

  /* calculate velocity rescaling factors */
  if ( ( thermostatted_atom_num > 0 )  &&  
       (fabs(current_temp) > 1.e-20) )
    scaling_factor = sqrt(target_thermostat_temp/current_temp);
  else {
    //printf("%s\n%s\n%s%.2e.\n%s\n",
    //	   "Warning in function rescale_velocity.",
    //   "Current temperature at bottom part of unit cell",
    //   "has been found to be ", current_lower_temp,
    //   "Velocity rescaling is impossible.");
    scaling_factor = 1.0;
  }


  /* rescale velocities of atoms lying in area 
     where thermostat applies */
  for (int i=0; i<thermostatted_atom_num; i++) {
    x_vel[atom_in_thermostat[i]] *= scaling_factor;
    y_vel[atom_in_thermostat[i]] *= scaling_factor;
    z_vel[atom_in_thermostat[i]] *= scaling_factor;
  }
  
  
  
  free(atom_in_thermostat);
  
  
  return 0;
}
