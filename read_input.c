#include "header.h"
#include <ctype.h>

int read_input(char *input_file_name, 
	       char *atom_params_input_file_name)
{
  FILE *p_input_file, *p_atom_file;
  
  char line[100][200];
  int line_cnt;
  char* token[100]; 
  
  if ( (p_input_file=fopen(input_file_name,"r")) == NULL )
    {
      printf("A fatal error occured while opening file %s.\n",
	     input_file_name);
      printf("Cannot continue, sorry!\n");
      exit(1);
    }


  /* read input file line by line and store 
     each line in buffer strings */
  line_cnt = 0;
  while ( fgets(line[line_cnt],200,p_input_file) != NULL ) 
    line_cnt++;
  
  fclose(p_input_file);
   
   
   
  line_cnt = 0;
  
  /*read number of blocks and number of steps per block*/
  break_line(line[line_cnt++], token);
  n_block = atoi(token[0]);
  n_step = atoi(token[1]);
  printf("Read numb of bloks and numb of steps per block: %d, %lu\n",
	 n_block, n_step);
  
  
  /* read energy output frequency */
  break_line(line[line_cnt++], token);
  energy_output_freq = atoi(token[0]);
  //temperature_output_freq = atoi(token[1]);
  //layer_output_freq = atoi(token[2]);
  printf("Read energy output freq: %d\n",
	 energy_output_freq);
  if ( energy_output_freq <= 0 ) {
    printf("%s\n%s%d\n%s\n%s\n%s\n",
	   "Error found by function read_input in your input set.",
	   "Invalid value ", energy_output_freq,
	   " provided for energy or temperature output frequency.",
	   "These parameters must be greater than 0.",
	   "Cannot continue, sorry!");
    exit(1);
  }
  
  
  /*read configuration and velocity output frequency */
  break_line(line[line_cnt++], token);
  config_output_freq = atoi(token[0]);
  velocity_output_freq = atoi(token[1]);
  printf("Read config and velocity output frequency: %lu, %d\n",
	 config_output_freq, velocity_output_freq);
  if ( config_output_freq <= 0 ) {
    printf("%s\n%s%lu%s\n%s\n%s\n",
	   "Error found by function read_input in your input set.",
	   "Invalid value ", config_output_freq, 
	   " provided for position output frequency.",
	   "This parameter must be greater than 0.",
	   "Cannot continue, sorry!");
    exit(1);
  }
  if ( velocity_output_freq <= 0 ){
    printf("%s\n%s%d%s\n%s\n%s\n",
	   "Error found by function read_input in your input set.",
	   "Invalid value ", velocity_output_freq, 
	   " provided for velocity output frequency.",
	   "This parameter must be greater than 0.",
	   "Cannot continue, sorry!");
    exit(1);
  }
  


  /* read number of relaxation steps and 
     output frequency during relaxation */
  break_line(line[line_cnt++], token);
  n_relax_step = atoi(token[0]);
  relax_output_freq = atoi(token[1]);
  printf("Read iteration number and output frequency during relaxation: %d, %d\n",
	 n_relax_step,relax_output_freq);
  if ( n_relax_step > 0  &&  relax_output_freq <= 0 ) {
    printf("%s\n%s%d%s\n%s\n%s\n",
	   "Error found by function read_input in your input set.",
	   "Invalid value ", relax_output_freq, 
	   " provided for output frequency",
	   "during relaxation. This parameter must be greater than 0.",
	   "Cannot continue, sorry!");
    exit(1);
  }
  
  /* read time step duration */
  break_line(line[line_cnt++], token);
  time_step = atof(token[0]);
  printf("Read time step: %6.4e s\n", time_step);
  
  
  /* read initial substrate temperature 
     and thermostat temperature */
  break_line(line[line_cnt++], token);
  initial_system_temperature = atof(token[0]);
  substrate_thermostat_temperature = atof(token[1]);
  printf("Read initial system temperature and thermostat temp/K: %8.4lf %8.4lf\n",
	 initial_system_temperature,  
	 substrate_thermostat_temperature);


  /* read thermostat frequency */
  break_line(line[line_cnt++], token);
  thermostat_freq  = atoi(token[0]);
  printf("Read thermostat frequency: %d\n",thermostat_freq);

  /* read fraction of slab where thermostat applies */
  break_line(line[line_cnt++], token);
  z_slab_thermostat = atof(token[0]);
  assert(z_slab_thermostat >= 0 );//&& z_slab_thermostat <=1);
  printf("Read fraction of slab where thermostat applies: %f\n", 
	 z_slab_thermostat);


  /* read distance between a newly introduced atom
     and topmost substrate plane and minimum distance
     allowed in order for a new atom to be released */
  break_line(line[line_cnt++], token);
  initial_atom_surface_distance = atof(token[0]);
  printf("Read initial distance between atoms and surface: %.3lf Angstr\n",
	 initial_atom_surface_distance);
  min_initial_atom_surface_distance = atof(token[1]);
  printf("Read minimum initial distance between atoms and surface: %.3lf Angstr\n",
	 min_initial_atom_surface_distance);

  /* read initial temperature of 
     newly introduced cluster,
     temperature is expressed in K */
  break_line(line[line_cnt++], token);
  initial_clust_temp = atof(token[0]);
  printf("Read initial depositing cluster temperature: %.3lf K\n",
	 initial_clust_temp);
  
  /* read rate of introduction of 
     impinging particles in 1/s */
  break_line(line[line_cnt++], token);
  impingement_rate = atof(token[0]);
  printf("Read rate of creation of new particles: %.3e/s\n",
	 impingement_rate);
  /* calculate the number of steps between two 
     consecutive atom  depositions */
  step_per_atom_release = 
    anint(1.0/(impingement_rate*time_step));
  /* make sure rate is not too high 
     compared to time step duration */
  if ( step_per_atom_release <= 0 ) {
    printf("%s\n%s%.2e%s\n%s\n%s%.2e%s\n%s%lu%s\n%s\n",
	   "An inconsistency in input data set has been detected.",
	   "The incoming particle impingement rate of ", 
	   impingement_rate, "/s",
	   "is either invalid or too high a value as compared to",
	   "MD step duration of ", 
	   time_step, " s.",
	   "Calculated number of steps per atom release is ", 
	   step_per_atom_release, ".",
	   "Cannot continue, sorry!");
    exit(1);
  }
  /* calculate time interval between two 
     consecutive adatom releases */
  sec_per_atom_release = (double)step_per_atom_release*time_step;
  printf("Calculated time between consecutive atom release: %.3e\n",
	 sec_per_atom_release);
  /* recalculate rate of impinging particles */
  impingement_rate = 1.0/sec_per_atom_release;
  //impingement_rate = 1.0/((double)step_per_atom_release*time_step);


  /* read seed of random number generator */
  break_line(line[line_cnt++], token);
  random_number_generator_seed = atoi(token[0]);
  printf("Read seed of random number generator: %d\n",
	 random_number_generator_seed);

  /* read option for setting box dimension
     equal to value input by user */
  break_line(line[line_cnt++], token);
  set_box_dimensions = atoi(token[0]);
  printf("Read option for inputting box_dimensions: %d\n",
	 set_box_dimensions);
  /* in case option is set to 1, box dimensions
     input by the user are read and used 
     box dimensions must be expressed 
     in Angstroms */
  if ( set_box_dimensions ) {
    boxx_input = atof(token[1]);
    boxy_input = atof(token[2]);
    boxz_input = atof(token[3]);
    printf("Read box dimensions: %.3lf, %.3lf, %.3lf\n",
	   boxx_input, boxy_input, boxz_input);
  }

  /* read option for local minimisation 
     and temperature below which 
     optimisation stops */
  break_line(line[line_cnt++], token);
  energy_minimisation = atoi(token[0]);
  printf("Read option for local optimisation: %d\n",
	 energy_minimisation);
  if ( energy_minimisation ) {
    minimisation_bottom_temperature =
      atof(token[1]);
    printf("Read bottom temp for miminisation/K: %.3lf\n",
	   minimisation_bottom_temperature);
  }

  /*read restart from input file*/
  break_line(line[line_cnt++], token);
  initial_positions = atoi(token[0]);
  printf("Read option for restart from given configuration: %d\n",
	 initial_positions);
  if( initial_positions ) {
    strncpy(initial_positions_file, token[1], 100); 
    printf("Read restart file: %s\n",initial_positions_file);
  }
  
  /*read restart with assigned velocities*/
  break_line(line[line_cnt++], token);
  initial_velocities = atoi(token[0]);
  printf("Read option for restart with given velocities: %d\n",
	 initial_velocities);
  if( initial_velocities ) {
    strncpy(initial_velocities_file, token[1], 100); 
    printf("Read initial velocity file: %s\n",initial_velocities_file); 
  }


  /* read single atom deposition run */
  break_line(line[line_cnt++], token);
  single_adsorbate = atoi(token[0]);
  printf("Read option for run with single adsorbate: %d\n",
	 single_adsorbate);
  if ( single_adsorbate ) {
    adatom_position_freq = atoi(token[1]);
    printf("Read step per adsorbate position output: %d\n",
	   adatom_position_freq);
    strncpy(adsorbate_position_file, token[2], 100);
    printf("Read adsorbate position file: %s\n",
	   adsorbate_position_file);
    strncpy(adsorbate_velocity_file, token[3], 100);
    printf("Read adsorbate velocity file: %s\n",
	   adsorbate_velocity_file);
  }


 
  





  /****************************************************************/ 
  /****************************************************************/
  /****************************************************************/
  
  
  

  
  /*read atom related parameters*/
  
  if ( (p_atom_file=fopen(atom_params_input_file_name,"r")) == NULL )
    {
      printf("A fatal error occured while opening file %s.\n",
	     atom_params_input_file_name);
      printf("Cannot continue, sorry!\n");
      exit(1);
    }

  /* read input file line by line 
     and store each line 
     in buffer strings */
  line_cnt = 0;
  while ( fgets(line[line_cnt],200,p_atom_file) != NULL ) 
    line_cnt++;

  fclose(p_atom_file);
  
  line_cnt = 0;
  
  /* read element names */
  break_line(line[line_cnt++], token);
  int atomic_species = 0;
  while ( (strlen(token[atomic_species]) == 2   ||
	   strlen(token[atomic_species]) == 1)  &&
	  (isupper(*token[atomic_species])) )  {
    strncpy(element_name[atomic_species], 
	    token[atomic_species], 2);
    element_name[atomic_species][2] = '\0';
    printf("Read element_name[%d]: %s\n",
	   atomic_species, element_name[atomic_species]);
    ++atomic_species;
  }


  /* read number of all species 
     and number of gas species */
  break_line(line[line_cnt++], token);
  n_species = atoi(token[0]);
  assert( n_species > 0 );
  printf("Read number of species: %d\n", n_species);
  n_gas_species = atoi(token[1]);
  assert( n_gas_species > -1 );
  printf("Read number of gas species: %d\n", n_gas_species);
  if ( n_species != atomic_species ) {
    printf("%s\n%s%d\n%s%d\n%s\n",
	   "An inconsistency in input data set has been detected",
	   "The number of element provided is ", atomic_species,
	   "whereas the number of atom species is ", n_species,
	   "Cannot continue, sorry!");
    exit(1);
  }
  if ( n_species < n_gas_species ) {
    printf("%s.\n%s%d\n%s%d.\n%s\n",
	   "An inconsistency in input data set has been detected",
	   "The number of species in gas phase ", n_gas_species,
	   "cannot be greater than the total number of species ", 
	   n_species,
	   "Cannot continue, sorry!");
    exit(1);
  }
  /* calculate number of species making up the substrate */
  n_substrate_species = n_species - n_gas_species; 


  /* read number of cells 
     the simulation box is divided into  */
  break_line(line[line_cnt++], token);
  n_cellx = atoi(token[0]);
  n_celly = atoi(token[1]);
  n_cellz = atoi(token[2]);
  assert(n_cellx > 0);
  assert(n_celly > 0);
  assert(n_cellz > 0);
  printf("Read number of slab unit cells: %d, %d, %d\n",
	 n_cellx,n_celly,n_cellz);

  

  /* read fraction of slab which is kept fixed */
  break_line(line[line_cnt++], token);
  z_fix_atom = atof(token[0]);
  assert( z_fix_atom>=0.0 && z_fix_atom<=1.0 );
  /* make sure fraction of thermostatted slab
     is greater than fraction of frozen slab */
  if ( z_fix_atom >  z_slab_thermostat ) {
    printf("\n%s\n%s\n%s\n%s%.3lf%s\n%s%.3lf%s\n%s\n",
	   "Warning in function read_input!",
	   "Inconsistency found in your input set.",
	   "Fraction of slab affected by",
	   "the thermostat is: ", z_slab_thermostat,
	   ", ", "whereas fraction of frozen slab is ",
	   z_fix_atom, ".",
	   "The thermostat will have no effect.");
    exit(1);
  }
    
  
  /* read option for vertically translating initial slab */
  break_line(line[line_cnt++], token);
  initial_struct_z_translation = atoi(token[0]);
  printf("Read option for translating slab along z: %d\n",
	   initial_struct_z_translation);
  assert( initial_struct_z_translation > -1 && 
	  initial_struct_z_translation < 2 );
  

  /* read option for directly inputting 
     z-position of substrate surface
     at the beginning of the deposition.
     If z-coordinate is not specified
     by the user, it is assumed to be 0 
     by default. Please mind the fact that,
     when the structure is initially translated
     vertically, the z-coordination of 
     the substrate surface must be always
     specified. */
  break_line(line[line_cnt++], token);
  int input_initial_z_surface_pos = 
    atoi(token[0]);
  assert( input_initial_z_surface_pos > -1 
	  &&
	  input_initial_z_surface_pos < 2 );
  if (input_initial_z_surface_pos) 
    init_substrate_surface_z_coord = 
      atof(token[1]);
  else {
    init_substrate_surface_z_coord = 0.0;
    if ( initial_struct_z_translation ) {
      printf("\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n\n",
	     "An inconsistency in your input data set was",
	     "dected by function read_input!",
	     "If the initial structure is translated",
	     "in the vertical direction, then the user",
	     "must input the z-coordinate of the substrate",
	     "at the beginning of the deposition.",
	     "It is impossible to continue, sorry!");
      exit(1);
    }
  }



  /* read mass number of elements */
  break_line(line[line_cnt++],token);
  for(int j=0; j<n_species; j++){
    dmas[j] = atof(token[j]);
    printf("Read mass number of element %d: %6.4f a.m.u.\n",
	   j,dmas[j]);
  }  
  
  /* read Van der Waals radius
     of elements in Angstroms */
  break_line(line[line_cnt++],token);
  for(int j=0; j<n_species; j++){
    van_der_waals_radius[j] = atof(token[j]);
    printf("Read Van der Waals radius of element %d: %6.4f Angstr\n",
	   j,van_der_waals_radius[j]);
  } 

  /* read bond distance between atoms 
     of species A and B, expressed in  
     units of the sum of the Van der Waals  
     radii of species A and species B  */
  for(int j=0; j<n_species; j++){
    break_line(line[line_cnt++],token);
    for(int k=j; k<n_species; k++) {
      species_bond_distance[j][k] = atof(token[k-j]);
      printf("Read parameter species_bond_dist%d%d: %.2lf\n",
	     j,k,species_bond_distance[j][k]);
    }
  }
  /* express bond distances in Angstroms */
  for(int j=0; j<n_species; j++)
    for(int k=j; k<n_species; k++) {
      species_bond_distance[j][k] *=  
	(van_der_waals_radius[j] + van_der_waals_radius[k]);
      species_bond_distance[k][j] = species_bond_distance[j][k];
    }
  


  
  /* Read parameters for interaction 
     between substrate atoms. 
     Born-Mayer potential is assumed.
     Parameters to assign are: rho, 
     qi, qj, b_ij, c_ij, d_ij, 
     and alpha_ij. */
  
  /* read rho in Angstroms  */
  break_line(line[line_cnt++],token);
  rho = atof(token[0]);

  /* read ion charges in units 
     of e (electron charge) */
  break_line(line[line_cnt++],token);
  for (int i=0; i<n_substrate_species; i++) {
    q[i] = atof(token[i]);
    printf("Read q[%d]: %.3lf e\n", i, q[i]);
  }
  
 
  /* read b_ij coefficient in eV */
  for(int j=0; j<n_substrate_species; j++) {
    break_line(line[line_cnt++],token);
    for(int k=j; k<n_substrate_species; k++) {
      b[j][k] = atof(token[k-j]);
      printf("Read b[%d][%d]: %.3lf eV\n", 
	     j,k,b[j][k]);
      b[k][j] = b[j][k];
      
    }
  }
  
  /* read c_ij coefficient in eV*Angstr^12 */
  for(int j=0; j<n_substrate_species; j++) {
    break_line(line[line_cnt++],token);
    for(int k=j; k<n_substrate_species; k++) {
      c[j][k] = atof(token[k-j]);
      printf("Read c[%d][%d]: %.3lf eV\n", 
	     j,k,c[j][k]); 
      c[k][j] = c[j][k];
    }
  }
    
  /* read d_ij coefficient in eV*Angstr^6 */
  for(int j=0; j<n_substrate_species; j++) {
    break_line(line[line_cnt++],token);
    for(int k=j; k<n_substrate_species; k++) {
      d[j][k] = atof(token[k-j]);
      printf("Read d[%d][%d]: %.3lf eV\n", 
	     j,k,d[j][k]);
      d[k][j] = d[j][k];
    }
  }

  /* read alpha coefficient in Angstr^-1 */
  for(int j=0; j<n_substrate_species; j++) {
    break_line(line[line_cnt++],token);
    for(int k=j; k<n_substrate_species; k++) {
      alpha[j][k] = atof(token[k-j]);
      printf("Read alpha[%d][%d]: %.3lf/Angstr\n", 
	     j,k,alpha[j][k]);
      alpha[k][j] = alpha[j][k];
    }
  }
  
 
 
  
  /* read Lennard Jones parameters for
     interactions between depositing atoms */
  
  /* read epsilon Lennard Jones in eV */
  for(int j=n_substrate_species; j<n_species; j++){
    break_line(line[line_cnt++],token);
    for(int k=j; k<n_species; k++){
      epsilon_lj[j][k] = atof(token[k-j]);
      printf("Read parameter epsilon_lj_%d%d: %.2lf\n",
	     j,k,epsilon_lj[j][k]);
    } 
  }
  
  /*read sigma Lennard Jones in Angstrom */
  for(int j=n_substrate_species; j<n_species; j++){
    break_line(line[line_cnt++],token);
    for(int k=j; k<n_species; k++) {
      sigma_lj[j][k] = atof(token[k-j]);
      printf("Read parameter sigma_lj_%d%d: %.2lf\n",
	     j,k,sigma_lj[j][k]);
    }
  }


  /* read parameters for 10-5 mixed  interactions */
  
  /* read epsilon for 10-5 interaction in eV */
  for(int j=n_substrate_species; j<n_species; j++){
    break_line(line[line_cnt++],token);
    for(int k=0; k<n_substrate_species; k++){
      epsilon_inter[k][j] = atof(token[k]);
      printf("Read parameter epsilon_inter_%d%d: %.2lf\n",
	     k,j,epsilon_inter[k][j]);
    } 
  }
  
  /*read sigma for 10-5 interaction in Angstrom */
  for(int j=n_substrate_species; j<n_species; j++){
    break_line(line[line_cnt++],token);
    for(int k=0; k<n_substrate_species; k++) {
      sigma_inter[k][j] = atof(token[k]);
      printf("Read parameter sigma_inter_%d%d: %.2lf\n",
	     k,j,sigma_inter[k][j]);
    }
  }
  
  
  /* read cutoff radii in Angstroms */
  for(int j=0; j<n_species; j++){
    break_line(line[line_cnt++],token);
    for(int k=j; k<n_species; k++) {
      cutoff_radius[j][k] = atof(token[k-j]);
      printf("Read cutoff_radius_%d%d: %.2lf\n",
	     j,k,cutoff_radius[j][k]);
    }
  }
  

  /* read potential ranges in Angstr */
  for(int j=0; j<n_species; j++){
    break_line(line[line_cnt++],token);
    for(int k=j; k<n_species; k++) {
      potential_range[j][k] = atof(token[k-j]);
      printf("Read potential_range_%d%d: %.2lf\n",
	     j,k,potential_range[j][k]);
    }
  }
  
    
  
  for(int j=0; j<n_species; j++) 
    for(int k=0; k<j; k++) {
      epsilon_lj[j][k] = epsilon_lj[k][j];
      sigma_lj[j][k] = sigma_lj[k][j];
      epsilon_inter[j][k] = epsilon_inter[k][j];
      sigma_inter[j][k] = sigma_inter[k][j];
      cutoff_radius[j][k] = cutoff_radius[k][j];
      potential_range[j][k] = potential_range[k][j];
    }
  
  
  
  
  
  return 0;
}








/****************************************************************/ 
/****************************************************************/
/****************************************************************/

  

int read_initial_part_num(char *position_input_file)
{
  FILE *p_input_file;
  
  char line[200];
  char* token[100]; 
  
  int particle_num = 0;
  
  if ( (p_input_file=fopen(position_input_file,"r")) == NULL )
    {
      printf("A fatal error occured while opening file %s.\n",
	     position_input_file);
      printf("Cannot continue, sorry!\n");
      exit(1);
    }
  
  
  /* read first line of input file */
  fgets(line,200,p_input_file); 
 
  fclose(p_input_file);
  
  /* break buffer string into tokens 
     and assign integer in first
     token to initial number
     of particles */
  break_line(line, token);
  particle_num = atoi(token[0]);
  printf("\nRead initial numb of particles: %d\n",
	 particle_num);

  return particle_num;

}
