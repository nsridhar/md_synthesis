#include "header.h"


/*static int seed;*/

/* double ran2()  */
/* {  */
/*    //cout << "calling ran2 with argument = " << seed << endl; */
/*   const int IM1=2147483563, IM2=2147483399; */
/*   const int IA1=40014, IA2=40692, IQ1=53668, IQ2=52774; */
/*   const int IR1=12211, IR2=3791, NTAB=32, IMM1=IM1-1; */
/*   const int NDIV=1+IMM1/NTAB; */
/*   const double EPS=3.0e-16, RNMX=1.0-EPS, AM=1.0/(double)IM1; */
/*   static int idum2=123456789, iy=0; */
/*   static int iv[NTAB]; */
/*   int j, k; */
/*   double temp; */

/*   if (seed <= 0) */
/*     { */
/*       seed =(seed==0 ? 1 : -seed); */
/*       idum2 = seed; */
/*       for (j=NTAB+7; j>=0; j--) */
/* 	{ */
/* 	  k = seed/IQ1; */
/* 	  seed = IA1*(seed-k*IQ1)-k*IR1; */
/* 	  if (seed < 0) seed += IM1; */
/* 	  if (j < NTAB) iv[j] = seed; */
/* 	} */
/*       iy = iv[0]; */
/*     } */
  
/*   k = seed/IQ1; */
 /*   seed = IA1*(seed-k*IQ1)-k*IR1; */
/*   if (seed < 0) seed += IM1; */
/*   k = idum2/IQ2; */
/*   idum2 = IA2*(idum2-k*IQ2)-k*IR2; */
/*   if (idum2 < 0) idum2 += IM2; */
/*   j = iy/NDIV; */
/*   iy = iv[j]-idum2; */
/*   iv[j] = seed; */
/*   if (iy < 1) iy += IMM1; */
/*   //cout << "at end of ran2 call seed is " << seed << endl; */
/*   if ((temp=AM*iy) > RNMX) return RNMX; */
/*   else return temp; */
/* } */




/* void set_init_seed(int init_seed) */
/* { */
/*   printf("entered set_init_seed. setting seed to %d\n",init_seed); */

/*   //\*pseed_to_set = init_seed; */
/*   printf("seed before function call: %d\n",seed); */
/*   seed = init_seed; */
/*   printf("set initial seed to %d\n",seed); */
/* } */



/* int get_seed() */
/* { */
/*   return seed; */
/* } */






/* #define IM1 2147483563  */
/* #define IM2 2147483399  */
/* #define AM (1.0/IM1) */
/* #define IMM1 (IM1-1) */
/* #define IA1 40014 */
/* #define IA2 40692 */
/* #define IQ1 53668 */
/* #define IQ2 52774 */
/* #define IR1 12211 */
/* #define IR2 3791  */
/* #define NTAB 32 */
/* #define NDIV (1+IMM1/NTAB)  */
/* #define EPS 1.2e-7  */
/* #define RNMX (1.0-EPS)  */


/* float ran2(long *idum) */
  
/*  /\*Long period (> 2  1018) random number generator of L'Ecuyer with Bays-Durham shuffle * \ *\/ */
/*  /\*and added safeguards. Returns a uniform random deviate between 0.0 and 1.0 *\/  */
/*  /\*(exclusive of the endpoint values). Call with idum a negative integer to initialize; *\/ */
/*  /\*thereafter, do not alter idum between successive deviates in a sequence.*\/ */
/*  /\*RNMX should approximate the largest floating value that is less than 1.*\/ */
  
/* { */
/*   int j;  */
/*   long k; */
/*   static long idum2=123456789;  */
/*   static long iy=0; */
/*   static long iv[NTAB]; */
/*   float temp;  */
  
/*   if (*idum <= 0) {                    /\*Initialize.*\/ */
/*     if (-(*idum) < 1) *idum=1;         /\*Be sure to prevent idum = 0.*\/ */
/*     else *idum = -(*idum);  */
/*     idum2=(*idum);  */
/*     for (j=NTAB+7;j>=0;j--) {          /\*Load the shuffle table (after 8 warm-ups).*\/ */
/*       k=(*idum)/IQ1; */
/*       *idum=IA1*(*idum-k*IQ1)-k*IR1; */
/*       if (*idum < 0) *idum += IM1; */
/*       if (j < NTAB) iv[j] = *idum;  */
/*     }  */
/*     iy=iv[0];  */
/*   } */
/*   k=(*idum)/IQ1;                       /\*Start here when not initializing.*\/ */
/*   *idum=IA1*(*idum-k*IQ1)-k*IR1;       /\*Compute idum=(IA1*idum) % IM1 without*\/ */
/*   if (*idum < 0) *idum += IM1;         /\*overflows by Schrage's method.*\/ */
/*   k=idum2/IQ2; */
/*   idum2=IA2*(idum2-k*IQ2)-k*IR2;       /\*Compute idum2=(IA2*idum) % IM2 likewise.*\/ */
/*   if (idum2 < 0) idum2 += IM2; */
/*   j=iy/NDIV;                           /\*Will be in the range 0..NTAB-1.*\/ */
/*   iy=iv[j]-idum2;                        /\*Here idum is shuffleded, idum and idum2 are*\/ */
/*   iv[j] = *idum;                        /\*combined to generate output.*\/ */
/*   if (iy < 1) iy += IMM1; */
/*   if ((temp=AM*iy) > RNMX) return RNMX; /\*Because users don't expect endpoint values.*\/ */
/*   else return temp; */
/* } */








#define IM1 2147483563
#define IM2 2147483399
#define AM (1.0/IM1)
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define NDIV (1+IMM1/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)

//static int seed = -12345;

static int seed;

float ran2()

/*Long period (> 2  1018) random number generator of L'Ecuyer 
  with Bays-Durham shuffle and added safeguards. Returns 
  a uniform random deviate between 0.0 and 1.0
  (exclusive of the endpoint values). Call with idum (here, seed) 
  a negative integer to initialize; thereafter, do not alter idum 
  between successive deviates in a sequence.
  RNMX should approximate the largest floating value 
  that is less than 1.*/
{
  //printf("Executing body of ran2() with seed of %d\n",
  // seed);
  int j;
  long k;
  static long idum2=123456789;
  static long iy=0;
  static long iv[NTAB];
  //float temp;
  double temp;
  if (seed <= 0) {                    //Initialize.
    if (-(seed) < 1) seed=1;         //Be sure to prevent idum = 0.
    else seed = -(seed);
    idum2=(seed);
    for (j=NTAB+7;j>=0;j--) {          //Load the shuffle table (after 8 warm-ups).
      k=seed/IQ1;
      seed=IA1*(seed-k*IQ1)-k*IR1;
      if (seed < 0) seed += IM1;
      if (j < NTAB) iv[j] = seed;
    }
    iy=iv[0];
  }
  k=seed/IQ1;                       //Start here when not initializing.
  seed=IA1*(seed-k*IQ1)-k*IR1;       //Compute idum=(IA1*idum) % IM1 without
  if (seed < 0) seed += IM1;         //overflows by Schrage's method.
  k=idum2/IQ2;
  idum2=IA2*(idum2-k*IQ2)-k*IR2;       //Compute idum2=(IA2*idum) % IM2 likewise.
  if (idum2 < 0) idum2 += IM2;
  j=iy/NDIV;                           //Will be in the range 0..NTAB-1.
  iy=iv[j]-idum2;                      //Here idum is shued, idum and idum2 are
  iv[j] = seed;                       //combined to generate output.
  if (iy < 1) iy += IMM1;
  if ((temp=AM*iy) > RNMX) return RNMX; //Because users don't expect endpoint values.
  else return temp;
}



/*************************************************************************************/




/* reference this function only once in main 
program to initialize seed of random number
generator. DO NOT reinitialize the seed again
during the same run!!! */
int set_seed(int seed_value)
{

  //printf("In function initialise_seed\n");
  seed = seed_value;
  //printf("After initialisation seed is %d\n",
  //	 seed);
  
  return 0;

}



/*************************************************************************************/



/* randomly select one event 
   among a set of options with
   probability prob[i] (sum_i prob[i] = 1) */
int random_choice(int size,
		  double *prob)
{
  //int choice = -1;
  
  /* generate a random number 
     between 0 and 1*/
  double random_num = ran2();
  double acc = 0.0;
  
  for (int i=0; i<size; i++) {
    if ( random_num > acc 
	 &&
	 random_num <=  acc + prob[i]  )
      return i;
    acc += prob[i];
  }
  
  return -1;
}






