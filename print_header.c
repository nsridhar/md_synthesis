#include "header.h"


int print_header(char *summary_file, char *energy_file) 
{
  FILE *p_summary_file, *p_energy_file;
  
  if ( (p_summary_file=fopen(summary_file, "w")) == NULL )
    {
      printf("Problems encountered during writing haeader to output file.\n");
      printf("A fatal error occured while opening file %s\n", summary_file);
      printf("Cannot continue, sorry!\n");
      exit(1);
    }
  
  
  fprintf(p_summary_file,"\n");
  fprintf(p_summary_file,"*************************************************************************************\n");
  fprintf(p_summary_file,"*            MD program for simulation of solid synthesis                           *\n");
  fprintf(p_summary_file,"*                     Born-Mayer version 3.3.1                                       *\n");
  fprintf(p_summary_file,"*                       Author: Nico, Sridhar                                        *\n");
  fprintf(p_summary_file,"*************************************************************************************\n\n");
  

  fprintf(p_summary_file,"%s\n", "Substrate species:");
  fprintf(p_summary_file,"%s %s %s %s\n",
	  "Chemical species", "Atom number", "Atomic mass", "VdW rad/A");
  for (int i=0; i<n_substrate_species; i++)
    fprintf(p_summary_file,"%12s %11d %11.3lf %9.3lf\n",
	    element_name[i], n_atom_type[i], 
	    dmas[i], van_der_waals_radius[i]);
  fprintf(p_summary_file,"%s\n", "Depositing species:");
  fprintf(p_summary_file,"%s %s %s %s\n",
	  "Chemical species", "Atom number", "Atomic mass", "VdW rad/A");
  for (int i=n_substrate_species; i<n_species; i++)
    fprintf(p_summary_file,"%12s %11d %11.3lf %9.3lf\n",
	    element_name[i], n_atom_type[i], 
	    dmas[i], van_der_waals_radius[i]);
  fprintf(p_summary_file, "%s\n", 
	  "Bond distance between atoms of all species / Angstr");
  for(int i=0; i<n_species; i++) {
    for(int j=i; j<n_species; j++) 
      fprintf(p_summary_file,"bond_dist_%d%d=%.4lf ",
	      i,j,species_bond_distance[i][j]);
    fprintf(p_summary_file,"\n");
  }
  fprintf(p_summary_file,"Number of atoms initially in the system: %d\n",
	  n_initial_atom);
  fprintf(p_summary_file,"Number of species making up the substrate: %d\n",
	  n_substrate_species);
  fprintf(p_summary_file,"Number of depositing species: %d\n",
	  n_gas_species);
  fprintf(p_summary_file,"Simulation box dimensions/Angstr: %.2lf, %.2lf, %.2lf\n",
	   boxx,boxy,boxz);
  fprintf(p_summary_file,"Number of cells the simulation box is divided into \nalong x, y, and z: %d, %d, %d\n",
	   n_cellx, n_celly, n_cellz);
  fprintf(p_summary_file,"Slab thickness: %.2lf Angstr\n",
          slab_thickness);
  fprintf(p_summary_file, "Vertical initial translation of slab: %d\n",
	  initial_struct_z_translation);
  fprintf(p_summary_file,"z position of substrate surface at beginning of deposition: %.3lf\n",
	  init_substrate_surface_z_coord);
  fprintf(p_summary_file, "z coordinate below which atoms are fixed: %.3lf\n",
	  z_fix_atom);
  fprintf(p_summary_file, "Number of fixed atoms: %d\n",n_fix_atom);
  fprintf(p_summary_file,"Initial temperature of impinging clusters: %.3lf eV\n",
	  initial_clust_temp);
  fprintf(p_summary_file,"Initial distance of depositing atom from substrate surface: %.3lf Angstr\n",
	  initial_atom_surface_distance);
   fprintf(p_summary_file,"Minimum initial distance of a depositing atom from substrate surface: %.3lf Angstr\n",
	  initial_atom_surface_distance);
  fprintf(p_summary_file,"Number of relaxation steps: %d\n", n_relax_step);
  fprintf(p_summary_file,"Time step duration: %.3e s\n", time_step);
  fprintf(p_summary_file,"Recalculated deposition rate: %.3e/s\n",
	  impingement_rate);
  fprintf(p_summary_file,"A new deposited atom is created every %lu MD steps\n",
	  step_per_atom_release);

  fprintf(p_summary_file,"Born-Mayer potential parameters\n");

  fprintf(p_summary_file,"%s%.3lf\n", 
	  "rho constant in Angstroms: ", 
	  rho);
  //for (int i=0; i<n_substrate_species; i++)
  //fprintf(p_summary_file,"%12s",element_name[i]);
  //fprintf(p_summary_file,"\n");
  //fprintf(p_summary_file,"%30s", "q/e");
  //for (int i=0; i<n_substrate_species; i++)
  //fprintf(p_summary_file,"%12.4lf", q[i]);
  //fprintf(p_summary_file,"\n");
  fprintf(p_summary_file,"%s", "q/e");
  for (int i=0; i<n_substrate_species; i++)
    fprintf(p_summary_file,"%s%d%s%.3lf ", 
	    "q_", i, "=", q[i]);
  fprintf(p_summary_file, "\n");
  fprintf(p_summary_file,"%s\n", "b/eV");
  for (int i=0; i<n_substrate_species; i++) {
    for (int j=i; j<n_substrate_species; j++)
      fprintf(p_summary_file,"%s%d%d%s%.3lf ", 
	      "b_", i, j, "=", b[i][j]);
    fprintf(p_summary_file,"\n");
  }
  fprintf(p_summary_file,"%s\n", "c/(ev*Angstr^12)");
  for (int i=0; i<n_substrate_species; i++) {
    for (int j=i; j<n_substrate_species; j++)
      fprintf(p_summary_file,"%s%d%d%s%.3lf ", 
	      "c_", i, j, "=", c[i][j]);
    fprintf(p_summary_file,"\n");
  }
  fprintf(p_summary_file,"%s\n", "d/(ev*Angstr^6)");
  for (int i=0; i<n_substrate_species; i++) {
    for (int j=i; j<n_substrate_species; j++)
      fprintf(p_summary_file,"%s%d%d%s%.3lf ", 
	      "d_", i, j, "=", d[i][j]);
    fprintf(p_summary_file,"\n");
  }
  fprintf(p_summary_file,"%s\n", "miu/Angstr^-2");
  for (int i=0; i<n_substrate_species; i++) {
    for (int j=i; j<n_substrate_species; j++)
      fprintf(p_summary_file,"%s%d%d%s%.3lf ", 
	      "alpha_", i, j, "=", alpha[i][j]);
    fprintf(p_summary_file,"\n");
  }
  

  fprintf(p_summary_file,"%s\n",
	  "Lennard-Jones potential parameters");
  fprintf(p_summary_file,"epsilon/ev:\n");
  for(int i=0; i<n_species; i++) {
    for(int j=i; j<n_species; j++) 
      fprintf(p_summary_file,"eps_%d%d=%.4f ",
	      i,j,epsilon_lj[i][j]);
    fprintf(p_summary_file,"\n");
  }
  fprintf(p_summary_file,"sigma/Angstrom:\n");
  for(int i=0; i<n_species; i++) {
    for(int j=i; j<n_species; j++) 
      fprintf(p_summary_file,"sigma_%d%d=%.3f  ",
	      i,j,sigma_lj[i][j]);
    fprintf(p_summary_file,"\n");
  }
  
  fprintf(p_summary_file,"%s\n",
	  "10-5 mixed interaction potential parameters");
  fprintf(p_summary_file,"epsilon/ev:\n");
  for(int i=0; i<n_species; i++) {
    for(int j=i; j<n_species; j++) 
      fprintf(p_summary_file,"eps_%d%d=%.4f ",
	      i,j,epsilon_inter[i][j]);
    fprintf(p_summary_file,"\n");
  }
  fprintf(p_summary_file,"sigma/Angstrom:\n");
  for(int i=0; i<n_species; i++) {
    for(int j=i; j<n_species; j++) 
      fprintf(p_summary_file,"sigma_%d%d=%.3f  ",
	      i,j,sigma_inter[i][j]);
    fprintf(p_summary_file,"\n");
  }
  
  fprintf(p_summary_file,"Cutoff Radii/Angstr and potential ranges/Angstr:\n");
  for(int i=0; i<n_species; i++) {
    for(int j=i; j<n_species; j++)
      fprintf(p_summary_file,"cutoff_radius_%d%d=%.3f ",
	      i,j,cutoff_radius[i][j]);
    fprintf(p_summary_file,"\n");
  }
  for(int i=0; i<n_species; i++) {
    for(int j=i; j<n_species; j++)
      fprintf(p_summary_file,"potential_range_%d%d=%.3f ",
	      i,j,potential_range[i][j]);
    fprintf(p_summary_file,"\n");
  } 
  //for(int i=0; i<n_species; i++) {
  //for(int j=i; j<n_species; j++)
  //  fprintf(p_summary_file,"verlet_radius_%d%d=%.3f ",
  //      i,j,verlet_radius[i][j]);
  //fprintf(p_summary_file,"\n");
  //}
  fprintf(p_summary_file,"Local minimisation : ");
  if ( energy_minimisation ) 
    fprintf( p_summary_file, "yes\n");
  else
    fprintf( p_summary_file, "no\n");
  fprintf(p_summary_file,"Initial structure temperature: %.3lf K\n",
	  initial_system_temperature);
  fprintf(p_summary_file,"Initial structure potential energy : %.3lf eV\n",
	   initial_pot_energy);
  if ( !energy_minimisation ) {
    fprintf(p_summary_file,"Substrate thermostat temperature: %.3lf K\n",
	    substrate_thermostat_temperature);
    fprintf(p_summary_file,"Thermostat active for atoms with z coord < %.2lf Angstr.\n",
	    z_slab_thermostat);
  }
  fprintf(p_summary_file,"Seed of random number generator: %d\n",
	  random_number_generator_seed);
  fprintf(p_summary_file,"Restart from given configuration: ");
  if ( initial_positions )
    fprintf(p_summary_file,"yes\n");
  else 
    fprintf(p_summary_file,"no\n");
  fprintf(p_summary_file,"Restart with initial velocities: ");
  if ( initial_velocities )
    fprintf(p_summary_file,"yes\n");
  else 
    fprintf(p_summary_file,"no\n");
  if ( !energy_minimisation ) {
    fprintf(p_summary_file,"Deposit atom at beginning: ");
    if ( single_adsorbate )
      fprintf(p_summary_file,"yes\n");
    else
      fprintf(p_summary_file,"no\n");
    //if ( single_adsorbate )
    //fprintf(p_summary_file, "Initial adatom coordinates: %.3lf, %.3lf, %.3lf\n",
    //      x[n_tot_atom-1], y[n_tot_atom-1], z[n_tot_atom-1]);
  }
  fprintf(p_summary_file,"\n*************************************************************************************\n");
  
  fclose(p_summary_file);
  
  
  /* print header of energy output file */
  if ( (p_energy_file=fopen(energy_file, "w")) == NULL )
    {
      printf("Problems encountered during writing haeader to output file.\n");
      printf("A fatal error occured while opening file %s\n", energy_file);
      printf("Cannot continue, sorry!\n");
      exit(1);
    }
  
  //if ( energy_minimisation )
    fprintf(p_energy_file,"%6s %15s %12s %20s %20s %12s\n",
	    "#Block", "Iter", "Time/ps",
	    "Pot En/eV", "Kin En/eV", "Temp/K"); 
    //else
    //fprintf(p_energy_file,"%6s %15s %12s %18s\n",
    //    "#Block", "Iter", "Time/s",
    //    "Pot En/eV"); 
  
  
  fclose(p_energy_file);
  


  /* in case of run with single adsorbate 
     print header of adsorbate position 
     and velocity file */

  if ( single_adsorbate ) {
     
    FILE *p_adsorbate_pos_file;
    
    if ( (p_adsorbate_pos_file=fopen(adsorbate_position_file, "w")) 
	 == NULL ) {
      printf("Problems encountered during writing header to output file.\n");
      printf("A fatal error occured while opening file %s\n", 
	     adsorbate_position_file);
      printf("Cannot continue, sorry!\n");
      exit(1);
    }

    fprintf(p_adsorbate_pos_file, "%6s %10s %12s %12s %12s %12s\n",
	    "#block", "step", "time", "x", "y", "z");

    fclose(p_adsorbate_pos_file);
    
    FILE *p_adsorbate_vel_file;
    
    if ( (p_adsorbate_vel_file=fopen(adsorbate_velocity_file, "w")) 
	 == NULL ) {
      printf("Problems encountered during writing haeader to output file.\n");
      printf("A fatal error occured while opening file %s\n", 
	     adsorbate_velocity_file);
      printf("Cannot continue, sorry!\n");
      exit(1);
    }

    fprintf(p_adsorbate_vel_file, "%6s %10s %12s %20s %20s %20s\n",
	  "#block", "step", "time", "vx", "vy", "vz");
    

    fclose(p_adsorbate_vel_file);
  }



  return(0);
}
