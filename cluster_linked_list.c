#ifndef _CLUSTER_LNKD_LIST_HEAD_H_
#define _CLUSTER_LNK_LIST_HEAD_H_    




#include "cluster_linked_list.h"



/* 
   This file contains the implementation
   of the functions associated with a linked
   list of integers. The list is sorted in 
   ascending order. 
*/





/* returns number of elements
   in the list*/
int count_element_in_list
(cluster_linked_list the_list) {
  
  if ( the_list == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function count_element!",
	    "The list is not initialised.");
    return -1; 
  }
  
 
  if ( the_list->next == NULL )
    /* last element in the list */
    return 0;
  else
    return (1 + 
	    count_element_in_list(the_list->next));
  
}




/***********************************************************/




cluster* 
create_clust_array(int cluster_num,
		   cluster_linked_list cluster_list)
{
  cluster_linked_list  list_node = 
    cluster_list->next;
  
  /* allocate memory for array */
  cluster *clust_array = 

    malloc(cluster_num*sizeof(cluster));
  
  for (int i=0; i<cluster_num; i++) {
    clust_array[i] = 
      create_copy_cluster(list_node->d);
    list_node = list_node->next;
  }
  
  
  return clust_array;
}





/*****************************************************/





/* Creates and initialises 
   an empty  list of clusters
   The first element in the list
   is just a sentinel */
cluster_linked_list create_list()
{
  cluster_linked_list the_list;

  /* allocate memory for the sentinel,
     first element of the list,
     please note that  the data field d  
     in the sentinel node is not 
     initialised */
  the_list = (cluster_linked_list) 
    malloc(sizeof(cluster_list_node));
  the_list->next = NULL; 

  return the_list;
}





/***********************************************************/





int delete_clust_array(int clust_array_size,
			cluster *clust_array)
{
  
  /* deallocate memory for cluster array */
  for (int i=0; i<clust_array_size; i++)
    //delet_cluster(clust_array+i);
    delete_cluster(&clust_array[i]);
  
  free(clust_array);

  return 0;
}





/***********************************************************/




int delete_element(data the_data, 
		   cluster_linked_list the_list)

{
  
  cluster_linked_list temp;

  if ( the_list == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function delete_element!",
	    "The list is not initialised.");
    return -1; 
  }
  

  if ( the_list->next == NULL )
    /* at the end of the list,
       the element to eliminate
       has not been found in the list */
    return 0;
  else 
    //if ( the_list->next->d == the_data ) { 
    if ( compare_cluster(the_list->next->d, 
			 the_data) ) {
      /* the_list->next points 
	 to element to delete.
	 Reassign the_list->next
	 in such a way that it points to 
	 element following element to delete
	 and free memory associated with
	 element to delete */
      temp = the_list->next;
      the_list->next = the_list->next->next;
      delete_cluster(&temp->d);
      free(temp);
      return 1;
    }
    else
      return ( delete_element(the_data, 
			      the_list->next) );
  
  
}




/***********************************************************/




/* always invoke this function before a cluster 
   list created with function create_list() gets
   out of scope */

int delete_list(cluster_linked_list *p_list)
{
  
  cluster_linked_list the_list = *p_list; 
  
   
  if ( the_list == NULL ) {
    /* all of the list has 
       already been deleted */
    *p_list = NULL;
    return 0;
  }  
  
  while( the_list->next != NULL ) {
    /* delete current node */
    cluster_linked_list temp = the_list->next;
    the_list->next = the_list->next->next;
    delete_cluster(&temp->d);
    free(temp);
  }  
  
  /* the list is now composed 
     of the sentinel node only */
  /* delete sentinel node and set 
     list to NULL */
  //delete_cluster(&the_list->d);
  free(the_list);
  *p_list = NULL;
 
  
  
  return 1;
}


  
/***********************************************************/


/* returns 1 if element is in 
   the list and 0 otherwise */
int element_in_list(data the_data, 
		    cluster_linked_list the_list)
{

  if ( the_list == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function element_in_list!",
	    "The list is not initialised.");
    return -1;
  }

  
  /* use function recursively */
  if ( the_list->next == NULL )
    /* already at end of list,
       the element was not found 
       in the list */
    return 0;
  else 
    if ( compare_cluster(the_data, 
			 the_list->next->d) )
      return 1;
    else
      return ( element_in_list(the_data, 
			       the_list->next) );
  
}




/***********************************************************/






/* Returns 1 if the list is empty 
   and 0 otherwise */
int empty_list(cluster_linked_list the_list) 
{
  
  if ( the_list == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function empty_list!",
	    "The list is not initialised.");
    return -1;
  }
  
  if ( the_list->next == NULL )
    return 1;
  else 
    return 0;
}



/***********************************************************/





/* Inserts an element in the 
   appropiate position in such
   a way to keep the list sorted.
   The list is assumed to be
   sorted in ascending order
   before the new data is inserted.
   Returns 1 in case of success 
   and 0 otherwise */
int insert_element_in_list(data the_data,
			   cluster_linked_list the_list)
{
  cluster_linked_list temp = the_list;
  
  if ( the_list == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function insert_element!",
	    "The list is not initialised.");
    return -1; 
  }

  /* return 0 if data to insert is 
     already  in the list */
  if ( element_in_list(the_data, the_list) )
    return 0;
  
  
  /* otherwise, insert element 
     at the correct position */
  if ( the_list->next == NULL
       ||
       ( the_data->species_num <= the_list->next->d->species_num 
	 &&
	 the_data->unit_num < the_list->next->d->unit_num ) ) {
    /* insert element at this point */
    temp = the_list->next;
    the_list->next = malloc(sizeof(cluster_list_node));
    the_list->next->d = create_copy_cluster(the_data);
    the_list->next->next = temp;
    return 1;
  }
  else
    /* insert element ahead in the list */
    return insert_element_in_list(the_data, the_list->next);
  
}







/***********************************************************/

int print_node_data(cluster_linked_list the_list)
{
   
  if ( the_list == NULL ) {
    printf( "\n%s\n%s\n\n", 
	    "Error in function print_node_data!",
	    "The list is not initialised.");
    return -1;
  }

  
  if ( the_list->next == NULL ) {
    printf("\n%s\n%s\n\n",
	   "Non fatal error found in function print_node_data.",
	   "The list is empty. No value to print.");
    return 1;
  }

  cluster the_clust = the_list->next->d;

  printf("\n%s%d\n%s%d\n",
	 "N. chemical species in cluster: ", 
	 the_clust->species_num,
	 "N. ions in cluster: ",
	 the_clust->unit_num);
  for (int i=0; 
       i<the_clust->unit_num; 
       i++)
    printf("%2s %12.3lf %12.3lf %12.3lf\n",
	   the_clust->
	   species_symbol[the_clust->unit_type[i]],
	   the_clust->unit_coord[i][0],
	   the_clust->unit_coord[i][1],
	   the_clust->unit_coord[i][2]);
  
  printf("\n");
  

  return 0;
}



/***********************************************************/



#endif
